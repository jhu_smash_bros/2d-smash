﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(CameraFollower))]
public class CaneraFollowerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CameraFollower cf = (CameraFollower)target;

        if (GUILayout.Button("Set Min Cam Pos"))
        {
            cf.SetMinCamPos();
        }

        if (GUILayout.Button("Set Max Cam Pos"))
        {
            cf.SetMaxCamPos();
        }
    }
}
