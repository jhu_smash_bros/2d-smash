
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CielController : MonoBehaviour
{
    [HideInInspector]
	public Animator an;
    [HideInInspector]
	public Rigidbody2D rb;

    private int facing = 1;

    void Start()
    {
		an = GetComponent<Animator>();
		rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if ((rb.velocity.x < 0 && facing > 0) || (rb.velocity.x > 0 && facing < 0))
            ReverseImage();

        an.SetFloat("speed", Mathf.Abs(rb.velocity.x));
    }

    // make sure the sprite is facing the right way
	public void ReverseImage()
	{
		// reverse direction
		facing = -facing;
        
        // flip the character around 
		Vector2 theScale = rb.transform.localScale;
		theScale.x *= -1;
		rb.transform.localScale = theScale;
	}
}

