﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CPUSelectToken : CharacterSelectToken
{
    public TextMeshProUGUI portText;
    public CharacterSelectToken csToken;
    // Start is called before the first frame update
    void Start()
    {
        activePlayer = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDrag()
    {
        Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pos.z = 0;
        trans.position = pos;
        chosen = false;
    }

    private void OnMouseUp()
    {
        if(character != null)
        {
            character.OnMouseClick(player, true);
            chosen = true;
            portText.text = "CPU " + player.ToString();
        }
    }
    private void OnMouseDown()
    {
        if(character != null && !csToken.isReady())
        {
            character.PlayerReset(player);
            portText.text = "Player " + player.ToString();
        }
    }
}
