﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterSelection : MonoBehaviour {

    public Sprite charPic;
    public Image p1;
    public Image p2;
    public Image p3;
    public Image p4;

    public GameObject characterMenu;
    public GameObject stageMenu;

    // determines the character chosen
    public void OnMouseClick(int player, bool cpu = false)
    {
        switch (player)
        {
            case 1:
                if (p1.IsActive() && (UtilStaticVars.Player1 =="" || UtilStaticVars.CPU1 == true))
                {
                    p1.sprite = charPic;
                    UtilStaticVars.Player1 = gameObject.name;
                    UtilStaticVars.CPU1 = cpu;
                }
                break;
            case 2:
                if (p2.IsActive() && (UtilStaticVars.Player2 == "" || UtilStaticVars.CPU2 == true))
                {
                    p2.sprite = charPic;
                    UtilStaticVars.Player2 = gameObject.name;
                    UtilStaticVars.CPU2 = cpu;
                }
                break;
            case 3:
                if (p3.IsActive() && (UtilStaticVars.Player3 == "" || UtilStaticVars.CPU3 == true))
                {
                    p3.sprite = charPic;
                    UtilStaticVars.Player3 = gameObject.name;
                    UtilStaticVars.CPU3 = cpu;
                }
                break;
            case 4:
                if (p4.IsActive() && (UtilStaticVars.Player4 == "" || UtilStaticVars.CPU4 == true))
                {
                    p4.sprite = charPic;
                    UtilStaticVars.Player4 = gameObject.name;
                    UtilStaticVars.CPU4 = cpu;
                }
                break;
            default:
                break;
        }
    }

    public void CharacterReset()
    {
        p1.sprite = null;
        p2.sprite = null;
        try
        {
            p3.sprite = null;
            p4.sprite = null;
        }
        catch 
        {

            
        }
        UtilStaticVars.Player1 = "";
        UtilStaticVars.Player2 = "";
        UtilStaticVars.Player3 = "";
        UtilStaticVars.Player4 = "";
        UtilStaticVars.CPU1 = false;
        UtilStaticVars.CPU2 = false;
        UtilStaticVars.CPU3 = false;
        UtilStaticVars.CPU4 = false;
    }

    public void PlayerReset(int playerNum)
    {
        switch (playerNum)
        {
            case 1:
                p1.sprite = null;
                UtilStaticVars.Player1 = "";
                UtilStaticVars.CPU1 = false;
                break;
            case 2:
                p2.sprite = null;
                UtilStaticVars.Player2 = "";
                UtilStaticVars.CPU2 = false;
                break;
            case 3:
                p3.sprite = null;
                UtilStaticVars.Player3 = "";
                UtilStaticVars.CPU3 = false;
                break;
            case 4:
                p4.sprite = null;
                UtilStaticVars.Player4 = "";
                UtilStaticVars.CPU4 = false;
                break;
            default:
                break;
                
        }
    }
}
