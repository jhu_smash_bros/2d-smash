﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsMenu : MonoBehaviour
{
    // change the volume
    public void SetVolume(float value)
    {
        FindObjectOfType<AudioManager>().GetComponent<AudioSource>().volume = value;
        UtilStaticVars.Volume = value;
    }
    private void Start()
    {
        UtilStaticVars.Volume = 0.5f;
    }
}
