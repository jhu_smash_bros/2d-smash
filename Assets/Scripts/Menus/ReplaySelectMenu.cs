﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ReplaySelectMenu : MonoBehaviour
{
    public TextMeshProUGUI title1;
    public TextMeshProUGUI title2;
    public TextMeshProUGUI title3;
    public TextMeshProUGUI title4;
    public TextMeshProUGUI title5;
    public TextMeshProUGUI title6;
    public TextMeshProUGUI title7;
    public TextMeshProUGUI title8;
    public TextMeshProUGUI title9;
    public GameObject button1;
    public GameObject button2;
    public GameObject button3;
    public GameObject button4;
    public GameObject button5;
    public GameObject button6;
    public GameObject button7;
    public GameObject button8;
    public GameObject button9;

    private string path = "Assets/Resources/Replays";
    private List<string> replayFiles;
    private List<string> allReplayFiles;
    private int arrayPosStart = 0;
    private bool transitionUp = false;
    private bool transitionDown = false;
    private ReplayData rd = new ReplayData();

    // Start is called before the first frame update
    void Awake()
    {
        try
        {
            allReplayFiles = new List<string>(Directory.GetFiles(path, "*.txt"));
        }
        catch 
        {
            allReplayFiles = new List<string>();
        }
        replayFiles = new List<string>();
        int numRepDisplayed = (allReplayFiles.Count > 9) ? 9 : allReplayFiles.Count;
        for (int i = 0; i < numRepDisplayed; i++)
        {
            replayFiles.Add(allReplayFiles[i]);
        }
    }

    //enable only the necessary buttons
    private void OnEnable()
    {
        char[] separator = { '/', '\\', '.'};
        if (replayFiles.Count < 1)
        {
            button1.SetActive(false);
        }
        else
        {
            title1.text = replayFiles[0].Split(separator)[3].Replace("_", " ");
        }
        if (replayFiles.Count < 2)
        {
            button2.SetActive(false);
        }
        else
        {
            title2.text = replayFiles[1].Split(separator)[3].Replace("_", " ");
        }
        if (replayFiles.Count < 3)
        {
            button3.SetActive(false);
        }
        else
        {
            title3.text = replayFiles[2].Split(separator)[3].Replace("_", " ");
        }
        if (replayFiles.Count < 4)
        {
            button4.SetActive(false);
        }
        else
        {
            title4.text = replayFiles[3].Split(separator)[3].Replace("_", " ");
        }
        if (replayFiles.Count < 5)
        {
            button5.SetActive(false);
        }
        else
        {
            title5.text = replayFiles[4].Split(separator)[3].Replace("_", " ");
        }
        if (replayFiles.Count < 6)
        {
            button6.SetActive(false);
        }
        else
        {
            title6.text = replayFiles[5].Split(separator)[3].Replace("_", " ");
        }
        if (replayFiles.Count < 7)
        {
            button7.SetActive(false);
        }
        else
        {
            title7.text = replayFiles[6].Split(separator)[3].Replace("_", " ");
        }
        if (replayFiles.Count < 8)
        {
            button8.SetActive(false);
        }
        else
        {
            title8.text = replayFiles[7].Split(separator)[3].Replace("_", " ");
        }
        if (replayFiles.Count < 9)
        {
            button9.SetActive(false);
        }
        else
        {
            title9.text = replayFiles[8].Split(separator)[3].Replace("_", " ");
        }
    }

    // Update is called once per frame
    void Update()
    {
        char[] separator = { '/', '\\', '.' };
        if (transitionUp)
        {
            //if it can't go any lower don't do anything
            if(arrayPosStart == 0)
            {
                transitionUp = false;
            }
            else
            {
                arrayPosStart -= 3;
                if(arrayPosStart < 0)
                {
                    arrayPosStart = 0;
                }
                for(int i = 0; i < 9; i++)
                {
                    replayFiles[i] = allReplayFiles[i + arrayPosStart];
                }
                title1.text = replayFiles[0].Split(separator)[3];
                title2.text = replayFiles[1].Split(separator)[3];
                title3.text = replayFiles[2].Split(separator)[3];
                title4.text = replayFiles[3].Split(separator)[3];
                title5.text = replayFiles[4].Split(separator)[3];
                title6.text = replayFiles[5].Split(separator)[3];
                title7.text = replayFiles[6].Split(separator)[3];
                title8.text = replayFiles[7].Split(separator)[3];
                title9.text = replayFiles[8].Split(separator)[3];
            }
            transitionUp = false;
        }
        else if (transitionDown)
        {
            //if it can't go any higher don't do anything
            if (arrayPosStart + 9 >= allReplayFiles.Count)
            {
                transitionDown = false;
            }
            else
            {
                for (int j = 0; j < 3; j++)
                {
                    arrayPosStart++;
                    if (arrayPosStart + 9 == allReplayFiles.Count)
                    {
                        break;
                    }
                }

                for (int i = 0; i < 9; i++)
                {
                    replayFiles[i] = allReplayFiles[i + arrayPosStart];
                }
                title1.text = replayFiles[0].Split(separator)[3];
                title2.text = replayFiles[1].Split(separator)[3];
                title3.text = replayFiles[2].Split(separator)[3];
                title4.text = replayFiles[3].Split(separator)[3];
                title5.text = replayFiles[4].Split(separator)[3];
                title6.text = replayFiles[5].Split(separator)[3];
                title7.text = replayFiles[6].Split(separator)[3];
                title8.text = replayFiles[7].Split(separator)[3];
                title9.text = replayFiles[8].Split(separator)[3];

            }
            transitionDown = false;
        }
    }

    public void goUp()
    {
        transitionUp = true;
    }
    public void goDown()
    {
        transitionDown = true;
    }

    public void loadReplay(int replayNum)
    {
        rd.Read(replayFiles[replayNum]);
        rd.addToUtilStaticVars();
        UtilStaticVars.RealGame = true;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        SceneManager.LoadScene(1);
    }
}
