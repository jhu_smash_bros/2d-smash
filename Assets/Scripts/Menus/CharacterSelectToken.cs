﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelectToken : MonoBehaviour
{
    public int player;
    public RectTransform trans;
    public Vector3 startPos;

    protected bool chosen = false;
    protected bool activePlayer = false;

    //deadzone of joystick
    private float deadzone = 0.075f;
    private float v;
    private float h;
    private float vAlt;
    private float hAlt;
    private bool select;
    private bool back;

    protected CharacterSelection character = null;

    // make sure to reset the tokens
    protected void OnEnable()
    {
        chosen = false;
        character = null;
        trans.anchoredPosition = startPos;
    }

    void Update()
    {
        switch (player)
        {
            case 1:
                v = Input.GetAxisRaw("Vertical");
                h = Input.GetAxisRaw("Horizontal");
                vAlt = Input.GetAxisRaw("VerticalAlt");
                hAlt = Input.GetAxisRaw("HorizontalAlt");
                select = Input.GetButtonDown("Attack");
                back = Input.GetButtonDown("Special");
                break;

            case 2:
                v = Input.GetAxisRaw("Vertical2");
                h = Input.GetAxisRaw("Horizontal2");
                vAlt = Input.GetAxisRaw("Vertical2Alt");
                hAlt = Input.GetAxisRaw("Horizontal2Alt");
                select = Input.GetButtonDown("Attack2");
                back = Input.GetButtonDown("Special2");
                break;

            case 3:
                v = 0;
                h = 0;
                vAlt = Input.GetAxisRaw("Vertical3");
                hAlt = Input.GetAxisRaw("Horizontal3");
                select = Input.GetButtonDown("Attack3");
                back = Input.GetButtonDown("Special3");
                break;

            case 4:
                v = 0;
                h = 0;
                vAlt = Input.GetAxisRaw("Vertical4");
                hAlt = Input.GetAxisRaw("Horizontal4");
                select = Input.GetButtonDown("Attack4");
                back = Input.GetButtonDown("Special4");
                break;

            default:
                break;
        }
        //move if in the process of selecting a character
        if (!chosen && activePlayer)
        {
            trans.position = new Vector3(trans.position.x + (0.2f * h) + (0.2f * hAlt),
                trans.position.y + (0.2f * v) + (0.2f * vAlt), trans.position.z);
            if(trans.anchoredPosition.x < -385f || trans.anchoredPosition.x > 385f)
            {
                trans.position = new Vector3(trans.position.x - (0.2f * h) - (0.2f * hAlt),
                    trans.position.y, trans.position.z);
            }
            if (trans.anchoredPosition.y > -48f || trans.anchoredPosition.y < -370f)
            {
                trans.position = new Vector3(trans.position.x,
                    trans.position.y - (0.2f * v) - (0.2f * vAlt), trans.position.z);
            }
            //press b to deactivate controller
            if (back)
            {
                activePlayer = false;
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("controller off");
                Color32 tempCol = gameObject.GetComponent<Image>().color;
                tempCol.a = 140;
                gameObject.GetComponent<Image>().color = tempCol;
            }
            //press a to select character
            else if (select)
            {
                if (character != null)
                {
                    GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("character select");
                    character.OnMouseClick(player);
                    chosen = true;
                }

            }
        }
        else if (!chosen && !activePlayer)
        {
            //press a to activate character
            if (select)
            {
                activePlayer = true;
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("controller on");
                Color32 tempCol = gameObject.GetComponent<Image>().color;
                tempCol.a = 255;
                gameObject.GetComponent<Image>().color = tempCol;
            }
            else
            {
                trans.anchoredPosition = startPos;
            }
        }
        else if (chosen && activePlayer && back)
        {
            if (chosen)
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("back");

            character.PlayerReset(player);
            chosen = false;
        }
    }

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<CharacterSelection>() != null && !chosen && activePlayer)
        {
            GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("hover");
            other.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            character = other.GetComponent<CharacterSelection>();
        }
    }
    protected void OnTriggerExit2D(Collider2D other)
    {
        if (other.GetComponent<CharacterSelection>() == character)
        {
            other.GetComponent<Image>().color = new Color32(255, 255, 255, 140);
            character = null;
        }
    }
    //is this player ready to go to stage select
    public bool isReady()
    {
        return chosen;
    }
}
