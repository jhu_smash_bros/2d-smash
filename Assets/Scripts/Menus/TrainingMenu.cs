﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TrainingMenu : MonoBehaviour
{
    public Vector3 highlighterPos0;
    public Vector3 highlighterPos1;
    public Vector3 highlighterPos2;
    public Vector3 highlighterPos3;
    public Vector3 highlighterPos4;

    public TextMeshProUGUI p1Percentage;
    public TextMeshProUGUI p2Percentage;
    public TextMeshProUGUI finalSmashMeterSettings;
    public TextMeshProUGUI cpuBehavior;
    public TextMeshProUGUI hitboxes;

    public GameObject highlighter;

    //buffers so that menu select bar doesn't change too fast
    private float vertBuffer = 0f;
    private float horBuffer = 0f;
    private float bufferTime = .20f;

    private int selector = 0;
    private int numOptions = 5;
    private int percent1 = 0;
    private int percent2 = 0;
    private bool alwaysOn = false;
    private bool hitboxOn = false;
    private bool cpuAttack = true;

    // Start is called before the first frame update
    void Start()
    {
        p1Percentage.text = "<  P1 Percent 0  >";
        p2Percentage.text = "<  P2 Percent 0  >";
        finalSmashMeterSettings.text = "Final Smash Meter:<  Normal  >";
        cpuBehavior.text = "CPU Behavior:<  Attack  >";
        hitboxes.text = "HitBoxes:<  Off  >";
    }

    // Update is called once per frame
    void Update()
    {
        if (UtilStaticVars.RealGame)
        {
            gameObject.SetActive(false);
            return;
        }

        //vertical movement to select
        if(Input.GetAxisRaw("Vertical")> .5f || Input.GetAxisRaw("VerticalAlt") > 0.5f ||
            Input.GetAxisRaw("Vertical2") >0.5f || Input.GetAxisRaw("Vertical2Alt") > 0.5f)
        {
            if (vertBuffer <= 0.0f)
            {
                selector--;
                if (selector < 0)
                {
                    selector = numOptions - 1;
                }
                vertBuffer = Time.deltaTime;
            }
            else
            {
                vertBuffer += Time.deltaTime;
                if(vertBuffer >= bufferTime)
                {
                    vertBuffer = 0.0f;
                }
            }
        }
        else if (Input.GetAxisRaw("Vertical") < -0.5f || Input.GetAxisRaw("VerticalAlt") < -0.5f ||
            Input.GetAxisRaw("Vertical2") < -0.5f || Input.GetAxisRaw("Vertical2Alt") < -0.5f)
        {
            if (vertBuffer >= 0.0f)
            {
                selector++;
                if (selector >= numOptions)
                {
                    selector = 0;
                }
                vertBuffer = -Time.deltaTime;
            }
            else
            {
                vertBuffer -= Time.deltaTime;
                if (vertBuffer <= -bufferTime)
                {
                    vertBuffer = 0.0f;
                }
            }
        }
        else
        {
            vertBuffer = 0.0f;
        }
        switch (selector)
        {
            case 0:
                highlighter.transform.localPosition = highlighterPos0;
                break;
            case 1:
                highlighter.transform.localPosition = highlighterPos1;
                break;
            case 2:
                highlighter.transform.localPosition = highlighterPos2;
                break;
            case 3:
                highlighter.transform.localPosition = highlighterPos3;
                break;
            case 4:
                highlighter.transform.localPosition = highlighterPos4;
                break;
            default:
                break;
        }
        //horizontal movement to change selected option
        if(Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("HorizontalAlt")>0.5f ||
            Input.GetAxisRaw("Horizontal2") > 0.5f || Input.GetAxisRaw("Horizontal2Alt") > 0.5f)
        {
            if (horBuffer <= 0.0f)
            {
                selection(true);
                horBuffer = Time.deltaTime;
            }
            else
            {
                horBuffer += Time.deltaTime;
                if(horBuffer >= bufferTime)
                {
                    horBuffer = 0.0f;
                }
            }
        }
        else if (Input.GetAxisRaw("Horizontal") < -0.5f || Input.GetAxisRaw("HorizontalAlt") < -0.5f ||
            Input.GetAxisRaw("Horizontal2") < -0.5f || Input.GetAxisRaw("Horizontal2Alt") < -0.5f)
        {
            if (horBuffer >= 0.0f)
            {
                selection(false);
                horBuffer = -Time.deltaTime;
            }
            else
            {
                horBuffer -= Time.deltaTime;
                if (horBuffer <= -bufferTime)
                {
                    horBuffer = 0.0f;
                }
            }
        }

        //final smash meter stays
        if (alwaysOn)
        {
            Camera.main.GetComponent<CameraFollower>().targets[0].GetComponent<SmashCharacter>().permaSmash = true;
            Camera.main.GetComponent<CameraFollower>().targets[1].GetComponent<SmashCharacter>().permaSmash = true;
        }

    }

    private void selection(bool right)
    {
        // 0: p1 percent
        // 1: p2 percent
        // 2: final smash meter
        // 3: cpu behavior
        // 4: hitboxes
        switch (selector)
        {
            case 0:
                if (right)
                {
                    percent1++;
                    if (percent1 > 999)
                    {
                        percent1 = 0;
                    }
                }
                else
                {
                    percent1--;
                    if (percent1 < 0)
                    {
                        percent1 = 999;
                    }
                }
                p1Percentage.text = "<  P1 Percent " + percent1 + "  >";
                Camera.main.GetComponent<CameraFollower>().targets[0].GetComponent<SmashCharacter>().percentage = percent1;
                break;
            case 1:
                if (right)
                {
                    percent2++;
                    if(percent2 > 999)
                    {
                        percent2 = 0;
                    }
                }
                else
                {
                    percent2--;
                    if(percent2 < 0)
                    {
                        percent2 = 999;
                    }
                }
                p2Percentage.text = "<  P2 Percent " + percent2 + "  >";
                Camera.main.GetComponent<CameraFollower>().targets[1].GetComponent<SmashCharacter>().percentage = percent2;
                break;
            case 2:
                if (alwaysOn)
                {
                    alwaysOn = false;
                    finalSmashMeterSettings.text = "Final Smash Meter:<  Normal  >";
                }
                else
                {
                    alwaysOn = true;
                    finalSmashMeterSettings.text = "Final Smash Meter:<  Full  >";
                }
                break;
            case 3:
                if (cpuAttack)
                {
                    cpuAttack = false;
                    cpuBehavior.text = "CPU Behavior:<  Stand  >";
                    Camera.main.GetComponent<CameraFollower>().targets[0].GetComponent<SmashCharacter>().isCPU = false;
                    Camera.main.GetComponent<CameraFollower>().targets[1].GetComponent<SmashCharacter>().isCPU = false;
                }
                else
                {
                    cpuAttack = true;
                    cpuBehavior.text = "CPU Behavior:<  Attack  >";
                    if(Camera.main.GetComponent<CameraFollower>().targets[0].GetComponent<SmashCharacter>().player < 0)
                    {
                        Camera.main.GetComponent<CameraFollower>().targets[0].GetComponent<SmashCharacter>().isCPU = true;
                    }
                    if (Camera.main.GetComponent<CameraFollower>().targets[1].GetComponent<SmashCharacter>().player < 0)
                    {
                        Camera.main.GetComponent<CameraFollower>().targets[1].GetComponent<SmashCharacter>().isCPU = true;
                    }
                }
                break;
            case 4:
                if (hitboxOn)
                {
                    hitboxOn = false;
                    UtilStaticVars.ShowHitbox = false;
                    hitboxes.text = "HitBoxes:<  Off  >"; 
                }
                else
                {
                    hitboxOn = true;
                    UtilStaticVars.ShowHitbox = true;
                    hitboxes.text = "HitBoxes:<  On  >";
                }
                break;
            default:
                break;
        }
    }
}
