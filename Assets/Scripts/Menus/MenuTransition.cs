﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuTransition : MonoBehaviour
{
    public CharacterSelectToken[] characterTokens;
    public GameObject characterMenu;
    public GameObject stageMenu;
    public TextMeshProUGUI prompt;
    public bool vs;

    private bool ready = false;

    void Update()
    {
        int numReady = 0;
        foreach (CharacterSelectToken ct in characterTokens)
        {
            //need at least 2 players for versus mode
            ready = ct.isReady();
            if (ready)
            {
                numReady++;
                ready = false;
            }
            if(vs && numReady > 1)
            {
                ready = true;
                break;
            }
            else if(!vs && numReady > 0)
            {
                ready = true;
                break;
            }
        }

        // once characters are chosen
        if (ready)
        {
            prompt.text = "PRESS ENTER";
            prompt.color = new Color32(255, 0, 0, 255);
        }
        else
        {
            prompt.text = "READY ???";
            prompt.color = new Color32(255, 0, 0, 100);
        }

        // start the game
        if (ready && Input.GetButtonDown("StartButton"))
        {
            GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("menu select");
            characterMenu.SetActive(false);
            stageMenu.SetActive(true);
            if (vs)
            {
                UtilStaticVars.RealGame = true;
                UtilStaticVars.ShowHitbox = false;
            }
            else
            {
                UtilStaticVars.RealGame = false;
            }
        }
    }
}
