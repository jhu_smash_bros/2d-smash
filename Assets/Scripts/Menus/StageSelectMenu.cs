﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StageSelectMenu : MonoBehaviour
{
    [SerializeField]
    private string name;
    [SerializeField]
    private string theme;

    public void OnMouseClick()
    {
        UtilStaticVars.Theme = theme;
        UtilStaticVars.Stage = name;
        StartCoroutine(PlayGame());
    }

    IEnumerator PlayGame()
    {
        int numPlayers = 0;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }

    public void updateStockAmount(int numStocks)
    {
        UtilStaticVars.Stocks = numStocks;
    }
}
