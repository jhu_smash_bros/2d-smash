﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorpionAI : GenericAI
{
    private ScorpionController scorp;
    private LinkedList<GameObject> platforms = new LinkedList<GameObject>();
    private GameObject bottomPlat;
    private Transform farLeftLedge;
    private Transform farRightLedge;
    private SmashCharacter targetEnemy;
    private int jump = 0;
    private bool pulling = false;
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] plats = GameObject.FindGameObjectsWithTag("Stage");
        foreach (GameObject p in plats)
        {
            if (p.name != "Bottom Platform")
                platforms.AddLast(p);
            else
                bottomPlat = p;
            if(farLeftLedge == null || p.transform.Find("left ledge").position.x <
                farLeftLedge.transform.position.x)
            {
                farLeftLedge = p.transform.Find("left ledge");
            }
            if (farRightLedge == null || p.transform.Find("right ledge").position.x >
                farRightLedge.transform.position.x)
            {
                farRightLedge = p.transform.Find("right ledge");
            }
        }
        scorp = GetComponent<ScorpionController>();
    }

    public override void inputSim()
    {
        inputClear();
        //target set
        targetEnemy = enemy1;
        if(enemy2 != null && !enemy2.dead && enemy2.percentage > targetEnemy.percentage)
        {
            targetEnemy = enemy2;
        }
        if (enemy3 != null && !enemy3.dead && enemy3.percentage > targetEnemy.percentage)
        {
            targetEnemy = enemy3;
        }

        if (scorp.rb.velocity.y == 0)
        {
            jump = 0;
        }
        //jump tracker
        else if (jump == 0 && scorp.rb.velocity.y != 0)
        {
            jump = 1;
        }
        if (scorp.an.GetCurrentAnimatorStateInfo(0).IsName("Here"))
        {
            pulling = true;
        }

        //logic
        if (targetEnemy != null && !targetEnemy.dead)
        {
            Transform lLedge;
            Transform rLedge;
            float xPosDiff = targetEnemy.transform.position.x - transform.position.x;
            float yPosDiff = targetEnemy.transform.position.y - transform.position.y;
            //x axis movement
            if (Mathf.Abs(xPosDiff) > 6)
            {
                hAlt = xPosDiff / Mathf.Abs(xPosDiff);
            }
            else
            {
                hAlt = 0;
            }
            if (xPosDiff < 0 && scorp.facing == 1
                && scorp.an.GetCurrentAnimatorStateInfo(0).IsName("idle"))
            {
                h = -1;
            }

            if (xPosDiff > 0 && scorp.facing == -1
                && scorp.an.GetCurrentAnimatorStateInfo(0).IsName("idle"))
            {
                h = 1;
            }

            //y axis movement
            if (yPosDiff > .5 && scorp.rb.velocity.y <= 0 && targetEnemy.rb.velocity.y < 0)
            {
                jumpKey = true;
            } else if (yPosDiff < -.5)
            {
                foreach (GameObject p in platforms)
                {
                    lLedge = p.transform.Find("left ledge");
                    rLedge = p.transform.Find("right ledge");
                    if (p.name != "Bottom Platform" && targetEnemy.transform.position.x < rLedge.position.x
                        && targetEnemy.transform.position.x > lLedge.position.x)
                    {
                        vAlt = -1;
                    }
                }
            }

            //opponent offstage
            if (targetEnemy.transform.position.x <
                farLeftLedge.position.x &&
                targetEnemy.rb.velocity.y !=0)
            {
                vAlt = 0;
                hAlt = 0;
                shieldKey = true;
            }
            else if (targetEnemy.transform.position.x >
                farRightLedge.position.x &&
                targetEnemy.rb.velocity.y != 0)
            {
                vAlt = 0;
                hAlt = 0;
                shieldKey = true;
            }

            //on Stage attacks
            if (Mathf.Abs(xPosDiff) < 5 &&
               Mathf.Abs(yPosDiff) < 5 &&
               targetEnemy.percentage < 88)
            {
                attackKey = true;
            }
            else if (Mathf.Abs(xPosDiff) < 24 && Mathf.Abs(yPosDiff) < 2 && targetEnemy.percentage > 75)
            {
                if(xPosDiff < 0)
                {
                    hAlt = -1;
                }
                else
                {
                    hAlt = 1;
                }
                specialKey = true;
                specialHold = true;
            }

            //recovery on the left side
            if ((transform.position.x <
                farLeftLedge.position.x) || (transform.position.x <
                bottomPlat.transform.Find("left ledge grab").position.x 
                && scorp.rb.velocity.y <0))
            {
                if (scorp.an.GetCurrentAnimatorStateInfo(0).IsName("up b"))
                {
                    //up b angle calculation
                    Vector3 distVec = bottomPlat.transform.Find("left ledge grab").position - transform.position;
                    float totalDist = distVec.x + distVec.y;
                    vAlt = distVec.y / totalDist;
                    hAlt = distVec.x / totalDist;
                }
                else if (jump == 1 && scorp.rb.velocity.y < 0)
                {
                    jumpKey = true;
                    hAlt = 1;
                    jump = 2;
                    specialHold = false;
                    specialKey = false;
                }
                else if (jump == 2 && scorp.rb.velocity.y < 0)
                {
                    specialHold = true;
                    specialKey = true;
                    attackKey = false;
                    vAlt = 1;
                    hAlt = 0;
                }
                else if (scorp.rb.velocity.y != 0)
                {
                    hAlt = 1;
                }
            }

            //recovery on the right side
            if (transform.position.x >
                farRightLedge.position.x || (transform.position.x >
                bottomPlat.transform.Find("right ledge grab").position.x
                && scorp.rb.velocity.y < 0))
            {
                if (scorp.an.GetCurrentAnimatorStateInfo(0).IsName("up b"))
                {
                    //up b angle calculation
                    Vector3 distVec = bottomPlat.transform.Find("right ledge grab").position - transform.position;
                    float totalDist = distVec.x + distVec.y;
                    vAlt = distVec.y / totalDist;
                    hAlt = distVec.x / totalDist;
                }
                else if (jump == 1 && scorp.rb.velocity.y < 0)
                {
                    jumpKey = true;
                    hAlt = -1;
                    jump = 2;
                    specialHold = false;
                    specialKey = false;
                }
                else if (jump == 2 && scorp.rb.velocity.y < 0)
                {
                    specialHold = true;
                    specialKey = true;
                    vAlt = -1;
                    hAlt = 0;
                }
                else if (scorp.rb.velocity.y != 0)
                {
                    hAlt = -1;
                }
            }

            if (!scorp.an.GetCurrentAnimatorStateInfo(0).IsName("Here") && pulling)
            {
                inputClear();
                vAlt = -1;
            }
            if(scorp.an.GetCurrentAnimatorStateInfo(0).IsName("crouch") && pulling)
            {
                specialKey = true;
                vAlt = -1;
                pulling = false;
            }
        }

        //Let go of shield key if low shield
        if(shieldKey && scorp.shieldHealth < 20f)
        {
            shieldKey = false;
        }
    }
}
