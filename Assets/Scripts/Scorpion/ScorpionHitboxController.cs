﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ScorpionHitboxController : MonoBehaviour
{
    [SerializeField]
    private Collider2D[] owner;
    [SerializeField]
    private SmashCharacter scorp;
    private bool collision = false;
    private string enemyName = "";
    private Vector3 ledgePos = Vector3.zero;
    public int sDmg;
    public int hitStun;
    public float dmgPercent;
    public float dist;
    public float sStun;
    public Vector2 knockback;
    public bool follow;
    public bool spike;
    public bool fs;
    public Transform spot;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (enemyName != "" && follow)
        {
            SmashCharacter enemy = GameObject.Find(enemyName).GetComponent<SmashCharacter>();
            if (!enemy.shielding)
            {
                enemy.transform.position = spot.position;
                scorp.hitting = true;
                enemy.an.SetBool("attacked", true);
                enemy.setHitStun(hitStun/60.0f);
            }
        }
        //tether recovery
        else if(follow && collision && enemyName == "")
        {
            scorp.transform.position = new Vector3(scorp.transform.position.x + ledgePos.x - spot.position.x,
                scorp.transform.position.y, scorp.transform.position.z);
        }
    }

    private void OnDisable()
    {
        scorp.hitting = false;
        scorp.an.SetBool("collision", false);
        collision = false;
        if(enemyName != "")
        {
            SmashCharacter enemy = GameObject.Find(enemyName).GetComponent<SmashCharacter>();
            enemy.an.SetBool("attacked", false);
        }
        enemyName = "";
        ledgePos = Vector3.zero;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //damage character on hit
        if ((other.tag == "Character" || other.tag == "Shield") && !owner.Contains(other))
        {
            enemyName = other.attachedRigidbody.name;
            StartCoroutine(scorp.Damage(enemyName, dmgPercent, knockback, hitStun, false, dist, spike, sStun, sDmg));
            scorp.an.SetBool("collision", true);
            collision = true;
            scorp.Play("Hit");
        }
        if(other.tag == "Ledge Grab" && follow)
        {
            scorp.an.SetBool("collision", true);
            collision = true;
            ledgePos = other.transform.position;
        }
    }
}
