﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorpionFire : MonoBehaviour
{
    private float lifespan = -21f;
    private float age = 0f;
    public ScorpionController scorp;
    [HideInInspector]
    public Animator an;
    public int facing;
    public int sDmg;
    public int hitStun;
    public float dmgPercent;
    public float dist;
    public float sStun;
    public Vector2 knockback;
    public bool follow;
    public bool spike;
    public bool fs;
    // Start is called before the first frame update
    void Start()
    {
        an = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //don't do this update during replay 
        if (!UtilStaticVars.Pause && !UtilStaticVars.IsReplay)
        {
            if (an.GetCurrentAnimatorStateInfo(0).IsName("flame start"))
            {
                age -= .2f;
                transform.position = new Vector3(transform.position.x + (.2f * facing), transform.position.y - .2f, transform.position.z);
            }
            if (age < lifespan)
            {
                disappear();
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {

        //damage character on hit
        if ((other.tag == "Character" && other.GetComponent<BodyController>().player != scorp) || other.tag == "Shield")
        {
            if (other.tag == "Character" && !other.gameObject.GetComponentInParent<SmashCharacter>().dodging)
            {
                StartCoroutine(scorp.Damage(other.attachedRigidbody.name, dmgPercent, knockback, hitStun, follow, dist, spike, sStun, sDmg));
                an.SetBool("hit", true);
            }
        }
        else
        {
            an.SetBool("hit", true);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        an.SetBool("hit", true);
    }

    public void disappear()
    {
        scorp.hitting = false;
        scorp.fireOut = false;
        if (!UtilStaticVars.IsReplay)
        {
            scorp.RemoveProjectile(this.gameObject);
            Destroy(this.gameObject);
        }
    }
    private void OnDestroy()
    {
        scorp.fireOut = false;
    }
}
