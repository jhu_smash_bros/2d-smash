﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorpionController : SmashCharacter
{
    private bool midJab = false;
    private bool floating = false;
    private float fsDmg = 47.0f;
    private Vector2 fsKnockback = new Vector2(0f, 160f);
    private int fsHitStun = 5;
    public bool fireOut = false;
    public GameObject flame;
    public SkellyHand hand;

    public override void FixedUpdate()
    {
        //don't run fixed update stuff during replay
        if (UtilStaticVars.IsReplay)
        {
            return;
        }
        if (active)
        {
            bool falling = false;
            if (rb.velocity.y < 0)
            {
                falling = true;
            }
            an.SetBool("falling", falling);
            if (an.GetBool("attacked") || an.GetCurrentAnimatorStateInfo(0).IsName("hanging"))
            {
                an.SetBool("freefall", false);
            }
            base.FixedUpdate();
            //if paused  do nothing 
            if (isPaused)
            {
                return;
            }
            if (floating)
            {
                rb.gravityScale = 0;
                rb.velocity = Vector2.zero;
            }
             //jab logic
            if (an.GetCurrentAnimatorStateInfo(0).IsName("jab1"))
            {
                an.ResetTrigger("a");
                //if going to jab 2
                if (attackKey)
                {
                    an.SetTrigger("aa");
                    an.SetBool("endAttack", false);
                }
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("jab2"))
            {
                an.ResetTrigger("aa");
                if (attackKey)
                {
                    an.SetTrigger("aaa");
                    an.SetBool("endAttack", false);
                    midJab = true;
                }
                if (!midJab)
                {
                    an.SetBool("endAttack", true);
                }
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("jab3"))
            {
                an.ResetTrigger("aaa");
                an.SetBool("endAttack", true);
                midJab = false;
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("Here") || (an.GetCurrentAnimatorStateInfo(0).IsName("Get Over")
                && an.GetBool("collision")))
            {
                rb.velocity = Vector2.zero;
            }
        }
    }
    //////////////////
    //up B Functions//
    //////////////////
    public void upBTeleport()
    {
        floating = false;
        //move depending on the direction that is being held
        if (h != 0 || v != 0)
        {
            rb.velocity = new Vector2(h * SPEED.x * 10, v * SPEED.y * 5);
        }
        else
        {
            rb.velocity = new Vector2(hAlt * SPEED.x * 10, vAlt * SPEED.y * 5);
        }
        //make invinvible
        transform.Find("Body").gameObject.tag = "Untagged";
        rd.color = new Color(rd.color.r, rd.color.g, rd.color.b, 0);
        an.SetBool("freefall", true);
    }

    public void upBReappear()
    {
        //make vulnerable again
        transform.Find("Body").gameObject.tag = "Character";
        rb.velocity = new Vector2(0f, 0f);
        rd.color = new Color(rd.color.r, rd.color.g, rd.color.b, 1);
    }

    public void startFloating()
    {
        //float during first part of the up b
        floating = true;
    }
    //////////////////////
    //up B Functions end//
    //////////////////////
    
    public void setFire()
    {
        //if one fire out don't send out another
        if (!fireOut)
        {
            fireOut = true;
            GameObject fire = Instantiate(flame, new Vector3(transform.position.x + (4f * facing), transform.position.y + 7.0f, 0), Quaternion.identity);
            fire.GetComponent<ScorpionFire>().scorp = this;
            fire.GetComponent<ScorpionFire>().facing = facing;
            projList.Add(fire);
            spawnedProjectile = 1;
            //flip sprite of flame when necessary
            if(facing == -1)
            {
                fire.transform.localScale = new Vector3(fire.transform.localScale.x *-1, fire.transform.localScale.y, fire.transform.localScale.z);
            }
            Play("Fire");
        }
    }

    public override void InstantiateProjectile(int type)
    {
        if(type == 1)
        {
            setFire();
        }
    }
    public override void stillProj()
    {
        projList[0].GetComponent<ScorpionFire>().an.SetBool("hit", true);
    }

    public void endFinalSmash()
    {
        StartCoroutine(Damage(hand.enemy.gameObject.name, fsDmg, fsKnockback,fsHitStun));
        hitting = false;
        an.SetBool("final smash", false);
        an.ResetTrigger("final smash trigger");
        hand.enemy = null;
    }

}
