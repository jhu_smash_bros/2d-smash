﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkellyHand : MonoBehaviour
{
    public SmashCharacter enemy = null;
    public Transform enemyLocation;
    [SerializeField]
    private ScorpionController scorp;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (enemy != null)
        {
            enemy.transform.position = enemyLocation.position;
            enemy.rb.velocity = Vector2.zero;
            enemy.an.SetBool("attacked", true);
            scorp.hitting = true;
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Character" && enemy == null)
        {
            enemy = other.attachedRigidbody.GetComponent<SmashCharacter>();
            scorp.an.SetTrigger("final smash trigger");
            if (enemy.name.Contains("Lyn"))
            {
                scorp.Play("Finish Her");
            }
            else
            {
                scorp.Play("Finish Him");
            }
        }
    }
}
