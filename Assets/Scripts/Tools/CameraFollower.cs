﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// reference: https://www.youtube.com/watch?v=U66VYM-ShVg&list=PLzDRvYVwl53uid7zTrGwxDyfb_eSE3PWe

public class CameraFollower:MonoBehaviour {

	public bool active = true;
	public Transform[] targets;
    public GameObject gameText;

	public bool bounds;
	public Vector3 minCameraPos;
	public Vector3 maxCameraPos;

	private float h;
	private float w;
	private float cameraZoom = 40f;

    private Camera cam;
	private Vector2 minMovingCameraPos;
	private Vector2 maxMovingCameraPos;
	private bool allDead = false;

    private Stack<string> losers = new Stack<string>();

    void Awake()
	{
		cam = Camera.main;
        if (GameObject.Find("Tournament") != null)
        {
            minCameraPos = new Vector2(-54.85f, -28.28f);
            maxCameraPos = new Vector2(54.85f, 63.32f);
        }
	}

	void FixedUpdate()
	{
        h = 2f * cam.orthographicSize;
        w = h * cam.aspect;
    
	    minMovingCameraPos = new Vector2(minCameraPos.x + w/2, minCameraPos.y + h/2);
	    maxMovingCameraPos = new Vector2(maxCameraPos.x - w/2, maxCameraPos.y - h/2);

		// freeze the camera
        if (Input.GetKeyDown(KeyCode.Minus))
		{
			if (!active)
			    active = true;
			else
			    active = false;
		}

        // only keep track of characters within the min/max camera pos
        float midX = 0f;
		float midY = 0f;
	    int trackedTargets = 0;
		Vector2 minPos = new Vector2(999f, 999f);
		Vector2 maxPos = new Vector2(-999f, -999f);

        for (int i = 0; i < targets.Length; i++)
		{
            if (targets[i].GetComponent<SmashCharacter>().dead || targets[i].GetComponent<SmashCharacter>().respawning)
			    continue;

            allDead = false;

			if (targets[i].position.x > minCameraPos.x && targets[i].position.y > minCameraPos.y
			    && targets[i].position.x < maxCameraPos.x && targets[i].position.y < maxCameraPos.y)
			{
				// obtain min pos
                if (targets[i].position.x < minPos.x)
				    minPos.x = targets[i].position.x;
				if (targets[i].position.y < minPos.y)
				    minPos.y = targets[i].position.y;
                // obtain max pos
				if (targets[i].position.x > maxPos.x)
				    maxPos.x = targets[i].position.x;
				if (targets[i].position.y > maxPos.y)
				    maxPos.y = targets[i].position.y;
                // obtain mid pos
				trackedTargets++;
			    midX += targets[i].position.x;
			    midY += targets[i].position.y;
			}
			else
			{
                if (!UtilStaticVars.IsReplay)
                {
                    targets[i].GetComponent<SmashCharacter>().LoseStock();
                }
			}
		}

        int numDead = 0;
        for (int i = 0; i < targets.Length; i++)
		{
            if (targets[i].GetComponent<SmashCharacter>().dead)
			    numDead += 1;
		}
        midX /= trackedTargets;
		midY /= trackedTargets;
        
        // if everyone is dead
		if (numDead == targets.Length)
        {
            allDead = true;
			midX = 0f;
			midY = 0f;
		    cameraZoom = 30;
        }
        // if a real match and there is one player left
        else if (numDead == targets.Length - 1 && UtilStaticVars.RealGame && losers.Count > 0)
        {
            StartCoroutine(StopGame());
        }
        
		if (active)
		{
		    HandleMovement(midX, midY+5f);
		    HandleZoom(minPos, maxPos);
		}
	}

	void HandleMovement(float midX, float midY)
	{
        // move the camera based on the obtained mid position
		Vector3 cameraFollowPos = new Vector3(midX, midY, transform.position.z);
		Vector3 cameraMoveDir = (cameraFollowPos - transform.position).normalized;
		float distance = Vector3.Distance(cameraFollowPos, transform.position);
		float cameraMoveSpeed = 5f;

        if (distance > 0)
		{
		    Vector3 newCameraPos = transform.position + cameraMoveDir * distance * cameraMoveSpeed * Time.deltaTime;
			float distanceAfterMoving = Vector3.Distance(newCameraPos, cameraFollowPos);

            // check for overshooting
			if (distanceAfterMoving > distance)
			    newCameraPos = cameraFollowPos;

			// update the camera's position
            transform.position = newCameraPos;
		}

        // keep the camera in the bounds
		if (bounds)
		{
			transform.position = new Vector3(Mathf.Clamp(transform.position.x, minMovingCameraPos.x, maxMovingCameraPos.x),
			                                 Mathf.Clamp(transform.position.y, minMovingCameraPos.y, maxMovingCameraPos.y),
			                                 Mathf.Clamp(transform.position.z, minCameraPos.z, maxCameraPos.z));
		}
	}

    void HandleZoom(Vector2 minPos, Vector2 maxPos)
	{
		float distance = Vector2.Distance(minPos, maxPos);
        cameraZoom = 0.5f * distance + 5f;

        if (cameraZoom < 10f)
		    cameraZoom = 10f;

        if (cameraZoom > 30)
		    cameraZoom = 30;

		float cameraZoomDiff = cameraZoom - cam.orthographicSize;
		float cameraZoomSpeed = 5f;

	    // update how zoomed in the camera is
	    cam.orthographicSize += cameraZoomDiff * cameraZoomSpeed * Time.deltaTime;

		// prevent overshooting
		if (cameraZoomDiff > 0)
		{
			if (cam.orthographicSize > cameraZoom)
			    cam.orthographicSize = cameraZoom;
		}
		else
		{
			if (cam.orthographicSize < cameraZoom)
			    cam.orthographicSize = cameraZoom; 
		}
	}

    public void SetMinCamPos()
	{
        minCameraPos = gameObject.transform.position;
	}

	public void SetMaxCamPos()
	{
        maxCameraPos = gameObject.transform.position;
	}

    //pushes loser's info on to the loser stack
    public void AddLoser(SmashCharacter loser)
    {
        losers.Push(characterToString(loser));
    }

    //ends game during a real match
    public IEnumerator StopGame()
    {
        SmashCharacter firstPlace;

        // assigns rankings
        UtilStaticVars.Second = losers.Pop();
        if(losers.Count != 0)
        {
            UtilStaticVars.Third = losers.Pop();
        }
        if (losers.Count != 0)
        {
            UtilStaticVars.Fourth = losers.Pop();
        }

        for (int i = 0; i < targets.Length; i++)
        {
            if (!targets[i].GetComponent<SmashCharacter>().dead)
            {
                firstPlace = targets[i].GetComponent<SmashCharacter>();
                UtilStaticVars.First = characterToString(firstPlace);
                break;
            }
        }
        gameText.SetActive(true);
        GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("Game!");
        yield return new WaitForSeconds(0.2f);
        GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("Cheers");
        //GameObject.Find("FightingStage").GetComponent<ReplayData>().Print();
        yield return new WaitForSeconds(3.0f);
        UtilStaticVars.Replay = GameObject.Find("FightingStage").GetComponent<ReplayData>();
        //Debug.Log("Printed");
        SceneManager.LoadScene(2);
    }

    //helper function that puts a string that represents a smash character
    private string characterToString(SmashCharacter character)
    {
        float r = character.colorChange(-1).r;
        float b = character.colorChange(-1).b;
        if (character is LynController)
        {
            if (r == b)
            {
                if (r > 0.9f)
                {
                    return "Normal Lyn";
                }
                else
                {
                    return "Dark Lyn";
                }
            }
            else
            {
                if (r < b)
                {
                    return "Blue Lyn";
                }
                else
                {
                    return "Red Lyn";
                }
            }
        }

        else if (character is ZeroController)
        {
            if (r == b)
            {
                if (r > 0.9f)
                {
                    return "Normal Zero";
                }
                else
                {
                    return "Dark Zero";
                }
            }
            else
            {
                if (r < b)
                {
                    return "Blue Zero";
                }
                else
                {
                    return "Red Zero";
                }
            }
        }

        else if (character is ScorpionController)
        {
            if (r == b)
            {
                if (r > 0.9f)
                {
                    return "Normal Scorpion";
                }
                else
                {
                    return "Dark Scorpion";
                }
            }
            else
            {
                if (r < b)
                {
                    return "Blue Scorpion";
                }
                else
                {
                    return "Red Scorpion";
                }
            }
        }

        else if (character is ShadowController)
        {
            if (r == b)
            {
                if (r > 0.9f)
                {
                    return "Normal Shadow";
                }
                else
                {
                    return "Dark Shadow";
                }
            }
            else
            {
                if (r < b)
                {
                    return "Blue Shadow";
                }
                else
                {
                    return "Red Shadow";
                }
            }
        }
        else if (character is GuileController)
        {
            if (r == b)
            {
                if (r > 0.9f)
                {
                    return "Normal Guile";
                }
                else
                {
                    return "Dark Guile";
                }
            }
            else
            {
                if (r < b)
                {
                    return "Blue Guile";
                }
                else
                {
                    return "Red Guile";
                }
            }
        }
        else if (character is SansController)
        {
            if (r == b)
            {
                if (r > 0.9f)
                {
                    return "Normal Sans";
                }
                else
                {
                    return "Dark Sans";
                }
            }
            else
            {
                if (r < b)
                {
                    return "Blue Sans";
                }
                else
                {
                    return "Red Sans";
                }
            }
        }
        return "";
    }
}
