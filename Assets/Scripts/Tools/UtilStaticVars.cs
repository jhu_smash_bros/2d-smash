﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UtilStaticVars  {
    private static string player1, player2, player3, player4, theme, stage;
    private static bool cpu1, cpu2, cpu3, cpu4, isReplay;
    private static ReplayData replay;

    public static float Volume { get; set; } = 50f;
    public static int Stocks { get; set; } = 1;
    public static bool Pause { get; set; } = false;
    public static bool RealGame { get; set; } = false;
    public static bool ShowHitbox { get; set; } = false;
    public static bool LockInput { get; set; } = false;
    public static bool CPU1 { get => cpu1; set => cpu1 = value; }
    public static bool CPU2 { get => cpu2; set => cpu2 = value; }
    public static bool CPU3 { get => cpu3; set => cpu3 = value; }
    public static bool CPU4 { get => cpu4; set => cpu4 = value; }
    public static string Player1 { get; set; } = "";
    public static string Player2 { get; set; } = "";
    public static string Player3 { get; set; } = "";
    public static string Player4 { get; set; } = "";
    public static string First { get; set; } = "";
    public static string Second { get; set; } = "";
    public static string Third { get; set; } = "";
    public static string Fourth { get; set; } = "";
    public static string Theme { get; set; } = "";
    public static string Stage { get; set; } = "";
    public static ReplayData Replay { get => replay; set => replay = value; }
    public static bool IsReplay { get => isReplay; set => isReplay = value; }
}
