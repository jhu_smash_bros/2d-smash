﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    /*********
    PLAY MUSIC
    **********/
    public void Play(string name)
    {
        GameObject.Find("AudioManager").GetComponent<AudioManager>().Play(name);
    }
}
