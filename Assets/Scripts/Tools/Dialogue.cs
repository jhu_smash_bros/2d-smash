﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class Dialogue
{
    public string name;
    public GameObject sprite;
    public GameObject textbox;
    public TextMeshProUGUI convoText;

    [TextArea(3, 10)]
    public string[] sentences;
}
