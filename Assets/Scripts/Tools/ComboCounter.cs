﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ComboCounter : MonoBehaviour
{
    public TextMeshProUGUI counter;
    public TextMeshProUGUI percentage;

    private int percent = 0;
    private int numHits = 0;
    // Start is called before the first frame update
    void Start()
    {
        counter.text = "combo: 0";
        percentage.text = "damage: 0%";
    }

    // Update is called once per frame
    void Update()
    {
        if (UtilStaticVars.RealGame)
        {
            gameObject.SetActive(false);
            return;
        }
    }

    public void UpdateCombo(float dmg, bool combo)
    {
        if(combo)
        {
            numHits++;
            percent += (int)dmg;
            if(percent>999){
                percent = 999;
            }
        }
        else
        {
            numHits = 1;
            percent = (int)dmg;
        }
        counter.text = "combo: " + numHits;
        percentage.text = "damage: "+percent+"%";
    }
}
