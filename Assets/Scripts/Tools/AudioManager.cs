﻿using UnityEngine.Audio;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;

    // initialize the sounds
    void Awake()
    {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.loop = s.loop;
            s.source.volume = s.volume * UtilStaticVars.Volume;
        }
    }

    // play a sound
    public void Play(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
            Debug.LogWarning("SONG " + name + " NOT FOUND!");
        s.source.Play();
    }

    // stop a sound
    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
            Debug.LogWarning("SONG " + name + " NOT FOUND!");
        s.source.Stop();
    }

    // set the volume
    public void SetVolume(string name, float value)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
            Debug.LogWarning("SONG " + name + " NOT FOUND!");
        s.source.volume = value;
    }

    //sets volume to the static vars value
    public void VolumeAdjustment()
    {
        foreach (Sound s in sounds)
        {
            s.source.volume = s.volume * UtilStaticVars.Volume;
        }
    }
}
