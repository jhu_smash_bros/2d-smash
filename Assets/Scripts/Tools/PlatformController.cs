﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    private Dictionary<SmashCharacter,float> scDict;

    void Start()
    {
        scDict = new Dictionary<SmashCharacter,float>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Feet")
        {
            SmashCharacter sc = GameObject.Find(other.attachedRigidbody.name).GetComponent<SmashCharacter>();
            if (!scDict.ContainsKey(sc)) scDict.Add(sc,0f);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Feet")
        {
            SmashCharacter sc = GameObject.Find(other.attachedRigidbody.name).GetComponent<SmashCharacter>();
            if (scDict.ContainsKey(sc) && scDict[sc] == 0f) scDict.Remove(sc);
        }
    }

    void FixedUpdate()
    {
        if (scDict == null)
        {
            scDict = new Dictionary<SmashCharacter,float>();
        }
        else
        {
            List<SmashCharacter> keys = new List<SmashCharacter>(scDict.Keys);

            foreach(SmashCharacter sc in keys)
            {
                // go through platform when crouching
                if (sc.an.GetCurrentAnimatorStateInfo(0).IsName("crouch"))
                {
                    scDict[sc] += Time.deltaTime;
                    if (scDict[sc] > 0.3f)
                    {
                        sc.dropping = true;
                        sc.transform.Find("Feet").GetComponent<Collider2D>().enabled = false;
                        scDict[sc] = 0.5f;
                    }
                }
                else
                {
                    if (!sc.dropping) scDict[sc] = 0f;
                }
                // set a timer to go through platform
                if (scDict[sc] > 0.4f)
                {
                    scDict[sc] += Time.deltaTime;
                }
                if (scDict[sc] > 0.8f)
                {
                    scDict.Remove(sc);
                    sc.dropping = false;
                    sc.transform.Find("Feet").GetComponent<Collider2D>().enabled = true;
                }
                // don't keep track of dead characters
                if (sc.dead)
                    scDict.Remove(sc);
            }
        }
    }
}
