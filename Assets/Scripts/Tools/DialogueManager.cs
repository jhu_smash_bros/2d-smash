﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueManager : MonoBehaviour
{
    private Dialogue dia;
    private Queue<string> conversation;

    void Start()
    {
        conversation = new Queue<string>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N) || Input.GetButtonDown("Attack"))
            DisplayNextSentence(); 
    }

    public void StartDialogue(Dialogue dialogue)
    {
        conversation.Clear();

        dia = dialogue;
        dialogue.textbox.SetActive(true);

        foreach (string sentence in dia.sentences)
            conversation.Enqueue(sentence);
        UtilStaticVars.LockInput = true;
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (conversation.Count == 0)
        {
            EndDialogue();
            return;
        }

        string sentence = conversation.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence(string sentence)
    {
        if (sentence != "")
            dia.textbox.SetActive(true);
        else
            dia.textbox.SetActive(false);
            

        dia.convoText.text = "";
        foreach (char c in sentence.ToCharArray())
        {
            dia.convoText.text += c;
            if (dia.sprite != null)
                dia.sprite.GetComponent<Animator>().SetBool("talking", true);
            yield return null;
        }
        if (dia.sprite != null)
            dia.sprite.GetComponent<Animator>().SetBool("talking", false);
    }

    public void EndDialogue()
    {
        Debug.Log("NO MORE");
        gameObject.GetComponent<Stage>().dialogueCounter += 1;
        gameObject.GetComponent<Stage>().dialogueStarted = true;
        dia.textbox.SetActive(false);
        UtilStaticVars.LockInput = false;
    }
}
