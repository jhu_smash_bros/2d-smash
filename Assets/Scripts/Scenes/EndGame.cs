﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    public Transform pos1;
    public Transform pos2;
    public Transform pos3;
    public Transform pos4;

    public SmashCharacter lyn;
    public SmashCharacter zero;
    public SmashCharacter shadow;
    public SmashCharacter scorpion;
    public SmashCharacter guile;
    public SmashCharacter sans;

    public GameObject SaveReplayText;

    private SmashCharacter first;
    private SmashCharacter last;
    private ReplayData replayData;

    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<AudioManager>().Play("Theme");

        first = placeContenders(1);
        placeContenders(2);
        placeContenders(3);
        last = placeContenders(4);
        first.GetComponent<Animator>().SetBool("win", true);
        replayData = UtilStaticVars.Replay;
        Cursor.visible = true;
        if (UtilStaticVars.IsReplay)
        {
            SaveReplayText.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (last != null)
        {
            last.GetComponent<Animator>().SetBool("stunned", true);
            if (!last.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("stunned"))
            {
                last.GetComponent<Animator>().Play("stunned");
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UtilStaticVars.Player1 = "";
            UtilStaticVars.Player2 = "";
            UtilStaticVars.Player3 = "";
            UtilStaticVars.Player4 = "";
            UtilStaticVars.CPU1 = false;
            UtilStaticVars.CPU2 = false;
            UtilStaticVars.CPU3 = false;
            UtilStaticVars.CPU4 = false;
            UtilStaticVars.IsReplay = false;
            SceneManager.LoadScene(0);
        }
    }

    public SmashCharacter placeContenders(int place)
    {
        Color col;
        SmashCharacter player = null;
        switch (place)
        {
            case 1: 
                if(UtilStaticVars.First != "")
                {
                    if (UtilStaticVars.First.Contains("Normal"))
                    {
                        col = new Color(1, 1, 1);
                    }
                    else if (UtilStaticVars.First.Contains("Dark"))
                    {
                        col = new Color(100f / 255f, 100f / 255f, 100f / 255f);
                    }
                    else if (UtilStaticVars.First.Contains("Blue"))
                    {
                        col = new Color(100f / 255f, 100f / 255f, 255f / 255f);
                    }
                    else
                    {
                        col = new Color(255f / 255f, 100f / 255f, 100f / 255f);
                    }

                    if (UtilStaticVars.First.Contains("Lyn"))
                    {
                        player = Instantiate(lyn, pos1.position, Quaternion.identity);
                        player.GetComponent<LynController>().enabled = false;
                    }
                    else if (UtilStaticVars.First.Contains("Shadow"))
                    {
                        player = Instantiate(shadow, pos1.position, Quaternion.identity);
                        player.GetComponent<ShadowController>().enabled = false;
                    }
                    else if (UtilStaticVars.First.Contains("Zero"))
                    {
                        player = Instantiate(zero, pos1.position, Quaternion.identity);
                        player.GetComponent<ZeroController>().enabled = false;
                    }
                    else if (UtilStaticVars.First.Contains("Scorpion"))
                    {
                        player = Instantiate(scorpion, pos1.position, Quaternion.identity);
                        player.GetComponent<ScorpionController>().enabled = false;
                    }
                    else if (UtilStaticVars.First.Contains("Guile"))
                    {
                        player = Instantiate(guile, pos1.position, Quaternion.identity);
                        player.GetComponent<GuileController>().enabled = false;
                    }
                    else
                    {
                        player = Instantiate(sans, pos1.position, Quaternion.identity);
                        player.GetComponent<SansController>().enabled = false;
                    }
                    player.GetComponent<SpriteRenderer>().color = col;
                    player.gameObject.SetActive(true);
                }
                break;
            case 2:
                if (UtilStaticVars.Second != "")
                {
                    if (UtilStaticVars.Second.Contains("Normal"))
                    {
                        col = new Color(1, 1, 1);
                    }
                    else if (UtilStaticVars.Second.Contains("Dark"))
                    {
                        col = new Color(100f / 255f, 100f / 255f, 100f / 255f);
                    }
                    else if (UtilStaticVars.Second.Contains("Blue"))
                    {
                        col = new Color(100f / 255f, 100f / 255f, 255f / 255f);
                    }
                    else
                    {
                        col = new Color(255f / 255f, 100f / 255f, 100f / 255f);
                    }

                    if (UtilStaticVars.Second.Contains("Lyn"))
                    {
                        player = Instantiate(lyn, pos2.position, Quaternion.identity);
                        player.GetComponent<LynController>().enabled = false;
                    }
                    else if (UtilStaticVars.Second.Contains("Shadow"))
                    {
                        player = Instantiate(shadow, pos2.position, Quaternion.identity);
                        player.GetComponent<ShadowController>().enabled = false;
                    }
                    else if (UtilStaticVars.Second.Contains("Zero"))
                    {
                        player = Instantiate(zero, pos2.position, Quaternion.identity);
                        player.GetComponent<ZeroController>().enabled = false;
                    }
                    else if (UtilStaticVars.Second.Contains("Scorpion"))
                    {
                        player = Instantiate(scorpion, pos2.position, Quaternion.identity);
                        player.GetComponent<ScorpionController>().enabled = false;
                    }
                    else if (UtilStaticVars.Second.Contains("Guile"))
                    {
                        player = Instantiate(guile, pos2.position, Quaternion.identity);
                        player.GetComponent<GuileController>().enabled = false;
                    }
                    else
                    {
                        player = Instantiate(sans, pos2.position, Quaternion.identity);
                        player.GetComponent<SansController>().enabled = false;
                    }
                    player.GetComponent<SpriteRenderer>().color = col;
                    player.gameObject.SetActive(true);
                }
                break;
            case 3:
                if (UtilStaticVars.Third != "")
                {
                    if (UtilStaticVars.Third.Contains("Normal"))
                    {
                        col = new Color(1, 1, 1);
                    }
                    else if (UtilStaticVars.Third.Contains("Dark"))
                    {
                        col = new Color(100f / 255f, 100f / 255f, 100f / 255f);
                    }
                    else if (UtilStaticVars.Third.Contains("Blue"))
                    {
                        col = new Color(100f / 255f, 100f / 255f, 255f / 255f);
                    }
                    else
                    {
                        col = new Color(255f / 255f, 100f / 255f, 100f / 255f);
                    }

                    if (UtilStaticVars.Third.Contains("Lyn"))
                    {
                        player = Instantiate(lyn, pos3.position, Quaternion.identity);
                        player.GetComponent<LynController>().enabled = false;
                    }
                    else if (UtilStaticVars.Third.Contains("Shadow"))
                    {
                        player = Instantiate(shadow, pos3.position, Quaternion.identity);
                        player.GetComponent<ShadowController>().enabled = false;
                    }
                    else if (UtilStaticVars.Third.Contains("Zero"))
                    {
                        player = Instantiate(zero, pos3.position, Quaternion.identity);
                        player.GetComponent<ZeroController>().enabled = false;
                    }
                    else if (UtilStaticVars.Third.Contains("Scorpion"))
                    {
                        player = Instantiate(scorpion, pos3.position, Quaternion.identity);
                        player.GetComponent<ScorpionController>().enabled = false;
                    }
                    else if (UtilStaticVars.Third.Contains("Guile"))
                    {
                        player = Instantiate(guile, pos3.position, Quaternion.identity);
                        player.GetComponent<GuileController>().enabled = false;
                    }
                    else
                    {
                        player = Instantiate(sans, pos3.position, Quaternion.identity);
                        player.GetComponent<SansController>().enabled = false;
                    }
                    player.GetComponent<SpriteRenderer>().color = col;
                    player.gameObject.SetActive(true);
                }
                break;
            case 4:
                if (UtilStaticVars.Fourth != "")
                {
                    if (UtilStaticVars.Fourth.Contains("Normal"))
                    {
                        col = new Color(1, 1, 1);
                    }
                    else if (UtilStaticVars.Fourth.Contains("Dark"))
                    {
                        col = new Color(100f / 255f, 100f / 255f, 100f / 255f);
                    }
                    else if (UtilStaticVars.Fourth.Contains("Blue"))
                    {
                        col = new Color(100f / 255f, 100f / 255f, 255f / 255f);
                    }
                    else
                    {
                        col = new Color(255f / 255f, 100f / 255f, 100f / 255f);
                    }

                    if (UtilStaticVars.Fourth.Contains("Lyn"))
                    {
                        player = Instantiate(lyn, pos4.position, Quaternion.identity);
                        player.GetComponent<LynController>().enabled = false;
                    }
                    else if (UtilStaticVars.Fourth.Contains("Shadow"))
                    {
                        player = Instantiate(shadow, pos4.position, Quaternion.identity);
                        player.GetComponent<ShadowController>().enabled = false;
                    }
                    else if (UtilStaticVars.Fourth.Contains("Zero"))
                    {
                        player = Instantiate(zero, pos4.position, Quaternion.identity);
                        player.GetComponent<ZeroController>().enabled = false;
                    }
                    else if (UtilStaticVars.Fourth.Contains("Scorpion"))
                    {
                        player = Instantiate(scorpion, pos4.position, Quaternion.identity);
                        player.GetComponent<ScorpionController>().enabled = false;
                    }
                    else if (UtilStaticVars.Fourth.Contains("Guile"))
                    {
                        player = Instantiate(guile, pos4.position, Quaternion.identity);
                        player.GetComponent<GuileController>().enabled = false;
                    }
                    else
                    {
                        player = Instantiate(sans, pos4.position, Quaternion.identity);
                        player.GetComponent<SansController>().enabled = false;
                    }
                    player.GetComponent<SpriteRenderer>().color = col;
                    player.gameObject.SetActive(true);
                }
                break;
            default:
                break;
        }
        return player;
    }

    public void saveReplay()
    {
        replayData.Print();
    }
}
