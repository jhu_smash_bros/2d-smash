
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ZeroIntroPart2 : Stage
{
    public GameObject Part1;
    public SmashCharacter Zero;
    public SmashCharacter Sans;
    public Dialogue[] Conversation;
    private int sentenceCounter = 0;

    public override void Start()
    {
        base.Start();
        Zero.transform.position = new Vector3(-18.58f, 1.5f, -2);
    }

    public override void Update()
    {
        base.Update();

        if (dialogueStarted)
        {
            // make sure Sans is sitting
            if (dialogueCounter == -1)
                Sans.an.SetBool("sitting", true);

            // once the scene starts
            if (dialogueCounter == 0)
            {
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("SansTheme");
                dialogueCounter += 1;
            }
            // there are no more dialogues left
            else if (dialogueCounter - 5 == Conversation.Length)
            {
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Stop("SansTheme");
                Conversation[dialogueCounter - 6].textbox.SetActive(false);
                // go back to menu screen
                SceneManager.LoadScene(0);
            }
            // dialogue does't start till later
            else if (dialogueCounter >= 5)
            {
                FindObjectOfType<DialogueManager>().StartDialogue(Conversation[dialogueCounter - 5]);
                if (dialogueCounter >= 6)
                    Conversation[dialogueCounter - 6].textbox.SetActive(false);
            }

            // zero teleports in
            if (dialogueCounter == 2)
            {
                Zero.transform.position = new Vector3(-18.58f, 1.5f, 0);
                Zero.an.SetTrigger("teleportin");
                dialogueCounter += 1;
            }

            if (dialogueCounter == 3)
            {
                Sans.an.SetBool("sit left", true);
                dialogueCounter += 1;
            }

            sentenceCounter = 0;
            dialogueStarted = false;
        }

        // some action from Sans
        if (dialogueCounter == 13)
        {
            // make him drink some ketchup
            if (sentenceCounter == 2)
            {
                Sans.an.SetTrigger("taunt");
                sentenceCounter += 1;
            }
            // make him show his back
            if (sentenceCounter == 3)
            {
                Sans.an.SetBool("sit left", false);
            }
            // make him scratch his but
            if (sentenceCounter == 5)
            {
                Sans.an.SetTrigger("scratch");
            }
        }



        // counts the sentence you are on
        if (Input.GetKeyDown(KeyCode.N))
            sentenceCounter += 1;
    }
}
