﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Tutorial : Stage
{
    public bool dialogueTrigger;
    public TextMeshProUGUI helpText;
    public LynController lyn;
    public LynController darkLyn;
    public SansController sans;
    public GameObject specialPlat;
    public GameObject darkLynProf;
    private GameObject sansHurtBox = null;
    private int part = 1;
    private bool controller = false;

    //part 1
    public Dialogue introWalk;
    private bool moveLeft = false;
    private bool moveRight = false;

    //part2
    public Dialogue jumping;
    private bool jump1 = false;
    private bool jump2 = false;

    //part 3
    public Dialogue recovery;
    private bool wrapAround = false;

    //part 5
    public Dialogue recovery2;

    //part 6
    public Dialogue specials;
    private bool upB = false;
    private bool neutralB = false;
    private bool sideB = false;
    private bool downB = false;

    //part 7
    public Dialogue normals;
    private bool jab = false;
    private bool dashAttack = false;
    private bool jumpAttack = false;

    //part 8
    public Dialogue defense;
    [HideInInspector]
    public bool shotBlocked = false;
    private int shotsToBlock = 3;

    //part 9
    public Dialogue dodge;
    [HideInInspector]
    public bool shotDodged = false;
    private int shotsToDodge = 3;

    //part 10
    public Dialogue finalSmash;
    
    //part 11
    public Dialogue end;

    public override void Start()
    {
        UtilStaticVars.Theme = "";
        base.Start();
        dialogueTrigger = true;
        dialogueStarted = true;
        UtilStaticVars.RealGame = false;
        sansHurtBox = sans.gameObject.GetComponentInChildren<BodyController>().gameObject;
        sansHurtBox.SetActive(false);
    }
    private void OnGUI()
    {
        if (Event.current != null && Event.current.isKey)
        {
            controller = false;
        }
    }
    // Update is called once per frame
    public override void Update()
    {
        if(Mathf.Abs(Input.GetAxis("HorizontalAlt")) > .2f || Mathf.Abs(Input.GetAxis("VerticalAlt")) > .2f)
        {
            controller = true;
        }
        for (int i = 0; i < 20; i++)
        {
            if (Input.GetKeyDown("joystick 1 button " + i))
            {
                controller = true;
            }
        }
        if (dialogueStarted && dialogueTrigger)
        {
            switch (part)
            {
                case 1:
                    darkLyn.gameObject.SetActive(false);
                    darkLynProf.gameObject.SetActive(false);
                    FindObjectOfType<DialogueManager>().StartDialogue(introWalk);
                    break;
                case 2:
                    FindObjectOfType<DialogueManager>().StartDialogue(jumping);
                    break;
                case 3:
                    FindObjectOfType<DialogueManager>().StartDialogue(recovery);
                    sans.gameObject.GetComponent<SansController>().enabled = false;
                    break;
                //case 4 has no dialogue so it is skipped
                case 5:
                    FindObjectOfType<DialogueManager>().StartDialogue(recovery2);
                    specialPlat.gameObject.SetActive(true);
                    lyn.transform.position = new Vector3(53f,-25f);
                    break;
                case 6:
                    FindObjectOfType<DialogueManager>().StartDialogue(specials);
                    break;
                case 7:
                    FindObjectOfType<DialogueManager>().StartDialogue(normals);
                    break;
                case 8:
                    FindObjectOfType<DialogueManager>().StartDialogue(defense);
                    break;
                case 9:
                    FindObjectOfType<DialogueManager>().StartDialogue(dodge);
                    break;
                case 10:
                    FindObjectOfType<DialogueManager>().StartDialogue(finalSmash);
                    break;
                case 11:
                    FindObjectOfType<DialogueManager>().StartDialogue(end);
                    break;
                default:
                    break;
            }
            dialogueTrigger = false;
            dialogueStarted = false;
        }
        if (!dialogueStarted && !dialogueTrigger)
        {
            helpText.text = "press \"N\" or the a button\n to continue.";
        }
        else {
            checkPart();
        }
    }

    private void checkPart()
    {
        string butt;
        switch (part)
        {
            case 1:
                helpText.text = controller? "use the analog\nstick to move" : "press \"A\" and \"D\" to\nmove.";
                if (lyn.rb.velocity.x > .01)
                {
                    moveRight = true;
                }
                else if(lyn.rb.velocity.x < -.01)
                {
                    moveLeft = true;
                }
                if(moveLeft && moveRight)
                {
                    part = 2;
                    dialogueTrigger = true;
                }
                break;
            case 2:
                helpText.text = controller ? "press x or y\n button to jump" : "press \"I\" to jump";
                if (Input.GetButtonDown("Jump"))
                {
                    if(!lyn.doubleJumping)
                    {
                        jump1 = true;
                    }
                    else
                    {
                        jump2 = true;
                    }
                }
                else if (jump1&&jump2)
                {
                    part = 3;
                    dialogueTrigger = true;
                }
                break;
            case 3:
                sans.rb.velocity = new Vector2(-14f, 0f);
                sans.rb.gravityScale = 0f;
                sans.respawning = false;
                sans.an.SetFloat("speed", 2);
                UtilStaticVars.LockInput = true;
                if (sans.rb.position.x < -83.08141f)
                {
                    sans.rb.position = new Vector2(83.08141f, -17.852f);
                    wrapAround = true;
                }
                else if (wrapAround && sans.rb.position.x < 0f)
                {
                    sans.an.SetFloat("speed", 0);
                    sans.rb.gravityScale = 1f;
                    sans.rb.velocity = new Vector2(0f, 0f);
                    sans.gameObject.GetComponent<SansController>().enabled = true;
                    UtilStaticVars.LockInput = false;
                    part = 4;
                }
                break;
            case 4:
                helpText.text = controller ? "press up and the b\n button to up special" : "press \"W\" + \"L\" to do an\nup special";
                if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("up b 3"))
                {
                    dialogueTrigger = true;
                    part = 5;
                }
                break;
            case 5:
                helpText.text = "Make it back on\nthe stage";
                if(lyn.transform.position.x < 38f && lyn.transform.position.y > -18.5f)
                {
                    specialPlat.gameObject.SetActive(false);
                    part = 6;
                    dialogueTrigger = true;
                }
                break;
            case 6:
                butt = controller ? "b button" : "\"L\""; 
                helpText.text =  $"Use {butt} to use the 4\nspecials up, side, down\nand neutral";
                if (upB && sideB && downB && neutralB)
                {
                    part = 7;
                    dialogueTrigger = true;
                }
                if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("up b 3"))
                {
                    upB = true;
                }
                if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("down b 1"))
                {
                    downB = true;
                }
                if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("side b 1"))
                {
                    sideB = true;
                }
                if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("neutral b 4"))
                {
                    neutralB = true;
                }
                break;
            case 7:
                butt = controller ? "a button" : "\"J\"";
                helpText.text = $"Use {butt} to use the 3\nnormals jab, sideAttack\nand jumping attack";
                if (jab && dashAttack && jumpAttack)
                {
                    part = 8;
                    dialogueTrigger = true;
                }
                if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("pummel 3"))
                {
                    jab = true;
                }
                if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("dash attack"))
                {
                    dashAttack = true;
                }
                if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("nair"))
                {
                    jumpAttack = true;
                }
                break;
            case 8:
                helpText.text = $"block {shotsToBlock} arrows";
                darkLynProf.gameObject.SetActive(true);
                darkLyn.gameObject.SetActive(true);
                GameObject.FindObjectOfType<CameraFollower>().targets[2] = darkLyn.transform;
                if (shotBlocked)
                {
                    shotsToBlock--;
                    shotBlocked = false;
                }
                else if(shotsToBlock <= 0)
                {
                    shotDodged = false;
                    part = 9;
                    dialogueTrigger = true;
                }
                break;
            case 9:
                helpText.text = $"dodge {shotsToDodge} arrows";
                if (shotDodged)
                {
                    shotsToDodge--;
                    shotDodged = false;
                }
                else if (shotsToDodge <= 0)
                {
                    part = 10;
                    dialogueTrigger = true;
                }
                break;
            case 10:
                butt = controller ? "the z button" : "\"O\"";
                helpText.text = $"Press {butt} to use the\nfinal smash ";
                darkLyn.isCPU = false;
                lyn.permaSmash = true;
                if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("final smash 5"))
                {
                    part = 11;
                    dialogueTrigger = true;
                }
                break;
            case 11:
                SceneManager.LoadScene(0);
                break;
            default:
                break;
        }
    }
}
