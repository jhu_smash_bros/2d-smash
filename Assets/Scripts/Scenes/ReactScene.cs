﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReactScene : Stage
{
    public SmashCharacter Lyn;
    public SmashCharacter Sans;
    public SmashCharacter Zero;

    public GameObject LynFace;
    public GameObject SansFace;

    public Dialogue[] Conversation;
    private int sentenceCounter = 0;

    // dc: 0
    private string[] sans1 = new string[]
    {
        "hehe . . . that last video\nwas hilarious!", // laughing face (sc: 0)
        "zero probably didn’t see\nthat coming.",
        ". . . ",
        "i wonder what tae’s gonna\nmake me go through this\ntime . . . ", // regular face (sc: 3)
        "", // sans comb (sc: 4)
        "~ haha... kinda nervous... ~", // nervous face (sc: 5)
        "", // zero teleports in (sc: 6 +1) & sans turn right (sc: 7)
        "oh, welcome back."
    };

    // dc: 1
    private string[] zero1 = new string[] {". . . "};

    // dc: 2
    private string[] sans2 = new string[] {"gonna give me the silent\ntreatment again?"};

    // dc: 3
    private string[] zero2 = new string[] {". . . "};

    // dc: 4
    private string[] sans3 = new string[]
    {
        ". . . ",
        "ok.", // turn back (sc: 1)
        "", // scratch butt (sc: 2)
        "by the way, did you see\nthat new Nintendo Direct?" // laughing face (sc: 3)
    };

    // dc: 5
    private string[] zero3 = new string[] {". . . ", "~ hides his smile ~"};

    // dc: 6
    private string[] sans4 = new string[]
    {
        "", // stop butt & turn right (sc: 0)
        "hehe . . . so you did see\nit.", // wink face (sc: 1)
        "it finally happened!",
        "we’ve finally become\nPLAYABLE (as skins).",
        "i bet tae’s having the time\nof his life.", // wink face (sc: 4)
        "", // drink (sc: 5 +1)
        "all the characters he ever\nwanted in SMASH are\nfinally-",
        "", // lyn jumps in (sc: 8)
        "", // lyn walks to side (sc: 9)
        "oops . . ." // surprised face (sc: 10)
    };

    // dc: 7
    private string[] lyn1 = new string[]
    {
        ". . . ", // sad face (sc: 0)
        "Hello everyone."
    };

    // dc: 8
    private string[] sans5 = new string[] {"hey lyn . . . you doing\nalright?"}; // turn left & nervous face (sc: 0)

    // dc: 9
    private string[] lyn2 = new string[] {". . . ", "Yah . . . "};

    // dc: 10
    private string[] sans6 = new string[]
    {
        "it’s not THAT bad!", // regular face (sc: 0)
        "at least you’re a trophy!",
        "and you have been in the\ngame for so long!",
        "i was never anything to\nbegin with!"
    };

    // dc: 11
    private string[] lyn3 = new string[] {". . . ", "Yah . . . "};

    // dc: 12
    private string[] zero4 = new string[] {". . . ", "We were once fellow trophies, huh?"};

    // dc: 13
    private string[] sans7 = new string[] {". . . "}; // turn right & nervous face (sc: 0)

    // dc: 14
    private string[] lyn4 = new string[] {". . . "};
    
    // dc: 15
    private string[] sans8 = new string[]
    {
        ". . . ", // nervous face (sc: 0)
        "why don’t we change the\nsubject!",
        "zero, what was your fav-"
    };

    // dc: 16
    private string[] zero5 = new string[]
    {
        "Right, Lyn?",
        "~ DOES NOT hide his smile ~"
    };

    // dc: 17
    private string[] sans9 = new string[]
    {
        "zero… maybe you should-",
        "", // lyn up b's up (sc: 1)
        "oh, oh . . . " // surprised face (sc: 2)
    };

    // dc: 18
    private string[] zero6 = new string[] {". . . "};

    // dc: 19
    private string[] sans10 = new string[]
    {
        "i’m just gonna go on a\nshort walk . . . ",
        "", // sans walks to the door (sc: 1)
        "zero, get ready for a-",
        "BAD TIME!" // bad time face (sc: 3)
    };

    // dc: 20
    private string[] lyn5 = new string[] {"Prepare yourself, Zero!"};

    // dc: 21
    private string[] zero7 = new string[]
    {
        "HYA!",
        "", // lyn final smashes zero away (sc: 1)
        "" // lyn walks to center (sc: 2)
    };

    // dc: 22
    private string[] lyn6 = new string[] {"Ahh, I feel great!"};

    // dc: 23
    private string[] sans11 = new string[]
    {
        "haha . . . ", // nervous face (sc: 0)
        "i’m glad . . . ",
        ". . . ",
        "well, that’s all folks!", // laughing face (sc: 3)
        "sakurai, i wish you the best\nas well!",
        "just look out for some\nSALTY fans . . . ", // winking face (sc: 5)
        ""
    };

    // NEW dc: 24
    private string[] sans12 = new string[]
    {
        "",
        ". . . ",
        ". . . ", // laughing face (sc: 2)
        "well, that’s all folks!", // laughing face (sc: 3)
        ""
    };

    private int[] characterOrder = new int[] {1,2,1,2,1,2,1,0,1,0,1,0,2,1,0,1,2,1,2,1,0,2,0,1,1};

    private Queue<string[]> lynLines;
    private Queue<string[]> zeroLines;
    private Queue<string[]> sansLines;

    public override void Start()
    {
        base.Start();
        lynLines = new Queue<string[]> (new[] {lyn1, lyn2, lyn3, lyn4, lyn5, lyn6});
        zeroLines = new Queue<string[]> (new[] {zero1, zero2, zero3, zero4, zero5, zero6, zero7});
        sansLines = new Queue<string[]> (new[] {sans1, sans2, sans3, sans4, sans5, sans6, sans7, sans8, sans9, sans10, sans11, sans12});
    }

    public override void Update()
    {
        base.Update();

        if (dialogueStarted)
        {
            switch (dialogueCounter)
            {
                // make sure all characters are positioned correctly
                case -2:
                    Sans.ReverseImage();
                    Zero.ReverseImage();
                    Sans.an.SetBool("sitting", true);
                    break;

                // once the scene starts
                case -1:
                    Sans.Play("Theme");
                    break;

                // restart the scene once it is finished
                case 25:
                    Lyn.transform.position = new Vector2(2.63f, -27.5f);
                    Zero.transform.position = new Vector2(50.0f, 1.607f);
                    Sans.transform.position = new Vector2(-7.94642f, 1.607f);
                    Conversation[characterOrder[dialogueCounter-1]].textbox.SetActive(false);
                    lynLines = new Queue<string[]> (new[] {lyn1, lyn2, lyn3, lyn4, lyn5, lyn6});
                    zeroLines = new Queue<string[]> (new[] {zero1, zero2, zero3, zero4, zero5, zero6, zero7});
                    sansLines = new Queue<string[]> (new[] {sans1, sans2, sans3, sans4, sans5, sans6, sans7, sans8, sans9, sans10, sans11});

                    Zero.ReverseImage();
                    dialogueCounter = -3;
                    Sans.Stop("BadTime");
                    break;

                // there is still some dialogue
                default:
                    int charNum = characterOrder[dialogueCounter];

                    if (dialogueCounter > 0)
                        Conversation[characterOrder[dialogueCounter-1]].textbox.SetActive(false);

                    switch (charNum)
                    {
                        case 0:
                            Conversation[charNum].sentences = lynLines.Dequeue();
                            break;
                        case 1:
                            Conversation[charNum].sentences = sansLines.Dequeue();
                            break;
                        case 2:
                            Conversation[charNum].sentences = zeroLines.Dequeue();
                            break;
                    }

                    FindObjectOfType<DialogueManager>().StartDialogue(Conversation[charNum]);
                    break;
            }
            sentenceCounter = 0;
            dialogueStarted = false;
        }
        // counts the sentence you are on
        if (Input.GetKeyDown(KeyCode.N))
            sentenceCounter += 1;

        // Zero teleport in
        if (dialogueCounter == 0 && sentenceCounter == 6)
        {
            Zero.transform.position = new Vector2(21.1f, 1.607f);
            Zero.an.SetTrigger("teleportin");
            sentenceCounter += 1;
        }
        // Zero walks in
        if (dialogueCounter == 0 && sentenceCounter == 8)
        {
            if (Zero.transform.position.x > 0.41f)
                Zero.rb.velocity = new Vector2(-10f, 0f);
            else
                Zero.rb.velocity = new Vector2(0f, 0f);
        }
        // Zero dies
        if (dialogueCounter == 21 && sentenceCounter == 1 && Zero.transform.position.x > 40f)
        {
            Zero.Play("Zerodeath");
            sentenceCounter += 1;
        }

        // Lyn sad face
        if (sentenceCounter == 0 && (dialogueCounter == 7 || dialogueCounter == 9 || dialogueCounter == 11 || dialogueCounter == 14))
            LynFace.GetComponent<Animator>().SetBool("sad", true);
        // Lyn taunt
        if (dialogueCounter == 22 && sentenceCounter == 0)
        {
            Lyn.an.SetTrigger("taunt");
            sentenceCounter += 1;
        }
        // Lyn walks in
        if (dialogueCounter == 6 && sentenceCounter == 9)
        {
            if (Lyn.transform.position.x > -18.88f)
                Lyn.rb.velocity = new Vector2(-20f, 0f);
            else
            {
                Lyn.rb.velocity = new Vector2(0f, 0f);
                Lyn.ReverseImage();
                sentenceCounter += 1;
            }
        }
        if (dialogueCounter == 21 && sentenceCounter == 3)
        {
            if (Lyn.transform.position.x > -0.3f)
                Lyn.rb.velocity = new Vector2(-10f, 0f);
            else
            {
                Lyn.rb.velocity = new Vector2(0f, 0f);
            }
        }

        // Sans butt
        if (dialogueCounter == 4 && sentenceCounter == 2)
            Sans.an.SetBool("butt", true);
        if (dialogueCounter == 4 && sentenceCounter == 3)
            Sans.an.SetBool("butt", false);
        // Sans comb
        if (dialogueCounter == 0 && sentenceCounter == 4)
            Sans.an.SetBool("comb", true);
        if (dialogueCounter == 0 && sentenceCounter == 7)
            Sans.an.SetBool("comb", false);
        // Sans ketchup
        if (dialogueCounter == 6 && sentenceCounter == 5)
        {
            Sans.an.SetTrigger("taunt");
            sentenceCounter += 1;
        }
        // Sans turn head
        if (dialogueCounter == 0 && sentenceCounter == 7
            || dialogueCounter == 6 && sentenceCounter == 0
            || dialogueCounter == 8 && sentenceCounter == 0)
            Sans.an.SetBool("sit left", true);
        if (dialogueCounter == 4 && sentenceCounter == 1
            || dialogueCounter == 19 && sentenceCounter == 0)
            Sans.an.SetBool("sit left", false);
        if (dialogueCounter == 6 && sentenceCounter == 10
            || dialogueCounter == 13 && sentenceCounter == 0
            || dialogueCounter == 15 && sentenceCounter == 0
            || dialogueCounter == 15 && sentenceCounter == 2
            || dialogueCounter == 19 && sentenceCounter == 0)
        {
            Sans.ReverseImage();
            sentenceCounter += 1;
        }
        // Sans walks away
        if (dialogueCounter == 19 && sentenceCounter == 1)
            Sans.an.SetBool("sitting", false);
        if (dialogueCounter == 19 && sentenceCounter == 2)
        {
            if (Sans.transform.position.x > -19f)
                Sans.rb.velocity = new Vector2(-5f, 0f);
            else
                Sans.rb.velocity = new Vector2(0f, 0f);
        }
        if (dialogueCounter == 24 && sentenceCounter == 0)
        {
            Sans.Stop("BadTime");
            Sans.transform.position = new Vector2(0.5f, 1.607f);
        }
        // Sans winking face
        if (dialogueCounter == 4 && sentenceCounter == 3
            || dialogueCounter == 6 && sentenceCounter == 1
            || dialogueCounter == 6 && sentenceCounter == 4
            || dialogueCounter == 10 && sentenceCounter == 0
            || dialogueCounter == 15 && sentenceCounter == 3
            || dialogueCounter == 19 && sentenceCounter == 2
            || dialogueCounter == 23 && sentenceCounter == 5
            || dialogueCounter == 24 && sentenceCounter == 3)
            SansFace.GetComponent<Animator>().SetTrigger("Wink");
        // Sans regular face
        if (dialogueCounter == 0 && sentenceCounter == 3
            || dialogueCounter == 10 && sentenceCounter == 1
            || dialogueCounter == 15 && sentenceCounter == 4)
            SansFace.GetComponent<Animator>().SetTrigger("Reset");
        // Sans smiling face
        if (dialogueCounter == 0 && sentenceCounter == 0
            || dialogueCounter == 6 && sentenceCounter == 2
            || dialogueCounter == 6 && sentenceCounter == 7
            || dialogueCounter == 23 && sentenceCounter == 3
            || dialogueCounter == 24 && sentenceCounter == 2)
            SansFace.GetComponent<Animator>().SetTrigger("Smile");
        // Sans nervous face
        if (dialogueCounter == 0 && sentenceCounter == 5
            || dialogueCounter == 8 && sentenceCounter == 0
            || dialogueCounter == 15 && sentenceCounter == 1
            || dialogueCounter == 17 && sentenceCounter == 0
            || dialogueCounter == 23 && sentenceCounter == 0 )
            SansFace.GetComponent<Animator>().SetTrigger("Nervous");
        // Sans bad time face
        if (dialogueCounter == 19 && sentenceCounter == 4)
        {
            SansFace.GetComponent<Animator>().SetTrigger("BadTime");
            Sans.Stop("Theme");
            Sans.Play("BadTime");
            sentenceCounter += 1;
        }
        // Sans surprised face
        if (dialogueCounter == 6 && sentenceCounter == 11
            || dialogueCounter == 13 && sentenceCounter == 1
            || dialogueCounter == 17 && sentenceCounter == 2)
            SansFace.GetComponent<Animator>().SetTrigger("Surprised");

        print("DC: " + dialogueCounter);
        print("SC: " + sentenceCounter);
    }
    
}
