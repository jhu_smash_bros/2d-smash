
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ZeroIntroPart1 : Stage
{
    public GameObject Part2;
    public GameObject RedFlash;
    public SmashCharacter Zero;
    public CielController Ciel;
    public Dialogue[] Conversation;
    private int sentenceCounter = 0;

    public override void Update()
    {
        base.Update();
        
        // have Zero walk up to Ciel
        if (dialogueCounter == 0)
        {
            if (Zero.transform.position.x > 1.5)
                Zero.rb.velocity = new Vector2(-30f, 0f);
            else
            {
                dialogueCounter = 1;
                dialogueStarted = true;
                Zero.rb.velocity = new Vector2(0f, 0f);
            }
        }
        // once there, the dialogue can start
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                dialogueCounter += 1;
                dialogueStarted = true;
            }
        }

        if (dialogueStarted)
        {
            // make sure Zero is facing the right way
            if (dialogueCounter == -1)
                Zero.ReverseImage();

            // once the scene starts
            if (dialogueCounter == 0)
            {
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("ZeroTheme");
            }
            // there are no more dialogues left
            else if (dialogueCounter == Conversation.Length + 1)
            {
                Conversation[dialogueCounter - 2].textbox.SetActive(false);
                // reset Zero
                Zero.transform.position = new Vector3(60f, -33.9f, 0f);
                Zero.an.SetBool("teleport0", false);
                Zero.an.SetBool("teleport1", false);
                // reset Ciel
                Ciel.transform.position = new Vector3(-18f, -33.9f, 0f);
                // reset dialogue counter
                dialogueCounter = -1;
                // start the next part
                Part2.SetActive(true);
                gameObject.SetActive(false);
            }
            else if (dialogueCounter > 0)
            {
                FindObjectOfType<DialogueManager>().StartDialogue(Conversation[dialogueCounter - 1]);
                if (dialogueCounter >= 2)
                    Conversation[dialogueCounter - 2].textbox.SetActive(false);

                if (dialogueCounter == 2 || dialogueCounter == 4 || dialogueCounter == 6)
                    GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("Pick");
            }

            // have Zero stand straight up once the dialogue starts
            if (dialogueCounter == 1)
                Zero.an.SetBool("teleport0", true);

            // shift dialogue boxes down
            if (dialogueCounter == 4 || dialogueCounter == 5)
                Conversation[dialogueCounter - 1].textbox.transform.position = new Vector3(0f, -24f, 0f);

            // shift dialogue boxes back up
            if (dialogueCounter == 6 || dialogueCounter == 7)
                Conversation[dialogueCounter - 1].textbox.transform.position = new Vector3(0f, 24f, 0f);

            if (dialogueCounter == 8)
            {

            }

            // trigger zero being sent to his missions
            if (dialogueCounter == 9)
            {
                RedFlash.SetActive(false);
                Zero.an.SetTrigger("teleport");
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Stop("Alarm");
            }

            sentenceCounter = 0;
            dialogueStarted = false;
        }

        // counts the sentence you are on
        if (Input.GetKeyDown(KeyCode.N))
            sentenceCounter += 1;

        if (dialogueCounter == 2)
        {
            if (sentenceCounter == 2)
            {
                sentenceCounter = 0;
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("Pick");
            }
        }

        // start alarm & move Ciel
        if (dialogueCounter == 6)
        {
            if (sentenceCounter == 2)
            {
                sentenceCounter = 0;
                RedFlash.SetActive(true);
                Zero.an.SetBool("teleport1", true);
                Ciel.rb.velocity = new Vector2(-10f, 0f);
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Stop("ZeroTheme");
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("Alarm");
            }
            if (Ciel.transform.position.x <= -26)
            {
                Ciel.rb.velocity = new Vector2(0f, 0f);
                Ciel.ReverseImage();
                dialogueCounter += 1;
            }
        }

        // pray once Zero leaves
        if (dialogueCounter == 9 && sentenceCounter == 2)
        {
            Ciel.an.SetTrigger("pray");
            sentenceCounter += 1;
        }
    }
}