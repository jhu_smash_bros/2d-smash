﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LynIntro : Stage
{
    public GameObject stage;
    public GameObject grillbys;

    public Dialogue sansDialogue1;
    public Dialogue lynDialogue;
    public Dialogue sansDialogue2;
    public Dialogue sansDialogue3;
    public Dialogue sansDialogue4;

    public override void Update()
    {
        base.Update();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            dialogueCounter += 1;
            dialogueStarted = true;
        }

        if (dialogueCounter == 0)
        {
            dialogueCounter += 1;
            dialogueStarted = true;
        }

        if (dialogueStarted)
        {
            switch(dialogueCounter)
            {
                case 1:
                    FindObjectOfType<DialogueManager>().StartDialogue(sansDialogue1);
                    GameObject.Find("SansAudioManager").GetComponent<AudioManager>().Play("Salt");
                    break;
                
                case 2:
                    sansDialogue1.textbox.SetActive(false);
                    GameObject.Find("SansAudioManager").GetComponent<AudioManager>().Stop("Salt");
                    GameObject.Find("LynAudioManager").GetComponent<AudioManager>().Play("Theme");
                    FindObjectOfType<DialogueManager>().StartDialogue(lynDialogue);
                    break;

                case 3:
                    lynDialogue.textbox.SetActive(false);
                    GameObject.Find("LynAudioManager").GetComponent<AudioManager>().Stop("Theme");
                    // prepare for next scene
                    sansDialogue1.sprite.GetComponent<SmashCharacter>().rb.gravityScale = 0;
                    GameObject.Find("Sans").GetComponent<SmashCharacter>().an.SetBool("sitting", true);
                    stage.SetActive(false);
                    grillbys.SetActive(true);
                    break;

                case 4:
                    GameObject.Find("SansAudioManager").GetComponent<AudioManager>().Play("Theme");
                    break;

                case 5:
                    FindObjectOfType<DialogueManager>().StartDialogue(sansDialogue2);
                    break;

                case 6:
                    sansDialogue2.textbox.SetActive(false);
                    FindObjectOfType<DialogueManager>().StartDialogue(sansDialogue3);
                    break;

                case 7:
                    sansDialogue3.textbox.SetActive(false);
                    FindObjectOfType<DialogueManager>().StartDialogue(sansDialogue4);
                    break;

                default:
                    sansDialogue4.textbox.SetActive(false);
                    sansDialogue1.sprite.GetComponent<SmashCharacter>().rb.gravityScale = 1;
                    GameObject.Find("SansAudioManager").GetComponent<AudioManager>().Stop("Theme");
                    GameObject.Find("Sans").GetComponent<SmashCharacter>().an.SetBool("sitting", false);
                    grillbys.SetActive(false);
                    stage.SetActive(true);
                    dialogueCounter = -1;
                    break;
            }
            dialogueStarted = false;
        }

        if (sansDialogue1.sprite.GetComponent<SmashCharacter>().dead)
            GameObject.Find("SansAudioManager").GetComponent<AudioManager>().Stop("Salt");
    }
}
