﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Stage : MonoBehaviour
{
    public bool playTheme;
    public GameObject percentageCanvas;

    public int dialogueCounter;
    [HideInInspector]
    public bool dialogueStarted;

    public virtual void Start()
    {
        if (playTheme)
        { 
            if (UtilStaticVars.Theme != "")
            {
                GameObject.Find("AudioManager").GetComponent<AudioManager>().Play(UtilStaticVars.Theme);
            }
        }
        Cursor.lockState = CursorLockMode.None;
    }

    public virtual void Update()
    {
        /**********
        LOAD SCENES
        ***********/

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UtilStaticVars.Player1 = "";
            UtilStaticVars.Player2 = "";
            UtilStaticVars.Player3 = "";
            UtilStaticVars.Player4 = "";
            UtilStaticVars.IsReplay = false;
            SceneManager.LoadScene(0);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
            SceneManager.LoadScene(1);
        
        if (Input.GetKeyDown(KeyCode.Alpha2))
            SceneManager.LoadScene(2);

        if (Input.GetKeyDown(KeyCode.Alpha3))
            SceneManager.LoadScene(3); 

        if (Input.GetKeyDown(KeyCode.Alpha4))
            SceneManager.LoadScene(4);

        if (Input.GetKeyDown(KeyCode.Alpha5))
            SceneManager.LoadScene(5);
        if (Input.GetKeyDown(KeyCode.Alpha6))
            SceneManager.LoadScene(6);

        // turn on / off the percentages
        if (Input.GetKeyDown(KeyCode.Equals))
            if (!percentageCanvas.activeSelf)
                percentageCanvas.SetActive(true);
            else
                percentageCanvas.SetActive(false);

        // turn on/ off the stage audio
        if (Input.GetKeyDown(KeyCode.LeftBracket))
        {
            if (gameObject.name != "FightingStage")
            {
                if (playTheme)
                    GameObject.Find("AudioManager").GetComponent<AudioManager>().Stop("Theme");
                else
                    GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("Theme");
            }
            else
            {
                FightingStage fs = gameObject.GetComponent<FightingStage>();
                if (playTheme)
                {
                    if (fs.island.activeSelf && !fs.rocketZone.activeSelf)
                        GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("IslandTheme");
                    else if (fs.rocketZone.activeSelf && !fs.island.activeSelf)
                        GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("RocketTheme");
                }
                else
                {
                    if (fs.island.activeSelf && !fs.rocketZone.activeSelf)
                        GameObject.Find("AudioManager").GetComponent<AudioManager>().Stop("IslandTheme");
                    else if (fs.rocketZone.activeSelf && !fs.island.activeSelf)
                        GameObject.Find("AudioManager").GetComponent<AudioManager>().Stop("RocketTheme"); 
                }
            }
            playTheme = !playTheme;
        }
    }
}
