﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FightingStage : Stage
{
    // PERCENTAGES
    public TextMeshProUGUI perText1;
    public TextMeshProUGUI perText2;
    public TextMeshProUGUI perText3;
    public TextMeshProUGUI perText4;
    
    // PAUSE TEXT
    public TextMeshProUGUI pauseText;

    // INITIAL POSITIONS
    public Vector2 p1Pos;
    public Vector2 p2Pos;
    public Vector2 p3Pos;
    public Vector2 p4Pos;

    // STAGES
    public GameObject island;
    public GameObject rocketZone;
    public GameObject tournament;

    // LYN
    [SerializeField]
    private SmashCharacter lyn;
    [SerializeField]
    private SmashCharacter lynReplay;
    [SerializeField]
    private Sprite lynSprite;
    
    // ZERO
    [SerializeField]
    private SmashCharacter zero;
    [SerializeField]
    private SmashCharacter zeroReplay;
    [SerializeField]
    private GameObject x;
    [SerializeField]
    private Sprite zeroSprite;

    // SANS
    [SerializeField]
    private SmashCharacter sans;
    [SerializeField]
    private Sprite sansSprite;
    
    // SHADOW
    [SerializeField]
    private SmashCharacter shadow;
    [SerializeField]
    private SmashCharacter shadowReplay;
    [SerializeField]
    private Sprite shadowSprite;

    // SCORPION
    [SerializeField]
    private SmashCharacter scorp;
    [SerializeField]
    private SmashCharacter scorpReplay;
    [SerializeField]
    private Sprite scorpSprite;

    // GUILE
    [SerializeField]
    private SmashCharacter guile;
    [SerializeField]
    private SmashCharacter guileReplay;
    [SerializeField]
    private Sprite guileSprite;

    // KID GOKU
    [SerializeField]
    private SmashCharacter kidGoku;
    [SerializeField]
    private SmashCharacter kidGokuReplay;
    [SerializeField]
    private Sprite kidGokuSprite;

    // PLAYERS
    private SmashCharacter p1;
    private SmashCharacter p2;
    private SmashCharacter p3;
    private SmashCharacter p4;

    // Sets up Character chosen from the CSS
    private void Awake()
    {
        // color choices if multiple people choose the same character
        Color first = new Color(255, 255, 255);
        Color second = new Color(100, 100, 100);
        Color third = new Color(100, 100, 255);
        Color fourth = new Color(255, 100, 100);
        int colorSelect = 0;

        //Stage position setter
        switch (UtilStaticVars.Stage)
        {
            case "Island":
                p1Pos = new Vector2(40, 0);
                p2Pos = new Vector2(-40, 0);
                p3Pos = new Vector2(-14, -14);
                p4Pos = new Vector2(14, -14);
                break;
            case "RocketZone":
                p1Pos = new Vector2(-22, 0);
                p2Pos = new Vector2(22, 0);
                p3Pos = new Vector2(-36, -13.5f);
                p4Pos = new Vector2(36, -13.5f);
                break;
            case "Tournament":
                p1Pos = new Vector2(20, -20);
                p2Pos = new Vector2(-20, -20);
                p3Pos = new Vector2(40, -20);
                p4Pos = new Vector2(-40, -20);
                break;
            default:
                break;
        }

        /* Sets players 1-4 to the selected characters if any have been selected.
        * Otherwise, the percentage text and character spawn will be inactive
        * and the camera follower will have dead targets in the empty indicies
        * The if statements are for changing color if multiple people choose the same character
        */

        // Player 1 Setup
        CharacterSetup(UtilStaticVars.Player1, p1Pos, perText1, first, p1, 0, UtilStaticVars.CPU1);

        // Player 2 Setup
        if (UtilStaticVars.Player2 == UtilStaticVars.Player1)
        {   CharacterSetup(UtilStaticVars.Player2, p2Pos, perText2, second, p2, 1, UtilStaticVars.CPU2);
        }
        else
        {   CharacterSetup(UtilStaticVars.Player2, p2Pos, perText2, first, p2, 1, UtilStaticVars.CPU2);
        }
        
        // Player 3 Setup
        if (UtilStaticVars.Player3 == UtilStaticVars.Player2 && UtilStaticVars.Player3 == UtilStaticVars.Player1)
        {   CharacterSetup(UtilStaticVars.Player3, p3Pos, perText3, third, p3, 2, UtilStaticVars.CPU3);
        }
        else if (UtilStaticVars.Player3 == UtilStaticVars.Player2 || UtilStaticVars.Player3 == UtilStaticVars.Player1)
        {   CharacterSetup(UtilStaticVars.Player3, p3Pos, perText3, second, p3, 2, UtilStaticVars.CPU3);
        }
        else
        {   CharacterSetup(UtilStaticVars.Player3, p3Pos, perText3, first, p3, 2, UtilStaticVars.CPU3);
        }
                
        // Player 4 Setup
        if (UtilStaticVars.Player4 == UtilStaticVars.Player3)
        {   colorSelect++;
        }
        if (UtilStaticVars.Player4 == UtilStaticVars.Player2)
        {   colorSelect++;
        }
        if (UtilStaticVars.Player4 == UtilStaticVars.Player1)
        {   colorSelect++;
        }
        if (colorSelect == 3)
        {   CharacterSetup(UtilStaticVars.Player4, p4Pos, perText4, fourth, p4, 3, UtilStaticVars.CPU4);
        }
        else if (colorSelect == 2)
        {   CharacterSetup(UtilStaticVars.Player4, p4Pos, perText4, third, p4, 3, UtilStaticVars.CPU4);
        }
        else if (colorSelect == 1)
        {   CharacterSetup(UtilStaticVars.Player4, p4Pos, perText4, second, p4, 3, UtilStaticVars.CPU4);
        }
        else
        {   CharacterSetup(UtilStaticVars.Player4, p4Pos, perText4, first, p4, 3, UtilStaticVars.CPU4);
        }
    }

    // Activates the correct stage. The default stage is the island stage.
    public override void Start()
    {
        switch (UtilStaticVars.Stage)
        {
            case "Island":
                island.SetActive(true);
                // inactive stages
                rocketZone.SetActive(false);
                tournament.SetActive(false);
                break;
            case "RocketZone":
                rocketZone.SetActive(true);
                // inactive stages
                island.SetActive(false);
                tournament.SetActive(false);
                break;
            case "Tournament":
                tournament.SetActive(true);
                // inactive stages
                island.SetActive(false);
                rocketZone.SetActive(false);

                if (perText1.transform.parent.gameObject.active)
                {
                    perText1.transform.parent.gameObject.transform.position
                        = new Vector3(perText1.transform.parent.gameObject.transform.position.x,
                                      -1*perText1.transform.parent.gameObject.transform.position.y,
                                      perText1.transform.parent.gameObject.transform.position.z);
                }
                if (perText2.transform.parent.gameObject.active)
                {
                    perText2.transform.parent.gameObject.transform.position
                        = new Vector3(perText2.transform.parent.gameObject.transform.position.x,
                                      -1*perText2.transform.parent.gameObject.transform.position.y,
                                      perText2.transform.parent.gameObject.transform.position.z);
                }
                if (perText3.transform.parent.gameObject.active)
                {
                    perText3.transform.parent.gameObject.transform.position
                        = new Vector3(perText3.transform.parent.gameObject.transform.position.x,
                                      -1*perText3.transform.parent.gameObject.transform.position.y,
                                      perText3.transform.parent.gameObject.transform.position.z);
                }
                if (perText4.transform.parent.gameObject.active)
                {
                    perText4.transform.parent.gameObject.transform.position
                        = new Vector3(perText4.transform.parent.gameObject.transform.position.x,
                                      -1*perText4.transform.parent.gameObject.transform.position.y,
                                      perText4.transform.parent.gameObject.transform.position.z);
                }
                break;
            case "":
                if (playTheme)
                    GameObject.Find("AudioManager").GetComponent<AudioManager>().Play("TrainingTheme");
                break;
            default:
                break;
        }
        base.Start();
    }

    public virtual void FixedUpdate()
    {
        //pausing
        if (Input.GetButtonDown("StartButton"))
        {
            UtilStaticVars.Pause = !UtilStaticVars.Pause;
        }
        if (!UtilStaticVars.Pause)
        {
            pauseText.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            pauseText.transform.parent.gameObject.SetActive(true);
        }
    }

    private void CharacterSetup(string character, Vector3 spawn, TextMeshProUGUI text, Color color, SmashCharacter p, int targetInd, bool cpu)
    {
        switch(character)
        {
            case "Lyn":
                LynSetup(spawn, text, color, p, targetInd, cpu);
                break;
            case "Zero":
                ZeroSetup(spawn, text, color, p, targetInd, cpu);
                break;
            case "Sans":
                SansSetup(spawn, text, color, p, targetInd, cpu);
                break;
            case "Shadow":
                ShadowSetup(spawn, text, color, p, targetInd, cpu);
                break;
            case "Scorpion":
                ScorpionSetup(spawn, text, color, p, targetInd, cpu);
                break;
            case "Guile":
                GuileSetup(spawn, text, color, p, targetInd, cpu);
                break;
            case "Kid Goku":
                KidGokuSetup(spawn, text, color, p, targetInd, cpu);
                break;
            default:
                switch (targetInd)
                {
                    case 0:
                        perText1.transform.parent.gameObject.SetActive(false);
                        break;
                    case 1:
                        perText2.transform.parent.gameObject.SetActive(false);
                        break;
                    case 2:
                        perText3.transform.parent.gameObject.SetActive(false);
                        break;
                    case 3:
                        perText4.transform.parent.gameObject.SetActive(false);
                        break;
                    default:
                        break;
                }
                Camera.main.GetComponent<CameraFollower>().targets[targetInd].GetComponent<SmashCharacter>().dead = true;
                break;
        }
    }

    private void LynSetup(Vector3 spawn, TextMeshProUGUI text, Color color, SmashCharacter p, int targetInd, bool cpu)
    {
        if (!UtilStaticVars.IsReplay)
        {
            p = Instantiate(lyn, spawn, Quaternion.identity);
        }
        else
        {
            p = Instantiate(lynReplay, spawn, Quaternion.identity);
        }
        p.name = "Lyn" + (targetInd + 1);
        LynController lyn1 = (LynController)p;
        lyn1.RESET = spawn;
        // sets Percentage Canvas
        lyn1.perText = text;
        lyn1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().sprite = lynSprite;
        lyn1.perText.transform.parent.Find("Profile").transform.localPosition = new Vector3(-25.9f, -54.60998f, 0f);
        lyn1.perText.transform.parent.Find("Profile").transform.localScale = new Vector3(20f, 40f, 0f);
        lyn1.perText.transform.parent.Find("Profile").Find("mask").transform.localPosition = new Vector3(0.38f, 1.45f, 0f);
        lyn1.perText.transform.parent.Find("Profile").Find("mask").transform.localScale = new Vector3(15f, 15f, 0f);
        lyn1.perText.transform.parent.Find("Name").GetComponent<TextMeshProUGUI>().text = "LYN";
        lyn1.finalSmashBar = lyn1.perText.transform.parent.Find("Bar outline").Find("fs meter").transform;
        // sets color if multiple Lyns 
        lyn1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        lyn1.GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        // sets controls and adds to camera
        Camera.main.GetComponent<CameraFollower>().targets[targetInd] = lyn1.transform;
        lyn1.gameObject.SetActive(true);
        lyn1.stocks = UtilStaticVars.Stocks;
        if (cpu)
        {
            lyn1.isCPU = true;
            lyn1.player = (targetInd + 1) * -1;
        }
        else
        {
            lyn1.player = targetInd + 1;
        }
    }

    private void ZeroSetup(Vector3 spawn, TextMeshProUGUI text, Color color, SmashCharacter p, int targetInd, bool cpu)
    {
        if (!UtilStaticVars.IsReplay)
        {
            p = Instantiate(zero, spawn, Quaternion.identity);
        }
        else
        {
            p = Instantiate(zeroReplay, spawn, Quaternion.identity);
        }
        p.name = "Zero" + (targetInd + 1);
        GameObject megaman = Instantiate(x, spawn, Quaternion.identity);
        ZeroController zero1 = (ZeroController)p;
        zero1.megamanX = megaman;
        zero1.RESET = spawn;
        // sets Percentage Canvas
        zero1.perText = text;
        zero1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().sprite = zeroSprite;
        zero1.perText.transform.parent.Find("Profile").transform.localPosition = new Vector3(-26.92f, -76.99768f, 0f);
        zero1.perText.transform.parent.Find("Profile").transform.localScale = new Vector3(20f, 40f, 0f);
        zero1.perText.transform.parent.Find("Profile").Find("mask").transform.localPosition = new Vector3(0.04550019f, 2.1f, 0f);
        zero1.perText.transform.parent.Find("Profile").Find("mask").transform.localScale = new Vector3(16.25f, 16.25f, 0f);
        zero1.perText.transform.parent.Find("Name").GetComponent<TextMeshProUGUI>().text = "ZERO";
        zero1.finalSmashBar = zero1.perText.transform.parent.Find("Bar outline").Find("fs meter").transform;
        // sets color if multiple Zeros 
        zero1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        zero1.GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        // sets controls and adds to camera
        Camera.main.GetComponent<CameraFollower>().targets[targetInd] = zero1.transform;
        zero1.gameObject.SetActive(true);
        zero1.stocks = UtilStaticVars.Stocks;
        if (cpu)
        {
            zero1.isCPU = true;
            zero1.player = (targetInd + 1) * -1;
        }
        else
        {
            zero1.player = targetInd + 1;
        }
    }

    private void SansSetup(Vector3 spawn, TextMeshProUGUI text, Color color, SmashCharacter p, int targetInd, bool cpu)
    {
        p = Instantiate(sans, spawn, Quaternion.identity);
        p.name = "Sans" + (targetInd + 1);
        SansController sans1 = (SansController)p;
        sans1.RESET = spawn;
        // sets Percentage Canvas
        sans1.perText = text;
        sans1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().sprite = sansSprite;
        sans1.perText.transform.parent.Find("Profile").transform.localPosition = new Vector3(-30.7f, -64.1981f, 0f);
        sans1.perText.transform.parent.Find("Profile").transform.localScale = new Vector3(25f, 50f, 0f);
        sans1.perText.transform.parent.Find("Profile").Find("mask").transform.localPosition = new Vector3(0f, 1.33f, 0f);
        sans1.perText.transform.parent.Find("Profile").Find("mask").transform.localScale = new Vector3(12f, 12f, 0f);
        sans1.perText.transform.parent.Find("Name").GetComponent<TextMeshProUGUI>().text = "SANS";
        sans1.finalSmashBar = sans1.perText.transform.parent.Find("Bar outline").Find("fs meter").transform;
        // sets color if multiple Sans 
        sans1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        sans1.GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        // sets controls and adds to camera
        Camera.main.GetComponent<CameraFollower>().targets[targetInd] = sans1.transform;
        sans1.gameObject.SetActive(true);
        sans1.stocks = UtilStaticVars.Stocks;
        if (cpu)
        {
            sans1.isCPU = true;
            sans1.player = (targetInd + 1) * -1;
        }
        else
        {
            sans1.player = targetInd + 1;
        }
    }

    private void ShadowSetup(Vector3 spawn, TextMeshProUGUI text, Color color, SmashCharacter p, int targetInd, bool cpu)
    {
        if (!UtilStaticVars.IsReplay)
        {
            p = Instantiate(shadow, spawn, Quaternion.identity);
        }
        else
        {
            p = Instantiate(shadowReplay, spawn, Quaternion.identity);
        }
        p.name = "Shadow" + (targetInd + 1);
        ShadowController shadow1 = (ShadowController)p;
        shadow1.RESET = spawn;
        // sets Percentage Canvas
        shadow1.perText = text;
        shadow1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().sprite = shadowSprite;
        shadow1.perText.transform.parent.Find("Profile").transform.localPosition = new Vector3(-32.6294f, -41.85408f, 0f);
        shadow1.perText.transform.parent.Find("Profile").transform.localScale = new Vector3(18f, 36f, 0f);
        shadow1.perText.transform.parent.Find("Profile").Find("mask").transform.localPosition = new Vector3(0.27f, 1.18f, 0f);
        shadow1.perText.transform.parent.Find("Profile").Find("mask").transform.localScale = new Vector3(18f, 18f, 0f);
        shadow1.perText.transform.parent.Find("Name").GetComponent<TextMeshProUGUI>().text = "SHADOW";
        shadow1.finalSmashBar = shadow1.perText.transform.parent.Find("Bar outline").Find("fs meter").transform;
        // sets color if multiple Shadows
        shadow1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        shadow1.GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        // sets controls and adds to camera
        Camera.main.GetComponent<CameraFollower>().targets[targetInd] = shadow1.transform;
        shadow1.gameObject.SetActive(true);
        shadow1.stocks = UtilStaticVars.Stocks;
        if (cpu)
        {
            shadow1.isCPU = true;
            shadow1.player = (targetInd + 1) * -1;
        }
        else
        {
            shadow1.player = targetInd + 1;
        }
    }

    private void ScorpionSetup(Vector3 spawn, TextMeshProUGUI text, Color color, SmashCharacter p, int targetInd, bool cpu)
    {
        if (!UtilStaticVars.IsReplay)
        {
            p = Instantiate(scorp, spawn, Quaternion.identity);
        }
        else
        {
            p = Instantiate(scorpReplay, spawn, Quaternion.identity);
        }
        p.name = "Scorpion" + (targetInd + 1);
        ScorpionController scorp1 = (ScorpionController)p;
        scorp1.RESET = spawn;
        // sets Percentage Canvas
        scorp1.perText = text;
        scorp1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().sprite = scorpSprite;
        scorp1.perText.transform.parent.Find("Profile").transform.localPosition = new Vector3(-30.7f, -183.67f, 0f);
        scorp1.perText.transform.parent.Find("Profile").transform.localScale = new Vector3(12.5f, 25f, 0f);
        scorp1.perText.transform.parent.Find("Profile").Find("mask").transform.localPosition = new Vector3(-0.23f, 7.67f, 0f);
        scorp1.perText.transform.parent.Find("Profile").Find("mask").transform.localScale = new Vector3(24f, 24f, 0f);
        scorp1.perText.transform.parent.Find("Name").GetComponent<TextMeshProUGUI>().text = "SCORPION";
        scorp1.finalSmashBar = scorp1.perText.transform.parent.Find("Bar outline").Find("fs meter").transform;
        // sets color if multiple Scorpions
        scorp1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        scorp1.GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        // sets controls and adds to camera
        Camera.main.GetComponent<CameraFollower>().targets[targetInd] = scorp1.transform;
        scorp1.gameObject.SetActive(true);
        scorp1.stocks = UtilStaticVars.Stocks;
        if (cpu)
        {
            scorp1.isCPU = true;
            scorp1.player = (targetInd + 1) * -1;
        }
        else
        {
            scorp1.player = targetInd + 1;
        }
    }

    private void GuileSetup(Vector3 spawn, TextMeshProUGUI text, Color color, SmashCharacter p, int targetInd, bool cpu)
    {
        if (!UtilStaticVars.IsReplay)
        {
            p = Instantiate(guile, spawn, Quaternion.identity);
        }
        else
        {
            p = Instantiate(guileReplay, spawn, Quaternion.identity);
        }
        p.name = "Guile" + (targetInd + 1);
        GuileController guile1 = (GuileController)p;
        guile1.RESET = spawn;
        // sets Percentage Canvas
        guile1.perText = text;
        guile1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().sprite = guileSprite;
        guile1.perText.transform.parent.Find("Profile").transform.localPosition = new Vector3(-27.9f, -42.5f, 0f);
        guile1.perText.transform.parent.Find("Profile").transform.localScale = new Vector3(7f, 14f, 0f);
        guile1.perText.transform.parent.Find("Profile").Find("mask").transform.localPosition = new Vector3(-0.05f, 3.52f, 0f);
        guile1.perText.transform.parent.Find("Profile").Find("mask").transform.localScale = new Vector3(50f, 50f, 0f);
        guile1.perText.transform.parent.Find("Name").GetComponent<TextMeshProUGUI>().text = "GUILE";
        guile1.finalSmashBar = guile1.perText.transform.parent.Find("Bar outline").Find("fs meter").transform;
        // sets color if multiple Guiles
        guile1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        guile1.GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        // sets controls and adds to camera
        Camera.main.GetComponent<CameraFollower>().targets[targetInd] = guile1.transform;
        guile1.gameObject.SetActive(true);
        guile1.stocks = UtilStaticVars.Stocks;
        if (cpu)
        {
            guile1.isCPU = true;
            guile1.player = (targetInd + 1) * -1;
        }
        else
        {
            guile1.player = targetInd + 1;
        }
    }

    private void KidGokuSetup(Vector3 spawn, TextMeshProUGUI text, Color color, SmashCharacter p, int targetInd, bool cpu)
    {
        if (!UtilStaticVars.IsReplay)
        {
            p = Instantiate(kidGoku, spawn, Quaternion.identity);
        }
        else
        {
            p = Instantiate(kidGokuReplay, spawn, Quaternion.identity);
        }
        p.name = "KidGoku" + (targetInd + 1);
        KidGokuController kidGoku1 = (KidGokuController)p;
        kidGoku1.RESET = spawn;
        // sets Percentage Canvas
        kidGoku1.perText = text;
        kidGoku1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().sprite = kidGokuSprite;
        kidGoku1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().flipX = true;
        kidGoku1.perText.transform.parent.Find("Profile").transform.localPosition = new Vector3(-25.9f, -58.46f, 0f);
        kidGoku1.perText.transform.parent.Find("Profile").transform.localScale = new Vector3(20f, 40f, 0f);
        kidGoku1.perText.transform.parent.Find("Profile").Find("mask").transform.localPosition = new Vector3(-0.05f, 1.6f, 0f);
        kidGoku1.perText.transform.parent.Find("Profile").Find("mask").transform.localScale = new Vector3(15.9f, 15.7f, 0f);
        kidGoku1.perText.transform.parent.Find("Name").GetComponent<TextMeshProUGUI>().text = "KID GOKU";
        kidGoku1.finalSmashBar = kidGoku1.perText.transform.parent.Find("Bar outline").Find("fs meter").transform;
        // sets color if multiple Shadows
        kidGoku1.perText.transform.parent.Find("Profile").GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        kidGoku1.GetComponent<SpriteRenderer>().color = new Color(color.r / 255f, color.g / 255f, color.b / 255f);
        // sets controls and adds to camera
        Camera.main.GetComponent<CameraFollower>().targets[targetInd] = kidGoku1.transform;
        kidGoku1.gameObject.SetActive(true);
        kidGoku1.stocks = UtilStaticVars.Stocks;
        if (cpu)
        {
            kidGoku1.isCPU = true;
            kidGoku1.player = (targetInd + 1) * -1;
        }
        else
        {
            kidGoku1.player = targetInd + 1;
        }
    }
}
