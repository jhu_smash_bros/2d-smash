﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LynFeetController : MonoBehaviour
{
    public SmashCharacter lyn;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (SceneManager.GetActiveScene().buildIndex != 2
            && lyn.rb.velocity.y < 0f && other.tag == "Stage"
            && lyn.an.GetCurrentAnimatorStateInfo(0).IsName("up b 2"))
        {
            lyn.rb.velocity = Vector2.zero;
        }
    }
}
