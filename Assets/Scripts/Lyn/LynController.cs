﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LynController : SmashCharacter
{
    public GameObject arrowPrefab;

    [HideInInspector]
    public float arrowStrength = 0.2f;

    [HideInInspector]
    public bool upBStart = false;
    [HideInInspector]
    public bool sideBStart = false;
    [HideInInspector]
    public bool sideBCombo = false;
    [HideInInspector]
    public bool downBStart = false;
    [HideInInspector]
    public bool pummelStart = false;
    [HideInInspector]
    public bool neutralBStart = false;
    [HideInInspector]
    public bool finalSmashStart = false;

    private bool startFSCombo = false;
    private int finalSmashCounter = 0;

	public override void FixedUpdate()
    {
        //don't run fixed update stuff during replay
        if (UtilStaticVars.IsReplay) return;

        /*****
        SOUNDS
        ******/
        if (jumpCounter == 1)
        {
            jumpCounter += 1;
            sound.Play("LynJump1");
        }
        else if (jumpCounter == 3)
        {
            jumpCounter = 0;
            sound.Play("LynJump2");
        }
        if (startDash)
        {
            startDash = false;
            if (!an.GetBool("jumping") && !falling)
                sound.Play("LynDash");
        }
        if (falling && rb.velocity.y == 0 && rb.gravityScale != 0)
        {
            sound.Play("LynLanding");
        }

        // reset, movement, jumping/falling, crouching
        if (active)
        {
            base.FixedUpdate();
            if (isPaused) return;
        }

        /***
        IDLE
        ****/
        if (an.GetCurrentAnimatorStateInfo(0).IsName("idle"))
        {
            // reset side B
            sideBStart = false;
            sideBCombo = false;
            // reset pummel
            pummelStart = false;
            // reset arrow
            arrowStrength = 0.2f;
            neutralBStart = false;
            an.ResetTrigger("b released");
            // reset counter
            downBStart = false;
            countering = false;
            an.SetBool("countering", false);
            // reset final smash
            startFSCombo = false;
            finalSmashCounter = 0;
        }
        /***
        UP B
        ****/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("up b 1"))
        {
            if (rb.gravityScale != 0)
            {
                upBStart = true;
                rb.gravityScale = 0;
                rb.velocity = new Vector2(0f, 0f);
            }
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("up b 2"))
        {
            if (rb.gravityScale == 0)
            {
                rb.gravityScale = 1;
                rb.velocity = new Vector2(0f, 60f);
            }
            rb.velocity = new Vector2(0.5f * facing * SPEED.x, rb.velocity.y);

            if (rb.velocity.y < 0)
                rb.gravityScale = 2;
            else if (rb.velocity.y == 0)
            {
                rb.velocity = Vector2.zero;
                an.SetTrigger("up b landed");
            }
        }
        /*****
        SIDE B
        ******/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("side b 1"))
        {
            sideBStart = true;
            rb.gravityScale = 0;
            rb.velocity = new Vector2(0f, 0f);
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("side b 1_5"))
        {
            rb.velocity = new Vector2(2 * facing * SPEED.x, 0f);
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("side b 2"))
        {
            rb.velocity = new Vector2(0f, 0f);
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("side b 5"))
        {
            rb.velocity = new Vector2(0f, 0f);
            if (sideBCombo)
            {
                sideBCombo = false;
                transform.position =
                    new Vector3(transform.position.x + 14 * facing,
                                transform.position.y, 0f);
            }
        }
        /*****
        DOWN B
        ******/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("down b 1"))
        {
            downBStart = true;
            countering = true;
            an.SetBool("countering", true);
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("down b 3"))
        {
            if (countering)
            {
                countering = false;
                an.SetBool("countering", false);
                transform.position =
                    new Vector3(transform.position.x,
                                transform.position.y + 10, 0f);
                rb.velocity = new Vector2(0f, -14f);
            }
            if (rb.velocity.y < 0)
                rb.velocity += 4 * Vector2.up * Physics2D.gravity.y * Time.deltaTime;
            else if (rb.velocity.y == 0f)
                an.SetTrigger("up b landed");
        }
        /********
        NEUTRAL B
        *********/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("neutral b 2"))
        {
            neutralBStart = true;
            if (specialHold && arrowStrength <= 1)
                arrowStrength += Time.deltaTime/2;
            else
            {
                if (arrowStrength >= 1)
                    arrowStrength = 1f;
                sound.Stop("LynBowCharge");
                an.SetTrigger("b released");
            }
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("neutral b 4"))
        {
            if (neutralBStart)
            {
                ActivateArrow();
                neutralBStart = false;
            }
        }
        /**********
        FINAL SMASH
        ***********/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("final smash 1"))
        {
            rb.gravityScale = 0;
            rb.velocity = new Vector2(0f, 0f);
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("final smash 1_3"))
        {
            rb.velocity = new Vector2(14 * facing * SPEED.x, 0f);
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("final smash miss"))
        {
            rb.velocity = new Vector2(0f, 0f);
        } 
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("final smash 2"))
        {
            startFSCombo = true;
            rb.velocity = new Vector2(0f, 0f);
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsTag("final smash 2 combo"))
        {
            if (startFSCombo)
            {
                startFSCombo = false;
                finalSmashCounter += 1;
                an.SetInteger("final smash counter", finalSmashCounter);
            }
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("final smash 3_0_1"))
        {
            if (an.GetBool("final smash"))
            {
                transform.position =
                    new Vector3(transform.position.x - 10 * facing,
                                transform.position.y + 1.5f, 0f);
                an.SetBool("final smash", false);
                rb.velocity = new Vector2(0f, 14f);
            }
            if (rb.velocity.y == 0)
                rb.velocity = new Vector2(0f, 0f);
            else
                rb.velocity = new Vector2(-0.5f * facing * SPEED.x, rb.velocity.y);
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("final smash 3_0_2"))
        {
            rb.velocity = new Vector2(7 * facing * SPEED.x, 0f);
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("final smash 3_1_1"))
        {
            rb.velocity = new Vector2(0f, 0f);
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("final smash 4"))
        {
            if (!an.GetBool("final smash"))
            {
                an.SetBool("final smash", true);
                transform.position =
                    new Vector3(transform.position.x + 14 * facing,
                                transform.position.y, 0f);
            }
        }
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("final smash 5"))
        {
            an.SetBool("jumping", false);
            an.SetBool("final smash", false);

            if (finalSmashCounter > 0)
            {
                finalSmashCounter = 0;
                an.SetInteger("final smash counter", finalSmashCounter);
                rb.velocity = new Vector2(0f, 14f);
            }
            if (rb.velocity.y == 0)
                rb.velocity = new Vector2(0f, 0f);
            else
                rb.velocity = new Vector2(-0.5f * facing * SPEED.x, rb.velocity.y);
        }
	}

    /****
    ARROW
    *****/
    public void ActivateArrow()
    {
        GameObject arrow;
        arrow = Instantiate(arrowPrefab,
                            new Vector3(transform.position.x + facing * 5f,
                                        transform.position.y + 4f, 0f),
                            Quaternion.identity);
        projList.Add(arrow);
        spawnedProjectile = 1;
        arrow.GetComponent<ArrowController>().parent = this;
    }
    public override void InstantiateProjectile(int type)
    {
        if (type == 1)
        {
            ActivateArrow();
        }
    }

    /***********
    SET VELOCITY
    ************/
    public void SetVelY(float y)
    {
        rb.velocity = new Vector2(rb.velocity.x, y);
    }
    public void SetVelX(float x)
    {
        rb.velocity = new Vector2(facing * x, rb.velocity.y);
    }
}
