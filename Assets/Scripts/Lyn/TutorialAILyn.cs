﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialAILyn : GenericAI
{
    private float timeCharging = 0.0f;
    private bool charging = false;
    private LynController lyn;
    // Start is called before the first frame update
    void Start()
    {
        lyn = GetComponent<LynController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void inputSim()
    {
        inputClear();
        if(lyn.facing == -1 && transform.position.x < enemy2.transform.position.x)
        {
            hAlt = 1;
        }
        else if (lyn.facing == 1 && transform.position.x > enemy2.transform.position.x)
        {
            hAlt = -1;
        }
        else if (!UtilStaticVars.LockInput)
        {
            vAlt = 0;
            hAlt = 0;
            if (charging)
            {
                timeCharging += Time.deltaTime;
                if (timeCharging > .5f)
                {
                    specialHold = false;
                    specialKey = false;
                    timeCharging = 0.0f;
                    charging = false;
                }
                else
                {
                    specialHold = true;
                    specialKey = true;
                }
            }
            else
            {
                specialKey = true;
                specialHold = true;
                charging = true;
            }
        }
        else
        {
            specialHold = false;
            specialKey = false;
            vAlt = 0;
            hAlt = 0;
        }
    }
}
