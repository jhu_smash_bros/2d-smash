﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{
    public LynController parent; 
    public float SPEED;

    private bool hit = false;
    private float speed = 0f;
    private float timer = 0f;
    private float facing = -1f;

    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

        if (facing != parent.facing)
        {
            facing = -facing;
        }
        transform.localScale = new Vector3(-1*facing*2.5f, 2.5f, 2.5f);

        speed = SPEED * parent.facing * parent.arrowStrength;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Stage")
        {
            parent.RemoveProjectile(this.gameObject);
            Destroy(this.gameObject);
        }

        if (other.tag == "Character" || other.tag == "Shield")
        {
            if(other.tag == "Shield" && GameObject.FindObjectOfType<Tutorial>() != null)
            {
                GameObject.FindObjectOfType<Tutorial>().shotBlocked = true;
            }
            hit = true;
            if (other.tag == "Character")
            {
                if (other.gameObject.GetComponentInParent<SmashCharacter>().dodging)
                {
                    if(GameObject.FindObjectOfType<Tutorial>() != null)
                        GameObject.FindObjectOfType<Tutorial>().shotDodged = true;
                    hit = false;
                }
            }
            Vector2 power = new Vector2(14f, 28f);
            StartCoroutine(parent.Damage(other.attachedRigidbody.name, 14*parent.arrowStrength, power*parent.arrowStrength, 1, false));
        }
    }

    void FixedUpdate()
    {
        //don't fixed update during replay
        if (!UtilStaticVars.Pause && !UtilStaticVars.IsReplay)
        {
            rb.gravityScale = 1;
            timer += Time.deltaTime;

            if (hit || timer > 2f)
            {
                parent.RemoveProjectile(this.gameObject);
                Destroy(this.gameObject);
            }

            rb.velocity = new Vector2(speed, rb.velocity.y);
        }
        else
        {
            rb.velocity = Vector2.zero;
            rb.gravityScale = 0;
        }
    }
}
