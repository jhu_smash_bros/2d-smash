﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordController : MonoBehaviour
{
    public LynController lyn;

    private string enemy;
    private string fsEnemy = "";

    private bool turn = false;
    private bool swordColliding = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Character" || other.tag == "Shield")
        {
            swordColliding = true;
            enemy = other.attachedRigidbody.name;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Character" || other.tag == "Shield")
        {
            lyn.hitting = false;
            swordColliding = false;
        }
    }

    void FixedUpdate()
    {
        if (swordColliding)
        {
            // rigidbody, percent damage, knockback power

            /*****
            PUMMEL
            ******/
            if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("pummel 1_5")
                || lyn.an.GetCurrentAnimatorStateInfo(0).IsName("pummel 2_5"))
            {
                Vector2 power = new Vector2(0f, 3f);
                StartCoroutine(lyn.Damage(enemy, 1f, power, 1, true, 4));
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("pummel 3_5"))
            {
                Vector2 power = new Vector2(3f, 3f);
                StartCoroutine(lyn.Damage(enemy, 1f, power, 1));
            }
            /**********
            DASH ATTACK
            ***********/
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("dash attack"))
            {
                Vector2 power = new Vector2(3f, 3f);
                StartCoroutine(lyn.Damage(enemy, 0.7f, power, 1));
            }
            /***
            NAIR
            ****/
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("nair"))
            {
                Vector2 power = new Vector2(2f, 2f);
                
                // flip knockback based on positioning
                if ((lyn.facing == 1 && GameObject.Find(enemy).transform.position.x < lyn.transform.position.x)
                    || (lyn.facing == -1 && GameObject.Find(enemy).transform.position.x > lyn.transform.position.x))
                {
                    power = new Vector2(-2f, 2f);
                }
                    
                StartCoroutine(lyn.Damage(enemy, 0.7f, power, 1));
            }
            /***
            UP B
            ****/
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("up b 2"))
            {
                Vector2 power = new Vector2(0f, 0f);
                StartCoroutine(lyn.Damage(enemy, 0.5f, power, 1, true, 4));
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("up b 2_5"))
            {
                if (lyn.upBStart)
                {
                    lyn.upBStart = false;
                    Vector2 power = new Vector2(14f, 28f);
                    StartCoroutine(lyn.Damage(enemy, 1f, power, 1, true, 4));
                }
            } 
            /*****
            SIDE B
            ******/
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("side b 1_5"))
            {
                lyn.sideBCombo = true;
                lyn.an.SetTrigger("side b hit");
                Vector2 power = new Vector2(0f, 0f);
                StartCoroutine(lyn.Damage(enemy, 0f, power, 1, true, 4));
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("side b 2"))
            {
                Vector2 power = new Vector2(0f, 0f);
                StartCoroutine(lyn.Damage(enemy, 0f, power, 1, true, 4));
            } 
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("side b 2_5")
                     || lyn.an.GetCurrentAnimatorStateInfo(0).IsName("side b 3_5"))
            {
                Vector2 power = new Vector2(0f, 0f);
                StartCoroutine(lyn.Damage(enemy, 1f, power, 1, true, 4));
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("side b 4_5"))
            {
                if (lyn.sideBStart)
                {
                    lyn.sideBStart = false;
                    Vector2 power = new Vector2(-28f, 28f);
                    StartCoroutine(lyn.Damage(enemy, 14f, power, 1));
                }
            }
            /*****
            DOWN B
            ******/
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("down b 2"))
            {
                if ((lyn.facing == 1 && GameObject.Find(enemy).transform.position.x < lyn.transform.position.x)
                    || (lyn.facing == -1 && GameObject.Find(enemy).transform.position.x > lyn.transform.position.x))
                {
                    if (turn == false)
                    {
                        turn = true;
                        lyn.ReverseImage();
                    }
                }
                Vector2 power = new Vector2(0f, 0f);
                StartCoroutine(lyn.Damage(enemy, 0f, power, 1, true, 4));
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("down b 3_5"))
            {
                if (lyn.downBStart)
                {
                    lyn.downBStart = false;
                    Vector2 power = new Vector2(28f, 28f);
                    StartCoroutine(lyn.Damage(enemy, 14f, power, 1));
                }
                turn = false;
            }
            /**********
            FINAL SMASH
            ***********/
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("final smash 1_3"))
            {
                fsEnemy = enemy;
                lyn.an.SetTrigger("final smash 1 hit");
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("final smash 3_0_2"))
            {
                lyn.an.SetTrigger("final smash 3 hit");
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsTag("final smash 3 attack"))
            {
                Vector2 power = new Vector2(0f, 0f);
                StartCoroutine(lyn.Damage(enemy, 1f, power, 1, true, 4));
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("final smash 3_6_2"))
            {
                Vector2 power = new Vector2(0f, 0f);
                StartCoroutine(lyn.Damage(enemy, 1.4f, power, 1));
            }
        }
        /*****************
        FINAL SMASH (CONT)
        ******************/
        if (fsEnemy != "")
        {
            if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("final smash miss"))
            {
                fsEnemy = "";
                lyn.an.SetBool("final smash", false);
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("final smash 2"))
            {
                Vector2 power = new Vector2(0f, 0f);
                StartCoroutine(lyn.Damage(fsEnemy, 0f, power, 1, true, 0));
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsTag("final smash 2 combo"))
            {
                Vector2 power = new Vector2(0f, 0f);
                StartCoroutine(lyn.Damage(fsEnemy, 1f, power, 1, true, 0));
            }
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("final smash 3_0_1"))
            {
                Vector2 power = new Vector2(0f, 0f);
                StartCoroutine(lyn.Damage(fsEnemy, 0f, power, 1));
            } 
            else if (lyn.an.GetCurrentAnimatorStateInfo(0).IsName("final smash 4"))
            {
                Vector2 power = new Vector2(140f, 40f);
                StartCoroutine(lyn.Damage(fsEnemy, 0f, power, 1));
                fsEnemy = "";
            }
        }
    }
}
