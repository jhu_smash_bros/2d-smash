﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeroAI : GenericAI
{
    private ZeroController zero;
    private LinkedList<GameObject> platforms = new LinkedList<GameObject>();
    private GameObject bottomPlat;
    private int jump = 0;
    private void Start()
    {
        GameObject[] plats = GameObject.FindGameObjectsWithTag("Stage");
        foreach (GameObject p in plats)
        {
            if (p.name != "Bottom Platform")
                platforms.AddLast(p);
            else
                bottomPlat = p;
        }
        zero = GetComponent<ZeroController>();

    }
    public override void inputSim()
    {
        inputClear();
        if(zero.rb.velocity.y == 0)
        {
            jump = 0;
        }
        //jump tracker
        else if(jump == 0 && zero.rb.velocity.y != 0)
        {
            jump = 1;
        }

        //logic
        if(enemy1 != null && !enemy1.dead)
        {
            Transform lLedge;
            Transform rLedge;
            float xPosDiff = enemy1.transform.position.x - transform.position.x;
            float yPosDiff = enemy1.transform.position.y - transform.position.y;
            //x axis movement
            if (Mathf.Abs(xPosDiff) > 6)
            {
                h = xPosDiff / Mathf.Abs(xPosDiff);
                if (h < 0)
                {
                    lKey = true;
                }
                else {
                    rKey = true;
                }
            }
            else
            {
                h = 0;
                rKey = false;
                lKey = false;
            }
            if(xPosDiff < 0 && zero.facing == 1 
                && zero.an.GetCurrentAnimatorStateInfo(0).IsName("idle"))
            {
               h = -1;
            }

            if (xPosDiff > 0 && zero.facing == -1
                && zero.an.GetCurrentAnimatorStateInfo(0).IsName("idle"))
            {
                h = 1;
            }
            //y axis movement
            if (yPosDiff > .5 && zero.rb.velocity.y <= 0)
            {
                foreach (GameObject p in platforms)
                {
                    lLedge = p.transform.Find("left ledge");
                    rLedge = p.transform.Find("right ledge");
                    if (p.name != "Bottom Platform" && enemy1.transform.position.x < rLedge.position.x
                        && enemy1.transform.position.x > lLedge.position.x)
                    {
                        jumpKey = true;
                    }
                }
            }

            //edgeguard kind of
            if(enemy1.transform.position.x <
                bottomPlat.transform.Find("left ledge grab").position.x)
            {
                specialHold = true;
                h = 0;
                if(!zero.an.GetCurrentAnimatorStateInfo(0).IsName("neutral b idle") ||
                    !zero.an.GetCurrentAnimatorStateInfo(0).IsName("neutral b walk") ||
                    !zero.an.GetCurrentAnimatorStateInfo(0).IsName("neutral b jump"))
                {
                    specialKey = true;
                }
            }
            else if (enemy1.transform.position.x >
                bottomPlat.transform.Find("right ledge grab").position.x)
            {
                specialHold = true;
                h = 0;
                if (!zero.an.GetCurrentAnimatorStateInfo(0).IsName("neutral b idle") ||
                    !zero.an.GetCurrentAnimatorStateInfo(0).IsName("neutral b walk") ||
                    !zero.an.GetCurrentAnimatorStateInfo(0).IsName("neutral b jump"))
                {
                    specialKey = true;
                }
            }

            //recovery on the left side
            if (zero.transform.position.x < 
                bottomPlat.transform.Find("left ledge grab").position.x)
            {
                if(jump == 1 && zero.rb.velocity.y < 0)
                {
                    jumpKey = true;
                    h = 1;
                    jump = 2;
                    specialHold = false;
                    specialKey = false;
                    }
                else if(jump == 2 && zero.rb.velocity.y < 0)
                {
                    foreach (GameObject p in platforms)
                    {
                        lLedge = p.transform.Find("left ledge");
                        rLedge = p.transform.Find("right ledge");
                        if (p.name != "Bottom Platform" && zero.transform.position.x < rLedge.position.x
                            && zero.transform.position.x > lLedge.position.x 
                            && zero.transform.position.y < p.transform.position.y)
                        {
                            specialHold = true;
                            specialKey = true;
                            v = 1;
                        }
                    }
                }
                else if(zero.rb.velocity.y != 0)
                {
                    h = 1;
                }
            }
            //recovery on the right side
            else if (zero.transform.position.x >
                bottomPlat.transform.Find("right ledge grab").position.x)
            {
                if (jump == 1 && zero.rb.velocity.y < 0)
                {
                    jumpKey = true;
                    h = -1;
                    jump = 2;
                    specialHold = false;
                    specialKey = false;
                    }
                else if (jump == 2 && zero.rb.velocity.y < 0)
                {
                    foreach (GameObject p in platforms)
                    {
                        lLedge = p.transform.Find("left ledge");
                        rLedge = p.transform.Find("right ledge");
                        if (p.name != "Bottom Platform" && zero.transform.position.x < rLedge.position.x
                            && zero.transform.position.x > lLedge.position.x
                            && zero.transform.position.y < p.transform.position.y)
                        {
                            specialHold = true;
                            specialKey = true;
                            v = 1;
                        }
                    }
                }
                else if(zero.rb.velocity.y != 0)
                {
                    h = -1;
                }
            }

            //On stage attacks
            else if( h == 0 && !specialHold && enemy1.percentage < 50 || enemy1.percentage > 110)
            {
                attackKey = true;
            }
            else if (h == 0 && !specialHold && enemy1.percentage > 50 && enemy1.percentage < 110)
            {
                //side b
                if(xPosDiff > 0 && bottomPlat.transform.Find("right ledge grab").position.x
                    - zero.transform.position.x > 33)
                {
                    specialKey = true;
                    specialHold = true;
                    h = 1;
                }
                else if (xPosDiff < 0 && bottomPlat.transform.Find("left ledge grab").position.x
                    - zero.transform.position.x < -33)
                {
                    specialKey = true;
                    specialHold = true;
                    h = -1;
                }
                //down b
                else
                {
                    specialKey = true;
                    specialHold = true;
                    v = -1;
                }
            }
        }

    }
}
