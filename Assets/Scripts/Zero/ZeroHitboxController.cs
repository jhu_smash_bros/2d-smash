﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ZeroHitboxController : MonoBehaviour {
    [SerializeField]
    private Collider2D[] owner;
    [SerializeField]
    private ZeroController zero;
    public int sDmg;
    public int hitStun;
    public float dmgPercent;
    public float dist;
    public float sStun;
    public Vector2 knockback;
    public bool follow;
    public bool spike;
    public bool fs;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnDisable()
    {
        zero.hitting = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //damage character on hit
        if (fs && !owner.Contains(other))
        {
            zero.initiateFinalSmash(other.attachedRigidbody.GetComponent<SmashCharacter>());
        }
        else if ((other.tag == "Character" || other.tag == "Shield") && !owner.Contains(other) )
        {
            StartCoroutine(zero.Damage(other.attachedRigidbody.name, dmgPercent, knockback, hitStun, follow, dist, spike,sStun, sDmg));
        }
    }
}
