﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZeroController : SmashCharacter
{
    private bool midJab = false;
    private bool downbStart = false;
    private bool finalSmashStart = false;
    private bool isBlasting = false;
    private bool sideBmomentum = false;
    //time between blasts
    private float blastAnimTime = .50f;
    //time since last blast
    private float lastBlast = 0.0f;
    private float fsDmg = 40f;
    //character getting hit in final smash
    private SmashCharacter fsVictim = null;
    private Vector2 fsKnockback = new Vector2(0f, 94f);
    private int fsHitStun = 5;
    public GameObject blast;
    public GameObject nairShot;
    public GameObject megamanX;
    //final smash positions
    public Vector3 fsLocation1;
    public Vector3 fsLocation2;
    public Vector3 fsLocation3;

    public override void Start () {
        facing = 1;
        base.Start();
    }
    public override void Update()
    {
        base.Update();
        if (UtilStaticVars.IsReplay)
        {
            if (an.GetCurrentAnimatorStateInfo(0).IsName("Final Smash attack"))
            {
                Camera.main.orthographicSize = 40f;
                Camera.main.transform.position = new Vector3(0.0f, 0.0f, Camera.main.transform.position.z);
            }
        }
    }
    public override void FixedUpdate()
    {
        //don't run fixed update stuff during replay
        if (UtilStaticVars.IsReplay)
        {
            return;
        }
        if (active)
        {
            //determines if zero is falling or jumping
            bool falling = false;
            if (rb.velocity.y < 0)
            {
                falling = true;
            }
            an.SetBool("falling", falling);
            base.FixedUpdate();

            //if paused set disable x animantor do nothing 
            if (isPaused)
            {
                megamanX.GetComponent<Animator>().enabled = false;
                return;
            }
            megamanX.GetComponent<Animator>().enabled = true;

            //if attacked or land you get your up b back
            if (an.GetBool("attacked") || an.GetCurrentAnimatorStateInfo(0).IsName("landing") 
                || an.GetCurrentAnimatorStateInfo(0).IsName("hanging"))
            {
                an.SetBool("freefall", false);
            }
            //if in freefall don't allow another up b
            if (an.GetBool("freefall"))
            {
                an.ResetTrigger("up b");
            }
            if (jumpCounter == 1)
            {
                jumpCounter += 1;
                sound.Play("Zerojump");
            }
            if (jumpCounter == 3)
            {
                jumpCounter = 0;
                sound.Play("Zerojump");
            }

            //final smash logic
            if (finalSmashStart)
            {
                fsLogic();
            }

            //jab logic
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("jab1"))
            {
                an.ResetTrigger("a");
                //if going to jab 2
                if (attackKey)
                {
                    an.SetTrigger("aa");
                    an.SetBool("endAttack", false);
                }
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("jab2"))
            {
                an.ResetTrigger("aa");
                if (attackKey)
                {
                    an.SetTrigger("aaa");
                    an.SetBool("endAttack", false);
                    midJab = true;
                }
                if (!midJab)
                {
                    an.SetBool("endAttack", true);
                }
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("jab3"))
            {
                an.ResetTrigger("aaa");
                an.SetBool("endAttack", true);
                midJab = false;
            }
            //Side b logic
            else if (!an.GetCurrentAnimatorStateInfo(0).IsName("side b") && sideBmomentum)
            {
                rb.velocity = new Vector2(0.0f, 0.0f);
                sideBmomentum = false;
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("side b"))
            {
                //make him fall slower and move faster laterally
                rb.velocity = new Vector2(2.5f * facing * SPEED.x, rb.velocity.y);
                rb.gravityScale = 0.25f;
                sideBmomentum = true;
            }
            //Up b logic
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("up b"))
            {
                //goes up can't double jumb after up b or up b again
                rb.velocity = new Vector2(rb.velocity.x, SPEED.y * 8.0f/7.0f);
                doubleJumping = true;
                an.SetBool("freefall", true);
            }
            //Down b logic
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("down b1"))
            {
                //initial hop
                if (!downbStart)
                {
                    downbStart = true;
                    rb.velocity = new Vector2(0f, SPEED.y);
                }
                rb.velocity = new Vector2(facing * SPEED.x / 2, rb.velocity.y);
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("down b2"))
            {
                //resetting variable during fall down and making fall down faster 
                downbStart = false;
                rb.gravityScale = 3f;
            }

            //Final Smash Start logic
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("Zero final Smash"))
            {
                //make him move faster laterally with no fall speed
                rb.velocity = new Vector2(4f * facing * SPEED.x, 0);
                rb.gravityScale = 0f;
            }

            //Neutral b logic
            else if(an.GetCurrentAnimatorStateInfo(0).IsName("neutral b idle"))
            {
                //change walking direction and start moving
                if ((h < 0 && facing == 1) || hAlt < -deadzone && facing == 1)
                    ReverseImage();
                else if ((h > 0 && facing == -1) || (hAlt > deadzone && facing == -1))
                    ReverseImage();

                //jumping from idle
                if (jumpKey)
                {
                    if (!doubleJumping)
                    {
                        if (jumpCounter == 0)
                            jumpCounter += 1;
                        if (jumpCounter == 2)
                            jumpCounter += 1;
                        rb.velocity = new Vector2(rb.velocity.x, SPEED.y);
                    }
                    if (an.GetBool("jumping"))
                        doubleJumping = true;
                }

                //start blasting
                isBlasting = true;
                if(specialHold && lastBlast == 0.0f)
                {
                    shoot("Zeroneutralb", 1);
                }

                //end blasting on release
                else if (!specialHold)
                {
                    an.SetTrigger("release b");
                    isBlasting = false;
                }

                //change to walking
                if((Mathf.Abs(hAlt) > deadzone) || (Mathf.Abs(h) > deadzone))
                {
                    an.SetFloat("speed", 15);
                }
                else if ((Mathf.Abs(hAlt) < deadzone) && (Mathf.Abs(h) < deadzone))
                {
                    an.SetFloat("speed", 0);
                }
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("neutral b walk"))
            {
                //change walking direction and start moving
                if ((h < 0 && facing == 1) || hAlt < -deadzone && facing == 1)
                    ReverseImage();
                else if ((h > 0 && facing == -1) || (hAlt > deadzone && facing == -1))
                    ReverseImage();
                rb.velocity = new Vector2(facing * SPEED.x / 1.5f, rb.velocity.y);

                //jumping from walking 
                if (jumpKey)
                {
                    if (!doubleJumping)
                    {
                        if (jumpCounter == 0)
                            jumpCounter += 1;
                        if (jumpCounter == 2)
                            jumpCounter += 1;
                        rb.velocity = new Vector2(rb.velocity.x, SPEED.y);
                    }
                    if (an.GetBool("jumping"))
                        doubleJumping = true;
                }

                //start blasting
                isBlasting = true;
                if (specialHold && lastBlast == 0.0f)
                {
                    shoot("Zeroneutralb", 1);
                }

                //end blasting on release
                else if (!specialHold)
                {
                    an.SetTrigger("release b");
                    isBlasting = false;
                }

                //change to idle
                if ((Mathf.Abs(hAlt) < deadzone) && (Mathf.Abs(h) < deadzone))
                {
                    an.SetFloat("speed", 0);
                }
                else if ((Mathf.Abs(hAlt) > deadzone) || (Mathf.Abs(h) > deadzone))
                {
                    an.SetFloat("speed", 15);
                }
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("neutral b jump"))
            {
                //change walking direction and start moving
                if ((h < 0 && facing == 1) || hAlt < -deadzone && facing == 1)
                    ReverseImage();
                else if ((h > 0 && facing == -1) || (hAlt > deadzone && facing == -1))
                    ReverseImage();
                if (h != 0)
                {
                    rb.velocity = new Vector2(h * SPEED.x / 1.5f, rb.velocity.y);
                }
                else
                {
                    rb.velocity = new Vector2(hAlt * SPEED.x / 1.5f, rb.velocity.y);
                }

                //start blasting
                isBlasting = true;
                if (specialHold && lastBlast == 0.0f)
                {
                    shoot("Zeroneutralb", 1);
                }

                //end blasting on release
                else if (!specialHold)
                {
                    an.SetTrigger("release b");
                    isBlasting = false;
                }

                //keep track of movement
                if ((Mathf.Abs(hAlt) > deadzone) || (Mathf.Abs(h) > deadzone))
                {
                    an.SetFloat("speed", 15);
                }
                else if ((Mathf.Abs(hAlt) < deadzone) && (Mathf.Abs(h) < deadzone))
                {
                    an.SetFloat("speed", 0);
                }
            }

            //keep track of when last plast was shot
            if (isBlasting)
            {
                lastBlast += Time.deltaTime;
            }
            if(!isBlasting || lastBlast > blastAnimTime)
            {
                lastBlast = 0.0f;
            }
        }
    }

    //shooting
    public void shoot(string noise, int type)
    {
        GameObject ammo = null;
        switch (type)
        {
            case 1:
                ammo = blast;
                spawnedProjectile = 1;
                break;
            case 2:
                ammo = nairShot;
                spawnedProjectile = 2;
                break;
            default:
                break;
        }
        sound.Play(noise);
        GameObject shot = Instantiate(ammo, new Vector3(transform.position.x + (5.5f * facing), transform.position.y + 4.0f, 0), Quaternion.identity);
        shot.GetComponent<BlastController>().zero = this;
        shot.GetComponent<BlastController>().setFacing(facing);
        projList.Add(shot);
    }
    public void nair()
    {
        if (!UtilStaticVars.IsReplay)
        {
            shoot("Zeronair", 2);
        }
    }
    public override void InstantiateProjectile(int type)
    {
        if (type == 1)
        {
            shoot("Zeroneutralb", 1);
        }
        else if(type == 2)
        {
            shoot("Zeronair", 2);
        }

    }
    //short hop before dash attack hitbox by adding force to rb
    public void dashAttackHop()
    {
        rb.AddForce(new Vector2(0, 1000f));
    }

    /////////////////////////
    //Final Smash Functions//
    /////////////////////////
    
    //logic
    private void fsLogic()
    {
        //keeps them in the air
        fsVictim.rb.velocity = Vector2.zero;
        rb.velocity = Vector2.zero;
        fsVictim.transform.position = fsLocation2;
        transform.position = fsLocation1;
        hitting = true;
        fsVictim.an.SetBool("attacked", true);
        Camera.main.orthographicSize = 40f;
        Camera.main.transform.position = new Vector3(0.0f, 0.0f, Camera.main.transform.position.z);
    }

    //called once the initial final smash hit connects
    public void initiateFinalSmash(SmashCharacter enemy)
    {
        finalSmashStart = true;
        fsVictim = enemy;
        fsVictim.rb.velocity = Vector2.zero;
        rb.velocity = Vector2.zero;
        fsVictim.transform.position = fsLocation2;
        transform.position = fsLocation1;
        hitting = true;
        if(facing != 1)
        {
            ReverseImage();
        }
        facing = 1;
        an.SetTrigger("final smash trigger");
    }

    //activates X for final smash
    public void activateX()
    {
        megamanX.SetActive(true);
        megamanX.transform.position = fsLocation3;
    }

    public void endFinalSmash()
    {
        sound.Play("Zerofinalsmashexplosion");
        megamanX.SetActive(false);
        if (!UtilStaticVars.IsReplay)
        {
            finalSmashStart = false;
            StartCoroutine(Damage(fsVictim.gameObject.name, fsDmg, fsKnockback, fsHitStun));
            hitting = false;
            fsVictim.an.SetBool("attacked", true);
            an.SetBool("final smash", false);
        }
    }

    /////////////////////////////
    //Final Smash Functions end//
    /////////////////////////////

    public override void Kill()
    {
        base.Kill();
        //sound.Play("Zerodeath");
    }

    public override void LoseStock()
    {
        sound.Play("Zerodeath");
        base.LoseStock();
    }
}
