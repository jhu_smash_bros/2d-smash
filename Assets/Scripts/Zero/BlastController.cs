﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlastController : MonoBehaviour {
    public int hitStun;
    public float speed;
    public float facing;
    public float lifespan = 4.3f;
    public float dmg;
    public Vector2 power;
    public ZeroController zero;
    private bool collisionOccurred = false;
    private float timer = 0f;



    private void Start()
    {
        facing = zero.facing;
        //Debug.Log(zero.facing);
        if (facing < 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
    }
    void FixedUpdate () {
        //don't fixed update during replay
        if (!UtilStaticVars.Pause && !UtilStaticVars.IsReplay)
        {
            transform.position += Vector3.right * facing * speed * Time.deltaTime;
            timer += Time.deltaTime;
            if (timer > lifespan || collisionOccurred)
            {
                zero.hitting = false;
                zero.RemoveProjectile(this.gameObject);
                Destroy(this.gameObject);
            }
        }
	}
    public void setFacing(float face)
    {
        facing = face;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        //damage character on hit
        if ((other.tag == "Character" && other.GetComponent<BodyController>().player != zero) || other.tag == "Shield" && !other.gameObject.GetComponentInParent<SmashCharacter>().dodging)
        {
            collisionOccurred = true;
            StartCoroutine(zero.Damage(other.attachedRigidbody.name, dmg, power, hitStun));
        }
    }
}
