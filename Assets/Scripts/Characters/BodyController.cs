﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyController : MonoBehaviour
{
    public SmashCharacter player;
    private SmashCharacter enemy;

    void OnTriggerEnter2D(Collider2D other)
    {
        // for attacks
        switch (other.tag)
        {
            case "Weapon":
                enemy = other.GetComponentInParent<SmashCharacter>();
                break;
            case "Fire":
                enemy = other.GetComponent<ScorpionFire>().scorp;
                break;
            case "Blast":
                if(other.name.Contains("Sonic"))
                    enemy = other.GetComponent<SonicBoomController>().guile;
                else
                    enemy = other.GetComponent<BlastController>().zero;
                break;
            case "Arrow":
                enemy = other.GetComponent<ArrowController>().parent;
                break;
            case "Fball":
                enemy = other.GetComponent<FballController>().shadow;
                break;
            case "Spark":
                enemy = other.GetComponent<ShadowHitboxController>().shadow;
                break;
            default:
                break;
        }
        // for ledges
        if (other.tag == "Ledge")
        {
            if (other.name[0] == 'r')
                player.rLedgeTouched = true;
            else if (other.name[0] == 'l')
                player.lLedgeTouched = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        // for ledges
        if (other.tag == "Ledge")
        {
            player.rLedgeTouched = false;
            player.lLedgeTouched = false;
        }
        if (other.tag == "Ledge Grab"
            && player.rb.velocity.y < 0f
            && player.transform.position.y < -22f)
        {
            player.an.SetBool("hanging", true);
            if ((player.facing < 0 && other.name[0] == 'l')
                || (player.facing > 0 && other.name[0] == 'r'))
            {
                player.ReverseImage();
            }
        }
    }

    void Update()
    {
        try
        {
            if (player.an.GetBool("attacked"))
                if (!enemy.hitting && !player.inHitStun)
                    player.an.SetBool("attacked", false);
        }
        catch
        {
            
        }
    }
}
