﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GenericAI : MonoBehaviour
{
    public float v;
    public float h;
    public float vAlt;
    public float hAlt;
    public bool tauntKey = false;
    public bool shieldKey = false;
    public bool attackKey = false;
    public bool specialKey = false;
    public bool specialHold = false;
    public bool finalSmashKey = false;
    public bool lKey = false;
    public bool rKey = false;
    public bool jumpKey = false;

    protected SmashCharacter enemy1;
    protected SmashCharacter enemy2;
    protected SmashCharacter enemy3;
    void FixedUpdate()
    {
        try
        {
            int enemyNumber = 1;
            foreach (Transform target in Camera.main.GetComponent<CameraFollower>().targets)
            {
                if (!target.gameObject.GetComponent<SmashCharacter>().dead && target != transform)
                {
                    switch (enemyNumber)
                    {
                        case 1:
                            enemy1 = target.gameObject.GetComponent<SmashCharacter>();
                            break;
                        case 2:
                            enemy2 = target.gameObject.GetComponent<SmashCharacter>();
                            break;
                        case 3:
                            enemy3 = target.gameObject.GetComponent<SmashCharacter>();
                            break;
                        default:
                            break;
                    }
                    enemyNumber++;
                }
            }
        }
        catch
        {

        }
    }

    public abstract void inputSim();

    protected void inputClear()
    {
        v = 0;
        h = 0;
        vAlt = 0;
        hAlt = 0;
        tauntKey = false;
        shieldKey = false;
        attackKey = false;
        specialKey = false;
        specialHold = false;
        finalSmashKey = false;
        lKey = false;
        rKey = false;
        jumpKey = false;
    }
}
