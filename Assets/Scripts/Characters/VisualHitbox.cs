﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualHitbox : MonoBehaviour
{
    public SpriteRenderer render;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!UtilStaticVars.ShowHitbox || UtilStaticVars.RealGame)
        {
            render.enabled = false;
        }
        else
        {
            render.enabled = true;
        }
    }
}
