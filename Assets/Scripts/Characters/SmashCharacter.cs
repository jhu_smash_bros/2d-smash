﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SmashCharacter : MonoBehaviour
{
    public int player;
    public int facing;
    public int stocks = 1;
    public bool active;
    public bool dead = false;
    [HideInInspector]
    public bool respawning = false;

    public AudioManager sound;

    public TextMeshProUGUI perText;
    public float percentage = 0f;
    [HideInInspector]
    public float percentageGoal = 0f;

	public Vector2 SPEED;
    public Vector3 RESET;
    public Vector3 shift;

    [HideInInspector]
	public Animator an;
    [HideInInspector]
	public Rigidbody2D rb;
    [HideInInspector]
    public SpriteRenderer rd;
    
    // variables for attacks
    [HideInInspector]
    public bool hitting = false;
    [HideInInspector]
    public bool dodging = false;
    [HideInInspector]
    public bool shielding = false;
    [HideInInspector]
    public bool countering = false;
    [HideInInspector]
    public bool sideDodging = false;
    [HideInInspector]
    public bool shieldBroken = false;
    [HideInInspector]
    public float shieldHealth = 100f;

    // variables for movement
    [HideInInspector]
    public bool falling = false;
    [HideInInspector]
    public bool dropping = false;
    [HideInInspector]
    public bool startDash = false;
    [HideInInspector]
    public float jumpCounter = 0f;
    [HideInInspector]
    public bool rLedgeTouched = false;
    [HideInInspector]
    public bool lLedgeTouched = false;

    //hitstun vars
    [HideInInspector]
    public bool inHitStun = false;
    private float hitStunDuration = 0.0f;
    private float hitStunTimer = 0.0f;
    

    //replay vars
    public ReplayData repData;
    protected int frameNum = 0;

    //final smash vars
    private float finalSmashRate = 1.800f;
    private float finalSmashTimer = 0.00f;
    protected int finalSmashMeter = 0;
    public bool permaSmash = false;
    public bool hasFinalSmash = false;
    //[HideInInspector]
    public Transform finalSmashBar = null;

    private float time1;
    private float time2;
    private float time3;
    private float width;
    private float height;
    private float smashBar;
    private float movement;
    private float hangingTime = 0f;
    private float respawnTime = 0f;
    private float fallMultiplier = 2f;
    private float sideDodgeTimer = 0f;
    private float sideDodgingDir = 0f;
    private float shieldStunTimer = 0f;
    private float shieldStunDuration = 0f;
    private float invincibilityTimer = 0f;
    private float invincibilityDuration = 3f;

    private int numLTap = 0;
    private int numRTap = 0;
    private int shieldRechargeBuffer = 0;
    private int shieldBreakBuffer = 0;

    //velocity of RB before pause
    private Vector2 storedVel = Vector2.zero;

    //the AI vars
    [SerializeField]
    private GenericAI ai;
    public bool isCPU;

    protected List<GameObject> projList = new List<GameObject>();
    protected List<int> removedProjList = new List<int>();
    protected int spawnedProjectile = 0;
    
    //deadzone of joystick
    protected float deadzone = 0.075f;
    protected float v;
    protected float h;
    protected float vAlt;
    protected float hAlt;
    protected bool tauntKey = false;
    protected bool shieldKey = false;
    protected bool attackKey = false;
    protected bool specialKey = false;
    protected bool specialHold = false;
    protected bool finalSmashKey = false;
    protected bool lKey = false;
    protected bool rKey = false;
    protected bool jumpKey = false;
    protected bool inShieldStun = false;
    protected bool isPaused = false;
    public bool doubleJumping { get; protected set; } = false;
    protected bool isInvincible = false;

    private bool dashing = false;
    private bool airUpB = false;
    private bool airSideB = false;

    protected Color spriteColor;

	public virtual void Start()
    {
		an = GetComponent<Animator>();
		rb = GetComponent<Rigidbody2D>();
        rd = GetComponent<SpriteRenderer>();

        try
        {
            if (!UtilStaticVars.IsReplay)
            {
                repData = GameObject.Find("FightingStage").GetComponent<ReplayData>();
            }
            else
            {
                repData = UtilStaticVars.Replay;
            }
        }
        catch (Exception)
        {

            repData = null;
        }

        width = rd.bounds.size.x;
        height = rd.bounds.size.y;
        shift = new Vector3(0f, 0f, 0f);

        spriteColor = rd.color;
        
        if ( (transform.position.x < 0 && facing == -1) || (transform.position.x > 0 && facing == 1) ) ReverseImage();
    }

    private void LateUpdate()
    {
        //respawn invincibility color change
        if (isInvincible)
        {
            colorChange(1);
        }
        else
        {
            if (!an.GetBool("stunned")) colorChange(0);
        }
    }

    public virtual void Update()
    {
        // reset pos with [r]
        if (Input.GetKeyDown(KeyCode.R))
        {
            dead = false;
            stocks = 3;
            percentage = 0f;
            percentageGoal = 0f;
            rb.gravityScale = 1;
            transform.position = RESET;
            rb.velocity = new Vector2(0f, 0f);
        }

        //replay stuff
        if (UtilStaticVars.IsReplay && !dead)
        {
            Frame f = repData.getReplayInput(frameNum);
            switch (player)
            {
                case 1:
                    transform.position = f.p1.pos;
                    if((f.p1.flipped == true && facing == 1) || (f.p1.flipped == false && facing == -1))
                    {
                        ReverseImage();
                    }
                    isInvincible = f.p1.inv;
                    percentage = f.p1.percent;
                    if (stocks == f.p1.stocks + 1)
                    {
                        rd.sortingOrder = 0;
                    }
                    stocks = f.p1.stocks;
                    finalSmashMeter = f.p1.fsMeter;
                    if (!an.GetCurrentAnimatorStateInfo(0).IsName(f.p1.animation))
                    {
                        an.Play(f.p1.animation);
                    }
                    if(f.p1.spawnProjectile != 0)
                    {
                        InstantiateProjectile(f.p1.spawnProjectile);
                    }
                    for (int i = 0; i < f.p1.destroyedProj.Count; i++)
                    {
                        DestroyProjectile(projList[f.p1.destroyedProj[i]]);
                    }
                    for (int i = 0; i < f.p1.projPos.Count; i++)
                    {
                        if (projList[i].transform.position == f.p1.projPos[i] && f.p1.spawnProjectile == 0)
                        {
                            stillProj();
                        }
                        else
                        {
                            projList[i].transform.position = f.p1.projPos[i];
                        }
                    }
                    break;
                case 2:
                    transform.position = f.p2.pos;
                    if ((f.p2.flipped == true && facing == 1) || (f.p2.flipped == false && facing == -1))
                    {
                        ReverseImage();
                    }
                    isInvincible = f.p2.inv;
                    percentage = f.p2.percent;
                    if (stocks == f.p2.stocks + 1)
                    {
                        rd.sortingOrder = 0;
                    }
                    stocks = f.p2.stocks;
                    finalSmashMeter = f.p2.fsMeter;
                    if (!an.GetCurrentAnimatorStateInfo(0).IsName(f.p2.animation))
                    {
                        an.Play(f.p2.animation);
                    }
                    if (f.p2.spawnProjectile != 0)
                    {
                        InstantiateProjectile(f.p2.spawnProjectile);
                    }
                    for (int i = 0; i < f.p2.destroyedProj.Count; i++)
                    {
                        DestroyProjectile(projList[f.p2.destroyedProj[i]]);
                    }
                    for (int i = 0; i < f.p2.projPos.Count; i++)
                    {
                        if (projList[i].transform.position == f.p2.projPos[i] && f.p2.spawnProjectile == 0)
                        {
                            stillProj();
                        }
                        else
                        {
                            projList[i].transform.position = f.p2.projPos[i];
                        }
                    }
                    break;
                case 3:
                    transform.position = f.p3.pos;
                    if ((f.p3.flipped == true && facing == 1) || (f.p3.flipped == false && facing == -1))
                    {
                        ReverseImage();
                    }
                    isInvincible = f.p3.inv;
                    percentage = f.p3.percent;
                    if (stocks == f.p3.stocks + 1)
                    {
                        rd.sortingOrder = 0;
                    }
                    stocks = f.p3.stocks;
                    finalSmashMeter = f.p3.fsMeter;
                    if (!an.GetCurrentAnimatorStateInfo(0).IsName(f.p3.animation))
                    {
                        an.Play(f.p3.animation);
                    }
                    if (f.p3.spawnProjectile != 0)
                    {
                        InstantiateProjectile(f.p3.spawnProjectile);
                    }
                    for (int i = 0; i < f.p3.destroyedProj.Count; i++)
                    {
                        DestroyProjectile(projList[f.p3.destroyedProj[i]]);
                    }
                    for (int i = 0; i < f.p3.projPos.Count; i++)
                    {
                        if (projList[i].transform.position == f.p3.projPos[i] && f.p3.spawnProjectile == 0)
                        {
                            stillProj();
                        }
                        else
                        {
                            projList[i].transform.position = f.p3.projPos[i];
                        }
                    }
                    break;
                case 4:
                    transform.position = f.p4.pos;
                    if ((f.p4.flipped == true && facing == 1) || (f.p4.flipped == false && facing == -1))
                    {
                        ReverseImage();
                    }
                    isInvincible = f.p4.inv;
                    percentage = f.p4.percent;
                    if (stocks == f.p4.stocks + 1)
                    {
                        rd.sortingOrder = 0;
                    }
                    stocks = f.p4.stocks;
                    finalSmashMeter = f.p4.fsMeter;
                    if (!an.GetCurrentAnimatorStateInfo(0).IsName(f.p4.animation))
                    {
                        an.Play(f.p4.animation);
                    }
                    if (f.p4.spawnProjectile != 0)
                    {
                        InstantiateProjectile(f.p4.spawnProjectile);
                    }
                    for (int i = 0; i < f.p4.destroyedProj.Count; i++)
                    {
                        DestroyProjectile(projList[f.p4.destroyedProj[i]]);
                    }
                    for (int i = 0; i < f.p4.projPos.Count; i++)
                    {
                        if (projList[i].transform.position == f.p4.projPos[i] && f.p4.spawnProjectile == 0)
                        {
                            stillProj();
                        }
                        else
                        {
                            projList[i].transform.position = f.p4.projPos[i];
                        }
                    }
                    break;
                default:
                    break;
            }
            if(stocks == 0)
            {
                Kill();
                transform.position = new Vector3(1000f, 1000f, 0f);
            }
            if (isInvincible)
            {
                rd.sortingOrder = 6;
            }
            frameNum++;
        }

        /********
        PECENTAGE
        *********/
        if (percentage < percentageGoal)
            percentage += 1;
        if (percentage > 999f)
            percentage = 999f;
        // color change
        float ratio = 1f - percentage / 300;
        if (ratio < 0) ratio = 0;
        byte colorB = (byte)(255 * ratio);
        if (percentage > 0)
            perText.color = new Color32(255, colorB, 0, 255);
        else
            perText.color = new Color32(255, 255, 255, 255);
        perText.text = percentage.ToString("0") + "%";
        //stock display
        perText.transform.parent.Find("Name").Find("StockCount").GetComponent<TextMeshProUGUI>().text = "x " + stocks.ToString();
        //final smash bar growth and repositioning
        finalSmashBar.localScale = new Vector3((float)finalSmashMeter * .01f, finalSmashBar.localScale.y,
            finalSmashBar.localScale.z);
        finalSmashBar.localPosition = new Vector3(-7.00f + 7f * finalSmashBar.localScale.x, 0f);

        if (!UtilStaticVars.IsReplay)
        {
            //adds frame data to the replaydata if frame is off somehow, doesn't update
            if (repData != null)
            {
                // causing a warning
                /*
                if (repData.addFrame(frameNum, this))
                {
                    frameNum++;
                }
                */
            }
        }
    }

	public virtual void FixedUpdate()
    {
        //don't run fixed update stuff during replay
        if (UtilStaticVars.IsReplay)
        {
            return;
        }
        // can't do anything when dead
        if (dead)
        {
            transform.position = new Vector3(1000f, 1000f, 0f);
            return;
        }

        // while respawning
        if (respawning)
        {
            if (respawnTime < 2f)
            {
                rd.sortingOrder = 0;
                rb.gravityScale = 0f;
                rb.velocity = Vector2.zero;
                transform.position = new Vector3(RESET.x, RESET.y + 4f, RESET.z);

                respawnTime += Time.deltaTime;
            }
            else
            {
                if ( (transform.position.x < 0 && facing == -1) || (transform.position.x > 0 && facing == 1) ) ReverseImage();

                colorChange(1);
                rd.sortingOrder = 6;

                respawnTime = 0f;
                respawning = false;

                isInvincible = true;
                invincibilityTimer = 0.0f;

                percentage = 0f;
                percentageGoal = 0f;
            }
        }

        // pausing the game
        if (UtilStaticVars.Pause)
        {
            isPaused = true;
            an.enabled = false;
            if(storedVel == Vector2.zero)
            {
                storedVel = rb.velocity;
                rb.velocity = Vector2.zero;
                rb.gravityScale = 0;
            }
            return;
        }
        else
        {
            if (isPaused)
            {
                isPaused = false;
                an.enabled = true;
                if (storedVel != Vector2.zero)
                {
                    rb.velocity = storedVel;
                    storedVel = Vector2.zero;
                    rb.gravityScale = 1;
                }
            }
        }

        // if shield broken, you're stunned and can't act.
        if (shieldBroken)
        {
            shielding = false;
            an.SetBool("stunned", shieldBroken);

            if (shieldHealth == 0f)
                Play("Break");
            if (shieldHealth == 8f)
            {
                Play("Dizzy");
                an.SetBool("shield", shielding);
            }
            if (shieldHealth == 98f)
                Stop("Dizzy");

            handleShield();
            return;
        }

        //respawn invincibility
        if (isInvincible)
        {
            invincibilityTimer += Time.deltaTime;
            transform.Find("Body").gameObject.tag = "Untagged";
            if (invincibilityTimer > invincibilityDuration)
            {
                transform.Find("Body").gameObject.tag = "Character";
                isInvincible = false;
                invincibilityTimer = 0.0f;
            }
        }
        if (!UtilStaticVars.IsReplay)
        {
            if (isCPU)
            {
                ai.inputSim();
                v = ai.v;
                h = ai.h;
                vAlt = ai.vAlt;
                hAlt = ai.hAlt;
                tauntKey = ai.tauntKey;
                shieldKey = ai.shieldKey;
                attackKey = ai.attackKey;
                specialKey = ai.specialKey;
                specialHold = ai.specialHold;
                finalSmashKey = ai.finalSmashKey;
                lKey = ai.lKey;
                rKey = ai.rKey;
                jumpKey = ai.jumpKey;
            }
            else
            {
                v = 0;
                h = 0;
                vAlt = 0;
                hAlt = 0;
                tauntKey = false;
                shieldKey = false;
                attackKey = false;
                specialKey = false;
                specialHold = false;
                finalSmashKey = false;
                lKey = false;
                rKey = false;
                jumpKey = false;
            }
            if (!UtilStaticVars.LockInput)
            {
                // button keybinding for all players
                switch (player)
                {
                    case 1:
                        v = Input.GetAxisRaw("Vertical");
                        h = Input.GetAxisRaw("Horizontal");
                        vAlt = Input.GetAxisRaw("VerticalAlt");
                        hAlt = Input.GetAxisRaw("HorizontalAlt");
                        tauntKey = Input.GetButtonDown("Taunt");
                        shieldKey = Input.GetButton("Shield") || Input.GetButton("AltShield");
                        attackKey = Input.GetButtonDown("Attack");
                        specialKey = Input.GetButtonDown("Special");
                        specialHold = Input.GetButton("Special");
                        finalSmashKey = Input.GetButtonDown("FinalSmash");
                        lKey = Input.GetKeyDown(KeyCode.A);
                        rKey = Input.GetKeyDown(KeyCode.D);
                        jumpKey = Input.GetButtonDown("Jump") || Input.GetButtonDown("AltJump");
                        break;

                    case 2:
                        v = Input.GetAxisRaw("Vertical2");
                        h = Input.GetAxisRaw("Horizontal2");
                        vAlt = Input.GetAxisRaw("Vertical2Alt");
                        hAlt = Input.GetAxisRaw("Horizontal2Alt");
                        tauntKey = Input.GetButtonDown("Taunt2");
                        shieldKey = Input.GetButton("Shield2") || Input.GetButton("AltShield2");
                        attackKey = Input.GetButtonDown("Attack2");
                        specialKey = Input.GetButtonDown("Special2");
                        specialHold = Input.GetButton("Special2");
                        finalSmashKey = Input.GetButtonDown("FinalSmash2");
                        lKey = Input.GetKeyDown(KeyCode.LeftArrow);
                        rKey = Input.GetKeyDown(KeyCode.RightArrow);
                        jumpKey = Input.GetButtonDown("Jump2") || Input.GetButtonDown("AltJump2");
                        break;

                    case 3:
                        v = 0;
                        h = 0;
                        vAlt = Input.GetAxisRaw("Vertical3");
                        hAlt = Input.GetAxisRaw("Horizontal3");
                        tauntKey = Input.GetButtonDown("Taunt3");
                        shieldKey = Input.GetButton("Shield3") || Input.GetButton("AltShield3");
                        attackKey = Input.GetButtonDown("Attack3");
                        specialKey = Input.GetButtonDown("Special3");
                        specialHold = Input.GetButton("Special3");
                        finalSmashKey = Input.GetButtonDown("FinalSmash3");
                        lKey = false;
                        rKey = false;
                        jumpKey = Input.GetButtonDown("Jump3") || Input.GetButtonDown("AltJump3");
                        break;

                    case 4:
                        v = 0;
                        h = 0;
                        vAlt = Input.GetAxisRaw("Vertical4");
                        hAlt = Input.GetAxisRaw("Horizontal4");
                        tauntKey = Input.GetButtonDown("Taunt4");
                        shieldKey = Input.GetButton("Shield4") || Input.GetButton("AltShield4");
                        attackKey = Input.GetButtonDown("Attack4");
                        specialKey = Input.GetButtonDown("Special4");
                        specialHold = Input.GetButton("Special4");
                        finalSmashKey = Input.GetButtonDown("FinalSmash4");
                        lKey = false;
                        rKey = false;
                        jumpKey = Input.GetButtonDown("Jump4") || Input.GetButtonDown("AltJump4");
                        break;

                    default:
                        break;
                }
            }
            an.SetFloat("crouch", v);
        }

        //check hitStun
        if (inHitStun)
        {
            if(hitStunTimer >= hitStunDuration)
            {
                inHitStun = false;
                hitStunDuration = 0.0f;
                hitStunTimer = 0.0f;
                an.SetBool("attacked", false);
            }
            else
            {
                hitStunTimer += Time.deltaTime;
                an.SetBool("attacked", true);
            }
        }

        // fall faster & only jump twice
        checkJumping();

        //final smash meter build up with time
        //but if perma smash always have final smash
        if (permaSmash)
        {
            permaFS();
        }
        if (!hasFinalSmash)
        {
            finalSmashTimer += Time.deltaTime;
            if (finalSmashTimer > finalSmashRate)
            {
                finalSmashTimer -= finalSmashRate;
                finalSmashMeter++;
                if(finalSmashMeter == 100)
                {
                    hasFinalSmash = true;
                }
            }
        }
        /*********************
        IDLE, JUMP, WALK, DASH
        **********************/
        if (an.GetCurrentAnimatorStateInfo(0).IsName("idle")
            || an.GetCurrentAnimatorStateInfo(0).IsName("jump")
            || an.GetCurrentAnimatorStateInfo(0).IsName("walk")
            || an.GetCurrentAnimatorStateInfo(0).IsName("dash")
            || an.GetCurrentAnimatorStateInfo(0).IsName("dash end")
            || an.GetCurrentAnimatorStateInfo(0).IsName("jump 1")
            || an.GetCurrentAnimatorStateInfo(0).IsName("jump 2")
            || an.GetCurrentAnimatorStateInfo(0).IsName("walk 1")
            || an.GetCurrentAnimatorStateInfo(0).IsName("walk 2")
            || an.GetCurrentAnimatorStateInfo(0).IsName("falling")
            || an.GetCurrentAnimatorStateInfo(0).IsName("dashTrans"))
        {
            dodging = false;
            hitting = false;
            sideDodgingDir = 0f;
            an.SetBool("final smash", false);

            shielding = false;
            an.SetBool("shield", shielding);
            an.SetBool("stunned", shieldBroken);

            // moving, crouching, jumping
            checkMovement();

            // reset the gravity back to 1
            if (rb.gravityScale != 1)
                rb.gravityScale = 1;

            // taunt with [e] or [f]
            if (tauntKey)
                an.SetTrigger("taunt");

            // shield with [k] or [f] only when idle
            else if (shieldKey && !shieldBroken)
            {
                if (an.GetCurrentAnimatorStateInfo(0).IsName("idle"))
                {
                    shielding = true;
                    an.SetBool("shield", shielding);
                    transform.Find("Shield").gameObject.SetActive(true);
                }
            }

            // a attack with [o] or [z]
            else if (attackKey)
            {
                an.SetTrigger("a");
            }

            // b attack with [p] or [x]
            else if (specialKey)
            {
                // neutral b
                if (h == 0 && v == 0 && Mathf.Abs(vAlt) < deadzone && Mathf.Abs(hAlt) < deadzone)
                    an.SetTrigger("b");

                // up b
                else if ((v > 0 && Mathf.Abs(v) > Mathf.Abs(h)) || (vAlt > deadzone && Mathf.Abs(vAlt) > Mathf.Abs(hAlt)))
                {
                    if (!airUpB)
                    {
                        airUpB = true;
                        an.SetTrigger("up b");
                    }
                }

                // side b
                else if ((Mathf.Abs(h) > Mathf.Abs(v)) || Mathf.Abs(hAlt) > Mathf.Abs(vAlt))
                {
                    if (rb.velocity.y == 0)
                        an.SetTrigger("side b");
                    else
                    {
                        if (!airSideB)
                        {
                            airSideB = true;
                            an.SetTrigger("side b");
                        }
                    }
                }

                // down b
                else if ((v < 0 && Mathf.Abs(v) > Mathf.Abs(h)) || (vAlt < -deadzone && Mathf.Abs(vAlt) > Mathf.Abs(hAlt)))
                    an.SetTrigger("down b");
            }

            // final smash with [o] or [v]
            else if (finalSmashKey && hasFinalSmash)
            {
                an.SetBool("final smash", true);
                finalSmashMeter = 0;
                hasFinalSmash = false;
            }

            handleShield();
        }
        /*****
        CROUCH
        ******/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("crouch"))
        {
            if (vAlt < -0.5) { an.SetFloat("crouch", vAlt); }
            else
            {
                an.SetFloat("crouch", v);
            }            
            // can down b while crouching
            if (specialKey) 
                an.SetTrigger("down b");

            // can spot dodge when crouching
            if (shieldKey && !shieldBroken)
            {
                dodging = true;
                an.SetTrigger("dodge");
            }
            else
            {
                handleShield();
            }
        }
        /****
        OTHER
        *****/
        else
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y);
            handleShield();
        }

        /******
        HANGING
        *******/
        if (an.GetBool("hanging"))
        {
            // allow attacks after hanging
            airUpB = false;
            airSideB = false;
            // allow jumping after hanging
            falling = false;
            doubleJumping = false;
            // freeze the character while hanging
            rb.gravityScale = 0f;
            rb.velocity = Vector2.zero;
            hangingTime += Time.deltaTime; 

            // allow jumping while hanging
            if (jumpKey)
            {
                jumpCounter += 1;
                an.SetTrigger("jump");
                an.SetBool("hanging", false);
                rb.velocity = new Vector2(rb.velocity.x, SPEED.y);
            }

            if (hangingTime > 0.5f && (-1*h == facing || v != 0))
            {
                // start falling once you stop hanging
                hangingTime = 0f;
                rb.gravityScale = 1f;
                an.SetBool("hanging", false);
            }
        }
        
        /********
        SHIELDING
        *********/
        if (shielding)
        {
            if (shieldBroken)
            {
                shielding = false;
            }
            else if (shieldKey || inShieldStun)
            {
                handleShield();
            }
            else
            {
                shielding = false;
            }

            if ((h != 0 || Math.Abs(hAlt) > deadzone)&& sideDodgingDir == 0)
            {
                
                sideDodging = true;
                //sets direction of side dodge
                sideDodgingDir = (Math.Abs(h)> Math.Abs(hAlt))? h : hAlt;
                sideDodgingDir = (sideDodgingDir < 0) ? -1 : 1;
                transform.Find("Shield").gameObject.SetActive(false);
            }

            if (shielding == false)
            {
                an.SetBool("shield", shielding);
                transform.Find("Shield").gameObject.SetActive(false);
            }

        }

        /*********
        SIDE DODGE
        **********/
        if (sideDodging)
        {
            dodging = true;

            if (facing == sideDodgingDir)
            {
                ReverseImage();
            }
            an.SetTrigger("side dodge");
            sideDodgeTimer += Time.deltaTime;

            if (sideDodgeTimer < 0.5f)
            {
                if ((rLedgeTouched && sideDodgingDir == 1)
                    || lLedgeTouched && sideDodgingDir == -1)
                    rb.velocity = Vector2.zero;
                else
                    rb.velocity = new Vector2(20f * sideDodgingDir, rb.velocity.y);
            }
            else
            {
                dodging = false;
                sideDodging = false;
                sideDodgeTimer = 0f;
                an.ResetTrigger("side dodge");
                rb.velocity = new Vector2(0f, rb.velocity.y);
            }
        }

        if (!shielding)
        {
            transform.Find("Shield").gameObject.SetActive(false);
        }

        /*******
        PLATFORM
        ********/
        if (rb.velocity.y <= 0f && (transform.position.y > -3f || transform.position.y < -8f))
            if (!dropping) transform.Find("Feet").GetComponent<Collider2D>().enabled = true;
        if (rb.velocity.y > 0f)
            transform.Find("Feet").GetComponent<Collider2D>().enabled = false;
        // make sure body can't go through bottom platform
        if (transform.position.y < -16f)
            transform.Find("Body").GetComponent<Collider2D>().isTrigger = false;
        else
            transform.Find("Body").GetComponent<Collider2D>().isTrigger = true;
	}

    /*****
    DAMAGE
    ******/
    public IEnumerator Damage(string name, float perDamage, Vector2 knockPower, int hitStun, bool follow = false, float distance = 0, bool spike = false, float sStun = .01f, int sDamage = 5)
    {
        hitting = true;
        float timer = 0f;

        SmashCharacter enemy = GameObject.Find(name).GetComponent<SmashCharacter>();

        if (!enemy.shielding)
        {
            //if hit shieldbreak dizziness ends
            enemy.shieldBroken = false;
            enemy.an.SetBool("stunned", false);
            enemy.Stop("Dizzy");
            enemy.colorChange(0);

            // to trigger the counter
            if (enemy.countering)
                enemy.an.SetBool("attacked", true);
            
            if (!isPaused && !enemy.countering && !enemy.dodging && !dead)
            {
                if (!UtilStaticVars.RealGame)
                {
                    try
                    {
                        //update combo counter if in training mode
                        GameObject.Find("Combo Info").GetComponent<ComboCounter>().UpdateCombo(perDamage,
                            enemy.an.GetCurrentAnimatorStateInfo(0).IsName("hurt")
                            || enemy.an.GetCurrentAnimatorStateInfo(0).IsName("hurt 1")
                            || enemy.an.GetCurrentAnimatorStateInfo(0).IsName("hurt 2")
                            || enemy.an.GetCurrentAnimatorStateInfo(0).IsName("Hurt"));
                        Debug.Log(enemy.an.GetCurrentAnimatorClipInfo(0)[0].clip.name);
                    }
                    catch
                    {
                        
                    }
                }
                enemy.percentageGoal += perDamage;
                enemy.fsMeterInc((int)perDamage);
                enemy.an.SetBool("attacked", true);
                //turn hitstun from frames to secs
                enemy.setHitStun(hitStun / 60.0f);

                if (follow)
                {
                    enemy.rb.velocity = rb.velocity;
                    enemy.transform.position = new Vector3(transform.position.x + facing * distance, transform.position.y, 0f);
                }

                float scale = (enemy.percentage + 50) / 100;

                while (!isPaused && timer < 0.1f)
                {
                    float spikeDir = (spike && enemy.rb.velocity.y != 0) ? -1.0f : 1.0f;
                    timer += Time.deltaTime;
                    enemy.rb.AddForce(new Vector3(facing * knockPower.x * scale * 10, spikeDir * knockPower.y * scale * 10, 0f));
                }
            }
        }
        else
        {
            enemy.setShieldStun(sStun);
            enemy.shieldHealth -= sDamage;
            enemy.shieldHealth = enemy.shieldHealth < 0f ? 0f : enemy.shieldHealth;
            enemy.transform.Find("Shield").gameObject.GetComponent<SpriteRenderer>().color =
                new Color(1.0f, (enemy.shieldHealth * 0.01f), (enemy.shieldHealth * 0.01f), 110.0f/255.0f);
            if(enemy.shieldHealth < .01f)
            {
                enemy.shieldBroken = true;
                enemy.transform.Find("Shield").gameObject.SetActive(false);
            }
        }
        yield return 0;
    }

    /***
    KILL
    ****/
    public virtual void Kill()
    {
        dead = true;
        percentage = 0f;
        percentageGoal = 0f;
        rb.gravityScale = 0f;
        if (UtilStaticVars.RealGame)
        {
            Camera.main.GetComponent<CameraFollower>().AddLoser(this);
        }
    }

    /*********
    LOSE STOCK
    **********/
    public virtual void LoseStock()
    {
        if (UtilStaticVars.RealGame)
        {
            stocks--;
        }
        if (stocks <= 0)
        {
            Kill();
        }
        else
        {
            respawning = true;
        }
    }

    /****************
    PLAY & STOP MUSIC
    *****************/
    public void Play(string name)
    {
        if (!dead)
            sound.Play(name);
    }
    public void Stop(string name)
    {
        sound.Stop(name);
    }

    /*******
    MOVEMENT
    ********/
    void checkMovement()
    {
        // crouch with [s] or down
        if (vAlt < -0.5) { an.SetFloat("crouch", vAlt); }
        else
        {
            an.SetFloat("crouch", v);
        }

        // check for double jump
        if (jumpKey && !airUpB)
        {
            if (!doubleJumping)
            {
                if (jumpCounter == 0)
                {
                    jumpCounter += 1;
                }
                if (jumpCounter == 2)
                {
                    jumpCounter += 1;
                }
                an.SetTrigger("jump");
                rb.velocity = new Vector2(rb.velocity.x, SPEED.y);
            }
            if (an.GetBool("jumping") || an.GetBool("falling"))
            {
                doubleJumping = true;
                Debug.Log("Double Jumping");
            }
        }
        //move stick quickly to either side to dash on controller
        if(Math.Abs(hAlt) < deadzone && h == 0)
        {
            startDash = false;
            //dashing = false;
            time3 = Time.time;
        }
        if (Math.Abs(hAlt) > .99f)
        {
            if (Time.time - time3 < 0.2f && !dashing)
            {
                dashing = true;
                startDash = true;
            }
        }
        /*********
        DOUBLE TAP
        **********/
        // left key pressed
        if (lKey)
        {
            numRTap = 0;
            if (numLTap == 1)
            {
                if (Time.time - time1 < 0.4f)
                {
                    dashing = true;
                    startDash = true;
                }
                else
                    numLTap = 0;
            }
            if (numLTap == 0)
            {
                numLTap += 1;
                time1 = Time.time;
                dashing = false;
                startDash = false;
            }
        }
        // right key pressed
        if (rKey)
        {
            numLTap = 0;
            if (numRTap == 1)
            {
                if (Time.time - time2 < 0.4f)
                {
                    dashing = true;
                    startDash = true;
                }
                else
                    numRTap = 0;
            }
            if (numRTap == 0)
            {
                numRTap += 1;
                time2 = Time.time;
                dashing = false;
                startDash = false;
            }
        }
        
        // change speed for walking / dashing
        if ((h == 0) && (Mathf.Abs(hAlt) < deadzone))
        {
            if (rb.velocity.x < -4)
                rb.AddForce(new Vector3(50, 0f, 0f));
            else if (rb.velocity.x > 4)
                rb.AddForce(new Vector3(-50, 0f, 0f));
            else
                rb.velocity = new Vector2(0f, rb.velocity.y);
        }
        else
        {
            //using keyboard
            if (dashing && h != 0)
            {
                rb.AddForce(new Vector3(h * 200, 0f, 0f));

                if (Mathf.Abs(rb.velocity.x) >= 1.5f * SPEED.x)
                    rb.velocity = new Vector2(h * 1.5f * SPEED.x, rb.velocity.y);

            }
            else if (!dashing && h != 0)
            {
                rb.AddForce(new Vector3(h * 100, 0f, 0f));

                if (Mathf.Abs(rb.velocity.x) >= SPEED.x)
                    rb.velocity = new Vector2(h * SPEED.x, rb.velocity.y); 
            }
            //using joystick
            else if (dashing && h == 0)
            {
                rb.AddForce(new Vector3(hAlt * 200, 0f, 0f));

                if (Mathf.Abs(rb.velocity.x) >= 1.5f * SPEED.x)
                    rb.velocity = new Vector2(hAlt * 1.5f * SPEED.x, rb.velocity.y);

            }
            else 
            {
                rb.AddForce(new Vector3(hAlt * 100, 0f, 0f));

                if (Mathf.Abs(rb.velocity.x) >= SPEED.x)
                    rb.velocity = new Vector2(hAlt * SPEED.x, rb.velocity.y);
            }
        }

        // change the animation based on speed
        an.SetFloat("speed", Mathf.Abs(rb.velocity.x));

        // check which way the player is facing 
        if ((h < 0 && facing == 1) || hAlt < -deadzone && facing == 1)
            ReverseImage();
        else if ((h > 0 && facing == -1) || (hAlt > deadzone && facing == -1))
            ReverseImage();
    }

    // checks for jumping and falling
    void checkJumping()
    {
        // fall faster
        if (rb.velocity.y < 0)
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        else if (rb.velocity.y > 0)
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
    
        // change animation when jumping / falling
        if (falling && rb.velocity.y == 0 && rb.gravityScale != 0)
        {
            jumpCounter = 0f;

            airUpB = false;
            airSideB = false;

            falling = false;
            doubleJumping = false;
            an.SetBool("jumping", false);
            an.SetBool("freefall", false);
        }
        else if (rb.velocity.y < 0)
        {
            falling = true;
            jumpCounter = 2;
            an.SetBool("jumping", true);
        }    
        else if (rb.velocity.y > 0)
        {
            falling = false;
            an.SetBool("jumping", true);
        }
    }

    void handleShield()
    {
        // check if out of shield stun; if not, update shieldstun timer
        if (inShieldStun)
        {
            shieldStunTimer += Time.deltaTime;
            if(shieldStunTimer > shieldStunDuration)
            {
                shieldStunTimer = 0.0f;
                shieldStunDuration = 0.0f;
                inShieldStun = false;
            }
        }
        if (shielding) {
            shieldBreakBuffer++;
            if (shieldBreakBuffer == 5)
            {
                shieldBreakBuffer = 0;
                shieldHealth--;
                transform.Find("Shield").gameObject.GetComponent<SpriteRenderer>().color =
                    new Color(1.0f, (shieldHealth * 0.01f), (shieldHealth * 0.01f), 110.0f / 255.0f);
            }
            if(shieldHealth < 0.01f)
            {
                shieldBroken = true;
                transform.Find("Shield").gameObject.SetActive(false);
            }
        }

        // shield recovers while not shielding
        else
        {
            shieldRechargeBuffer++;
            if (shieldRechargeBuffer == 5)
            {
                shieldRechargeBuffer = 0;
                shieldHealth++;
            }
            // cap shield health at 100
            shieldHealth = shieldHealth > 100f ? 100f : shieldHealth;
            // shield gets less red when it recovers
            transform.Find("Shield").gameObject.GetComponent<SpriteRenderer>().color = 
                new Color(1.0f, (shieldHealth * 0.01f), (shieldHealth * 0.01f), 110.0f/255.0f);
            if(shieldHealth > 99f)
            {
                shieldBroken = false;
                an.SetBool("stunned", shieldBroken);
                colorChange(0);
            }
        }
    }

    public void setShieldStun(float ssTotal)
    {
        shieldStunDuration = ssTotal;
        shieldStunTimer = 0.0f;
        inShieldStun = true;
    }

    public void setHitStun(float hsTotal)
    {
        hitStunDuration = hsTotal;
        hitStunTimer = 0.0f;
        inHitStun = true;
    }


    // when you turn around, so should the sprite
    public void ReverseImage()
	{
		// reverse direction
		facing = -facing;
        
        // flip the character around 
		Vector2 theScale = rb.transform.localScale;
		theScale.x *= -1;
		rb.transform.localScale = theScale;

        if (facing == 1)
            transform.position +=  shift;
        if (facing == -1)
            transform.position -=  shift;
	}

    //change colors here so it can be left out of animation
    public Color colorChange(int color)
    {
        switch (color)
        {
            case 0:
                rd.color = spriteColor;
                break;
            case 1:
                rd.color = new Color(spriteColor.r, spriteColor.g, spriteColor.b, 0.4f);
                break;
            default:
                break;
        }
        return spriteColor;
    }
    //adding final smash meter from damage
    public void fsMeterInc(int amount)
    {
        finalSmashMeter += amount;
        if(finalSmashMeter >= 100)
        {
            hasFinalSmash = true;
            finalSmashMeter = 100;
        }
    }

    protected void permaFS()
    {
        hasFinalSmash = true;
        finalSmashMeter = 100;
    }

    /************************
    instatiating and destroying projectiles
    *************************/
    public virtual void InstantiateProjectile(int type)
    {

    }
    public void DestroyProjectile(GameObject proj)
    {
        projList.Remove(proj);
        Destroy(proj);
    }
    //remove projectile from list
    public void RemoveProjectile(GameObject proj)
    {
        int rem = projList.IndexOf(proj);
        projList.Remove(proj);
        removedProjList.Add(rem);
    }
    //if the projectile doesn't move
    public virtual void stillProj()
    {

    }

    public void addInputToFrameRecording(Frame f)
    {
        switch (Mathf.Abs(player))
        {
            case 1:
                f.p1.pos = transform.position;
                f.p1.flipped = facing == -1;
                f.p1.inv = isInvincible;
                f.p1.percent = percentage;
                f.p1.stocks = stocks;
                f.p1.fsMeter = finalSmashMeter;
                f.p1.animation = an.GetCurrentAnimatorClipInfo(0)[0].clip.name;
                f.p1.spawnProjectile = spawnedProjectile;
                for (int i = 0; i < removedProjList.Count; i++)
                {
                    f.p1.destroyedProj.Add(removedProjList[i]);
                }
                for (int i = 0; i < projList.Count; i++)
                {
                    f.p1.projPos.Add(projList[i].transform.position);
                }
                break;
            case 2:
                f.p2.pos = transform.position;
                f.p2.flipped = facing == -1;
                f.p2.inv = isInvincible;
                f.p2.percent = percentage;
                f.p2.stocks = stocks;
                f.p2.fsMeter = finalSmashMeter;
                f.p2.animation = an.GetCurrentAnimatorClipInfo(0)[0].clip.name;
                f.p2.spawnProjectile = spawnedProjectile;
                for (int i = 0; i < removedProjList.Count; i++)
                {
                    f.p2.destroyedProj.Add(removedProjList[i]);
                }
                for (int i = 0; i < projList.Count; i++)
                {
                    f.p2.projPos.Add(projList[i].transform.position);
                }
                break;
            case 3:
                f.p3.pos = transform.position;
                f.p3.flipped = facing == -1;
                f.p3.inv = isInvincible;
                f.p3.percent = percentage;
                f.p3.stocks = stocks;
                f.p3.fsMeter = finalSmashMeter;
                f.p3.animation = an.GetCurrentAnimatorClipInfo(0)[0].clip.name;
                f.p3.spawnProjectile = spawnedProjectile;
                for (int i = 0; i < removedProjList.Count; i++)
                {
                    f.p3.destroyedProj.Add(removedProjList[i]);
                }
                for (int i = 0; i < projList.Count; i++)
                {
                    f.p3.projPos.Add(projList[i].transform.position);
                }
                break;
            case 4:
                f.p4.pos = transform.position;
                f.p4.flipped = facing == -1;
                f.p4.inv = isInvincible;
                f.p4.percent = percentage;
                f.p4.stocks = stocks;
                f.p4.fsMeter = finalSmashMeter;
                f.p4.animation = an.GetCurrentAnimatorClipInfo(0)[0].clip.name;
                f.p4.spawnProjectile = spawnedProjectile;
                for (int i = 0; i < removedProjList.Count; i++)
                {
                    f.p4.destroyedProj.Add(removedProjList[i]);
                }
                for (int i = 0; i < projList.Count; i++)
                {
                    f.p4.projPos.Add(projList[i].transform.position);
                }
                break;
            default:
                break;
        }
        spawnedProjectile = 0;
        removedProjList.Clear();
    }
}
