﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TromboneController : MonoBehaviour
{
    private SansController sans;

    private string enemy;
    private bool colliding = false;

    void Start()
    {
        sans = GetComponentInParent<SansController>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Character" || other.tag == "Shield")
        {
            colliding = true;
            enemy = other.attachedRigidbody.name;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Character" || other.tag == "Shield")
        {
            colliding = false;
            sans.hitting = false;
        }
    }

    void FixedUpdate()
    {
        if (colliding)
        {
            // rigidbody, percent damage, knockback power

            if (sans.an.GetCurrentAnimatorStateInfo(0).IsName("trombone"))
            {
                Vector2 power = new Vector2(5f, 5f);
                StartCoroutine(sans.Damage(enemy, 1f, power, 0, false));
            }
            else
            {
                sans.hitting = false;
            }
        }
    }
}
