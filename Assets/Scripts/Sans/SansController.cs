﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SansController : SmashCharacter
{
    public override void Start()
    {
        base.Start();
    }

    public override void FixedUpdate()
    {
        //don't run fixed update stuff during replay
        if (UtilStaticVars.IsReplay)
        {
            return;
        }
        if (active)
            base.FixedUpdate();
    }

    // set the x vel
    public void SetVelX(float x)
    {
        rb.velocity = new Vector2(facing * x, rb.velocity.y);
    }
}
