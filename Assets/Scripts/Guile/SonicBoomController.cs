﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonicBoomController : MonoBehaviour
{
    public int hitStun;
    public float speed;
    public float facing;
    public float lifespan = 4.3f;
    public float dmg;
    public Vector2 power;
    public GuileController guile;
    public bool finalSmash;
    private bool collisionOccurred = false;
    private float timer = 0f;
    private float yPos;



    private void Start()
    {
        facing = guile.facing;
        if (facing < 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        yPos = transform.position.y;
    }
    void FixedUpdate()
    {
        //don't fixed update during replay
        if (!UtilStaticVars.Pause && !UtilStaticVars.IsReplay)
        {
            transform.position += Vector3.right * facing * speed * Time.deltaTime;
            if (!collisionOccurred )
            {
                timer += Time.deltaTime;
            }
            //if hit change animation and stop timer.
            else if(collisionOccurred && !finalSmash)
            {
                transform.position += Vector3.down  * 0.01f;
                GetComponent<Animator>().SetTrigger("hit");
            }
            if (timer > lifespan)
            {
                removeSelf();
            }
        }
        else if(UtilStaticVars.IsReplay && !finalSmash)
        {
            if(transform.position.y < yPos)
            {
                GetComponent<Animator>().SetTrigger("hit");
            }
        }
    }
    public void setFacing(float face)
    {
        facing = face;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        //damage character on hit
        if ((other.tag == "Character" && other.GetComponent<BodyController>().player != guile) || other.tag == "Shield")
        {
            collisionOccurred = true;
            if (other.tag == "Character")
            {
                if (other.gameObject.GetComponentInParent<SmashCharacter>().dodging)
                {
                    collisionOccurred = false;
                }
            }
            if (!finalSmash)
            {
                StartCoroutine(guile.Damage(other.attachedRigidbody.name, dmg, power, hitStun));
            }
            else
            {
                //Final Smash breaks shield
                StartCoroutine(guile.Damage(other.attachedRigidbody.name, dmg, power, hitStun, false,0,false,0, 100));
            }
        }
    }
    
    public void removeSelf()
    {
        if (!UtilStaticVars.Pause && !UtilStaticVars.IsReplay)
        {
            guile.hitting = false;
            guile.RemoveProjectile(this.gameObject);
            Destroy(this.gameObject);
        }
    }
}
