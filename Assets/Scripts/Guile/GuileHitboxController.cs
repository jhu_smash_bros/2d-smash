﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GuileHitboxController : MonoBehaviour
{
    [SerializeField]
    private Collider2D[] owner;
    [SerializeField]
    private GuileController guile;
    private float grabHeightOffset = 4.8f;
    private SmashCharacter enemy = null;
    public int sDmg;
    public int hitStun;
    public float dmgPercent;
    public float dist;
    public float sStun;
    public Vector2 knockback;
    public bool follow;
    public bool spike;
    public bool grab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //
        if (enemy != null && grab)
        {
            enemy.transform.position = new Vector3(transform.position.x, transform.position.y - grabHeightOffset, 0f);
        }
    }
    private void OnDisable()
    {
        if (!grab)
        {
            guile.hitting = false;
        }
        else if(enemy != null)
        {
            enemy = null;
        }

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Character" || other.tag == "Shield") {
            //neutral b grab
            if (grab)
            {
                guile.setGrabbing(true);
                guile.hitting = true;
                enemy = GameObject.Find(other.attachedRigidbody.name).GetComponent<SmashCharacter>();
                guile.enemy = enemy;

                //if hit shieldbreak dizziness ends
                enemy.shieldBroken = false;
                enemy.an.SetBool("stunned", false);
                enemy.Stop("Dizzy");
                enemy.colorChange(0);

                // goes through the counter
                if (enemy.countering)
                {
                    enemy.an.SetBool("countering", false);
                    enemy.an.SetBool("attacked", true);
                }

                if (!enemy.countering && !enemy.dodging)
                {
                    enemy.an.SetBool("attacked", true);
                }

            }
            //damage character on hit
            else if (!owner.Contains(other))
            {
                StartCoroutine(guile.Damage(other.attachedRigidbody.name, dmgPercent, knockback, hitStun, follow, dist, spike, sStun, sDmg));
            }
        }
    }
}
