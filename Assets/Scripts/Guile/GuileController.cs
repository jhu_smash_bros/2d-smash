﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuileController : SmashCharacter
{
    private bool midJab = false;
    private bool isGrabbing = false;
    private bool isThrowing = false;
    public SmashCharacter enemy = null;
    public GameObject sonicBoom;
    public GameObject sonicHurricane;

    public override void Update()
    {
        base.Update();
    }
    public override void FixedUpdate()
    {
        //don't run fixed update stuff during replay
        if (UtilStaticVars.IsReplay)
        {
            return;
        }
        if (active)
        {
            //determines if Guile is falling or jumping
            bool falling = false;
            if (rb.velocity.y < 0)
            {
                falling = true;
            }
            an.SetBool("falling", falling);
            base.FixedUpdate();

            //if paused do nothing 
            if (isPaused)
            {
                return;
            }
            if (isThrowing)
            {

                float multiplier;
                if(an.GetCurrentAnimatorStateInfo(0).IsName("neutral backwards"))
                {
                    multiplier = -1.0f;
                }
                else
                {
                    multiplier = 1.0f;
                }
                StartCoroutine(Damage(enemy.name, 15.0f, new Vector2(56.0f * multiplier, 10.0f), 1));
                hitting = false;
                isThrowing = false;
                enemy = null;
            }
            //if attacked or land you get your up b back
            if (an.GetBool("attacked") || an.GetCurrentAnimatorStateInfo(0).IsName("landing")
                || an.GetCurrentAnimatorStateInfo(0).IsName("hanging"))
            {
                an.SetBool("freefall", false);
            }
            //if in freefall don't allow another up b
            if (an.GetBool("freefall"))
            {
                an.ResetTrigger("up b");
            }

            //jab logic
            if (an.GetCurrentAnimatorStateInfo(0).IsName("jab 1"))
            {
                an.ResetTrigger("a");
                //if going to jab 2
                if (attackKey)
                {
                    an.SetTrigger("aa");
                    an.SetBool("endAttack", false);
                }
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("jab 2"))
            {
                an.ResetTrigger("aa");
                if (attackKey)
                {
                    an.SetTrigger("aaa");
                    an.SetBool("end jab", false);
                    midJab = true;
                }
                if (!midJab)
                {
                    an.SetBool("end jab", true);
                }
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("jab 3"))
            {
                an.ResetTrigger("aaa");
                an.SetBool("end jab", true);
                midJab = false;
            }
            //Up b logic
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("up b"))
            {
                //goes up can't double jumb after up b or up b again
                rb.velocity = new Vector2(rb.velocity.x, SPEED.y * 7.0f / 8.0f);
                doubleJumping = true;
                an.SetBool("freefall", true);
            }
            //neutral b ground logic
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("neutral b ground start"))
            {
                //if input in opposite direction throw behind otherwise throw forward
                if (((h < 0 && facing == 1) || (hAlt < -deadzone && facing == 1) || 
                    (h > 0 && facing == -1) || (hAlt > deadzone && facing == -1)) && isGrabbing)
                {
                    an.SetTrigger("throw b");
                    setGrabbing(false);
                }
                else if (((h < 0 && facing == -1) || (hAlt < -deadzone && facing == -1) ||
                    (h > 0 && facing == 1) || (hAlt > deadzone && facing == 1)) && isGrabbing)
                {
                    an.SetTrigger("throw f");
                    setGrabbing(false);
                }
            }
            //final smash logic
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("final smash"))
            {
                rb.velocity = Vector2.zero;
            }
        }
    }

    public void checkGrabbing()
    {
        if (!isGrabbing)
        {
            an.SetTrigger("grab miss");
        }
    }

    public void setGrabbing(bool grab)
    {
        isGrabbing = grab;
    }

    public void setThrowing()
    {
        if (isGrabbing)
        {
            isThrowing = true;
        }
    }

    public void SonicBoom()
    {
        if (!UtilStaticVars.IsReplay)
        {
            SonicAttack(1);
        }
    }

    public void SonicHurricane()
    {
        if (!UtilStaticVars.IsReplay) { 
            SonicAttack(2);
        }
    }

    private void SonicAttack(int type)
    {
        GameObject shot = null;
        switch (type)
        {
            case 1:
                shot = Instantiate(sonicBoom, new Vector3(transform.position.x + (3.83f * facing), transform.position.y + 5.0f, 0), Quaternion.identity);
                spawnedProjectile = 1;
                break;
            case 2:
                shot = Instantiate(sonicHurricane, new Vector3(transform.position.x + (3.9f * facing), transform.position.y + 5.0f, 0), Quaternion.identity);
                spawnedProjectile = 2;
                break;
            default:
                break;
        }
        shot.GetComponent<SonicBoomController>().guile = this;
        shot.GetComponent<SonicBoomController>().setFacing(facing);
        projList.Add(shot);
    }
    public override void InstantiateProjectile(int type)
    {
        if (type == 1)
        {
            SonicAttack(1);
        }
        else if (type == 2)
        {
            SonicAttack(2);
        }

    }
}
