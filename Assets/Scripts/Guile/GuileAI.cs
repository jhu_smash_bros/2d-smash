﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuileAI : GenericAI
{
    private GuileController guile;
    private LinkedList<GameObject> platforms = new LinkedList<GameObject>();
    private GameObject bottomPlat;
    private Transform farLeftLedge;
    private Transform farRightLedge;
    private SmashCharacter targetEnemy;
    private int jump = 0;
    private int targetEnemyNum = 0;
    private bool enemyOffStage = false;
    // Start is called before the first frame update
    void Start()
    {
        GameObject[] plats = GameObject.FindGameObjectsWithTag("Stage");
        foreach (GameObject p in plats)
        {
            if (p.name != "Bottom Platform")
                platforms.AddLast(p);
            else
                bottomPlat = p;
            if (farLeftLedge == null || p.transform.Find("left ledge").position.x <
                farLeftLedge.transform.position.x)
            {
                farLeftLedge = p.transform.Find("left ledge");
            }
            if (farRightLedge == null || p.transform.Find("right ledge").position.x >
                farRightLedge.transform.position.x)
            {
                farRightLedge = p.transform.Find("right ledge");
            }
        }
        guile = GetComponent<GuileController>();
    }

    public override void inputSim()
    {
        inputClear();
        //target set
        targetEnemy = enemy1;
        targetEnemyNum = 1;
        if (enemy2 != null && !enemy2.dead && enemy2.percentage > targetEnemy.percentage 
            && enemy2.stocks >= targetEnemy.stocks)
        {
            targetEnemy = enemy2;
            targetEnemyNum = 2;
        }
        if (enemy3 != null && !enemy3.dead && enemy3.percentage > targetEnemy.percentage
            && enemy3.stocks >= targetEnemy.stocks)
        {
            targetEnemy = enemy3;
            targetEnemyNum = 3;
        }

        if (guile.rb.velocity.y == 0)
        {
            jump = 0;
        }
        //jump tracker
        else if (jump == 0 && guile.rb.velocity.y != 0)
        {
            jump = 1;
        }

        //logic
        if (targetEnemy != null && !targetEnemy.dead)
        {
            Transform lLedge;
            Transform rLedge;
            float xPosDiff = targetEnemy.transform.position.x - transform.position.x;
            float yPosDiff = targetEnemy.transform.position.y - transform.position.y;

            //opponent offstage
            enemyOffStage = false;
            if (targetEnemy.transform.position.x <
                farLeftLedge.position.x &&
                targetEnemy.rb.velocity.y != 0)
            {
                if (guile.shieldHealth > 49)
                {
                    vAlt = 0;
                    hAlt = 0;
                    shieldKey = true;
                    enemyOffStage = true;
                }
                else if (yPosDiff > 0 && !guile.an.GetCurrentAnimatorStateInfo(0).IsName("side b"))
                {
                    vAlt = 0;
                    hAlt = -1;
                    specialKey = true;
                    enemyOffStage = true;
                }
                else
                {
                    vAlt = 0;
                    hAlt = 0;
                    if (changeTargetEnemy() == 0){
                        enemyOffStage = true;
                    }
                }
            }
            else if (targetEnemy.transform.position.x >
                farRightLedge.position.x &&
                targetEnemy.rb.velocity.y != 0)
            {
                if (guile.shieldHealth > 49)
                {
                    vAlt = 0;
                    hAlt = 0;
                    shieldKey = true;
                    enemyOffStage = true;
                }
                else if (yPosDiff > 0)
                {
                    vAlt = 0;
                    hAlt = 1;
                    specialKey = true;
                    enemyOffStage = true;
                }
                else
                {
                    vAlt = 0;
                    hAlt = 0;
                    if (changeTargetEnemy() == 0)
                    {
                        enemyOffStage = true;
                    }

                }
            }

            //if enemy off stage don't do any other enemy depended stuff
            if (!enemyOffStage)
            {
                //x axis movement
                if (Mathf.Abs(xPosDiff) > 6)
                {
                    hAlt = xPosDiff / Mathf.Abs(xPosDiff);
                }
                else
                {
                    hAlt = 0;
                }
                if (xPosDiff < 0 && guile.facing == 1
                    && guile.an.GetCurrentAnimatorStateInfo(0).IsName("idle"))
                {
                    h = -1;
                }

                if (xPosDiff > 0 && guile.facing == -1
                    && guile.an.GetCurrentAnimatorStateInfo(0).IsName("idle"))
                {
                    h = 1;
                }

                //y axis movement
                if (yPosDiff > .5 && guile.rb.velocity.y >= 0 && targetEnemy.rb.velocity.y < 0)
                {
                    jumpKey = true;
                }
                else if (yPosDiff < -.5 && targetEnemy.rb.velocity.y == 0 && guile.rb.velocity.y == 0)
                {
                    foreach (GameObject p in platforms)
                    {
                        lLedge = p.transform.Find("left ledge");
                        rLedge = p.transform.Find("right ledge");
                        if (p.name != "Bottom Platform" && guile.transform.position.x < rLedge.position.x
                            && guile.transform.position.x > lLedge.position.x)
                        {
                            vAlt = -1;
                        }
                    }
                }

                //on Stage attacks
                if (Mathf.Abs(xPosDiff) < 5 &&
                   Mathf.Abs(yPosDiff) < 5 &&
                   targetEnemy.percentage < 100)
                {
                    attackKey = true;
                }
                else if (((xPosDiff > 0 && xPosDiff < 5 && guile.facing == 1) ||
                    (xPosDiff < 0 && xPosDiff > -5 && guile.facing == -1)) && Mathf.Abs(yPosDiff) < 5 &&
                    targetEnemy.percentage < 100)
                {
                    vAlt = -1;
                    specialKey = true;
                }
                else if (Mathf.Abs(xPosDiff) < 24 && Mathf.Abs(yPosDiff) < 2 && targetEnemy.percentage > guile.percentage)
                {
                    if (xPosDiff < 0)
                    {
                        hAlt = -1;
                    }
                    else
                    {
                        hAlt = 1;
                    }
                    specialKey = true;
                }

                //recovery on the left side
                if ((transform.position.x <
                    farLeftLedge.position.x) || (transform.position.x <
                    bottomPlat.transform.Find("left ledge grab").position.x
                    && guile.rb.velocity.y < 0))
                {
                    if (jump == 1 && guile.rb.velocity.y < 0)
                    {
                        jumpKey = true;
                        hAlt = 1;
                        jump = 2;
                        specialHold = false;
                        specialKey = false;
                    }
                    else if (jump == 2 && guile.rb.velocity.y < 0)
                    {
                        specialHold = true;
                        specialKey = true;
                        attackKey = false;
                        vAlt = 1;
                        hAlt = 0;
                    }
                    else if (guile.rb.velocity.y != 0)
                    {
                        hAlt = 1;
                    }
                }
            }
            //recovery on the right side
            if (transform.position.x >
                farRightLedge.position.x || (transform.position.x >
                bottomPlat.transform.Find("right ledge grab").position.x
                && guile.rb.velocity.y < 0))
            {
                if (jump == 1 && guile.rb.velocity.y < 0)
                {
                    jumpKey = true;
                    hAlt = -1;
                    jump = 2;
                    specialHold = false;
                    specialKey = false;
                }
                else if (jump == 2 && guile.rb.velocity.y < 0)
                {
                    specialHold = true;
                    specialKey = true;
                    vAlt = -1;
                    hAlt = 0;
                    Debug.Log("HEY");
                }
                else if (guile.rb.velocity.y != 0)
                {
                    hAlt = -1;
                }
            }
        }
    }

    //returns num of enemy targeted unless target unchanged then reurns 0
    // returns -1 on error
    private int changeTargetEnemy()
    {
        bool e1OffStage = false;
        bool e2OffStage = false;
        bool e3OffStage = false;

        //only checks the enemies for being offstage, but only those
        //that aren't already the target
        switch (targetEnemyNum)
        {
            case 1:
                e1OffStage = true;
                if(((enemy2.transform.position.x > farRightLedge.position.x) || 
                    (enemy2.transform.position.x < farLeftLedge.position.x)) &&
                    enemy2.rb.velocity.y != 0)
                {
                    e2OffStage = true;
                }
                if (((enemy3.transform.position.x > farRightLedge.position.x) ||
                    (enemy3.transform.position.x < farLeftLedge.position.x)) &&
                    enemy3.rb.velocity.y != 0)
                {
                    e3OffStage = true;
                }
                if(e2OffStage && e3OffStage)
                {
                    return 0;
                }
                else if(!e2OffStage && e3OffStage)
                {
                    targetEnemy = enemy2;
                    targetEnemyNum = 2;
                    return 2;
                }
                else if(e2OffStage && !e3OffStage)
                {
                    targetEnemy = enemy3;
                    targetEnemyNum = 3;
                    return 3;
                }
                else
                {
                    if(enemy2.stocks == enemy3.stocks)
                    {
                        if(enemy2.percentage > enemy3.percentage)
                        {
                            targetEnemy = enemy2;
                            targetEnemyNum = 2;
                            return 2;
                        }
                        else
                        {
                            targetEnemy = enemy3;
                            targetEnemyNum = 3;
                            return 3;
                        }
                    }
                    else
                    {
                        if (enemy2.stocks > enemy3.stocks)
                        {
                            targetEnemy = enemy2;
                            targetEnemyNum = 2;
                            return 2;
                        }
                        else
                        {
                            targetEnemy = enemy3;
                            targetEnemyNum = 3;
                            return 3;
                        }
                    }
                }
            case 2:
                e2OffStage = true;
                if (((enemy1.transform.position.x > farRightLedge.position.x) ||
                    (enemy1.transform.position.x < farLeftLedge.position.x)) &&
                    enemy1.rb.velocity.y != 0)
                {
                    e1OffStage = true;
                }
                if (((enemy3.transform.position.x > farRightLedge.position.x) ||
                    (enemy3.transform.position.x < farLeftLedge.position.x)) &&
                    enemy3.rb.velocity.y != 0)
                {
                    e3OffStage = true;
                }
                if (e1OffStage && e3OffStage)
                {
                    return 0;
                }
                else if (!e1OffStage && e3OffStage)
                {
                    targetEnemy = enemy1;
                    targetEnemyNum = 1;
                    return 1;
                }
                else if (e1OffStage && !e3OffStage)
                {
                    targetEnemy = enemy3;
                    targetEnemyNum = 3;
                    return 3;
                }
                else
                {
                    if (enemy1.stocks == enemy3.stocks)
                    {
                        if (enemy1.percentage > enemy3.percentage)
                        {
                            targetEnemy = enemy1;
                            targetEnemyNum = 1;
                            return 1;
                        }
                        else
                        {
                            targetEnemy = enemy3;
                            targetEnemyNum = 3;
                            return 3;
                        }
                    }
                    else
                    {
                        if (enemy1.stocks > enemy3.stocks)
                        {
                            targetEnemy = enemy1;
                            targetEnemyNum = 1;
                            return 1;
                        }
                        else
                        {
                            targetEnemy = enemy3;
                            targetEnemyNum = 3;
                            return 3;
                        }
                    }
                }
            case 3:
                e3OffStage = true;
                if (((enemy1.transform.position.x > farRightLedge.position.x) ||
                    (enemy1.transform.position.x < farLeftLedge.position.x)) &&
                    enemy1.rb.velocity.y != 0)
                {
                    e1OffStage = true;
                }
                if (((enemy2.transform.position.x > farRightLedge.position.x) ||
                    (enemy2.transform.position.x < farLeftLedge.position.x)) &&
                    enemy2.rb.velocity.y != 0)
                {
                    e2OffStage = true;
                }
                if (e1OffStage && e2OffStage)
                {
                    return 0;
                }
                else if (!e1OffStage && e2OffStage)
                {
                    targetEnemy = enemy1;
                    targetEnemyNum = 1;
                    return 1;
                }
                else if (e1OffStage && !e2OffStage)
                {
                    targetEnemy = enemy2;
                    targetEnemyNum = 2;
                    return 2;
                }
                else
                {
                    if (enemy1.stocks == enemy2.stocks)
                    {
                        if (enemy1.percentage > enemy2.percentage)
                        {
                            targetEnemy = enemy1;
                            targetEnemyNum = 1;
                            return 1;
                        }
                        else
                        {
                            targetEnemy = enemy2;
                            targetEnemyNum = 2;
                            return 2;
                        }
                    }
                    else
                    {
                        if (enemy1.stocks > enemy2.stocks)
                        {
                            targetEnemy = enemy1;
                            targetEnemyNum = 1;
                            return 1;
                        }
                        else
                        {
                            targetEnemy = enemy2;
                            targetEnemyNum = 2;
                            return 2;
                        }
                    }
                }
            default:
                break;
        }
        return -1;

    }
}
