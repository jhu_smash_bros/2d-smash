﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO; 

public class Frame : MonoBehaviour
{
    public Inputs p1 = new Inputs();
    public Inputs p2 = new Inputs();
    public Inputs p3 = new Inputs();
    public Inputs p4 = new Inputs();
    //public string path = "Assets/Resources/Replays/test.txt";

    public void Print(string path)
    {
        //append frame to the end of file
        //each player data is on a line
        StreamWriter writer = new StreamWriter(path, true);
        writer.Write(p1.pos.x + " " + p1.pos.y + " " + p1.pos.z + " ");
        writer.Write(p1.flipped + " ");
        writer.Write(p1.inv + " ");
        writer.Write(p1.percent + " ");
        writer.Write(p1.stocks + " ");
        writer.Write(p1.fsMeter + " ");
        writer.Write(p1.animation + " / ");
        writer.Write(p1.spawnProjectile + " ");
        for (int i = 0; i < p1.destroyedProj.Count; i++)
        {
            writer.Write(p1.destroyedProj[i] + " ");
        }
        writer.Write("/ ");
        for (int i = 0; i < p1.projPos.Count; i++)
        {
            writer.Write(p1.projPos[i].x + " " + p1.projPos[i].y + " " + p1.projPos[i].z + " ");
        }
        //signifies end of that player
        writer.Write("END\n");

        writer.Write(p2.pos.x + " " + p2.pos.y + " " + p2.pos.z + " ");
        writer.Write(p2.flipped + " ");
        writer.Write(p2.inv + " ");
        writer.Write(p2.percent + " ");
        writer.Write(p2.stocks + " ");
        writer.Write(p2.fsMeter + " ");
        writer.Write(p2.animation + " / ");
        writer.Write(p2.spawnProjectile + " ");
        for (int i = 0; i < p2.destroyedProj.Count; i++)
        {
            writer.Write(p2.destroyedProj[i] + " ");
        }
        writer.Write("/ ");
        for (int i = 0; i < p2.projPos.Count; i++)
        {
            writer.Write(p2.projPos[i].x + " " + p2.projPos[i].y + " " + p2.projPos[i].z + " ");
        }
        //signifies end of that player
        writer.Write("END\n");

        writer.Write(p3.pos.x + " " + p3.pos.y + " " + p3.pos.z + " ");
        writer.Write(p3.flipped + " ");
        writer.Write(p3.inv + " ");
        writer.Write(p3.percent + " ");
        writer.Write(p3.stocks + " ");
        writer.Write(p3.fsMeter + " ");
        writer.Write(p3.animation + " / ");
        writer.Write(p3.spawnProjectile + " ");
        for (int i = 0; i < p3.destroyedProj.Count; i++)
        {
            writer.Write(p3.destroyedProj[i] + " ");
        }
        writer.Write("/ ");
        for (int i = 0; i < p3.projPos.Count; i++)
        {
            writer.Write(p3.projPos[i].x + " " + p3.projPos[i].y + " " + p3.projPos[i].z + " ");
        }
        //signifies end of that player
        writer.Write("END\n");

        writer.Write(p4.pos.x + " " + p4.pos.y + " " + p4.pos.z + " ");
        writer.Write(p4.flipped + " ");
        writer.Write(p4.inv + " ");
        writer.Write(p4.percent + " ");
        writer.Write(p4.stocks + " ");
        writer.Write(p4.fsMeter + " ");
        writer.Write(p4.animation + " / ");
        writer.Write(p4.spawnProjectile + " ");
        for (int i = 0; i < p4.destroyedProj.Count; i++)
        {
            writer.Write(p4.destroyedProj[i] + " ");
        }
        writer.Write("/ ");
        for (int i = 0; i < p4.projPos.Count; i++)
        {
            writer.Write(p4.projPos[i].x + " " + p4.projPos[i].y + " " + p4.projPos[i].z + " ");
        }
        //signifies end of that player
        writer.Write("END\n");
        //close stream
        writer.Close();
    }

}
//struct for the inputs
public struct Inputs
{
    public Vector3 pos;
    public bool flipped;
    public bool inv;
    public float percent;
    public int stocks;
    public int fsMeter;
    public string animation;
    public int spawnProjectile;
    public List<int> destroyedProj;
    public List<Vector3> projPos;
}
