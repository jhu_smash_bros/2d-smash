﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class ReplayData : MonoBehaviour
{
    private List<Frame> frames = new List<Frame>();
    private string p1Name;
    private string p2Name;
    private string p3Name;
    private string p4Name;
    private string stage;
    private int stocks;
    public string path = "Assets/Resources/Replays/Replay0.txt";

    private void Start()
    {
        string fightersNames = "";
        int replayInt = 1;
        p1Name = (UtilStaticVars.Player1 == "")? "none" : UtilStaticVars.Player1;
        if(p1Name != "none") {
            fightersNames = p1Name;
        }
        p2Name = (UtilStaticVars.Player2 == "") ? "none" : UtilStaticVars.Player2;
        if (p2Name != "none") {
            if (fightersNames != "")
            {
                fightersNames = fightersNames + "_vs_" + p2Name;
            }
            else
            {
                fightersNames = fightersNames + p2Name;
            }
        }
        p3Name = (UtilStaticVars.Player3 == "") ? "none" : UtilStaticVars.Player3;
        if (p3Name != "none")
        {
            if (fightersNames != "")
            {
                fightersNames = fightersNames + "_vs_" + p3Name;
            }
            else
            {
                fightersNames = fightersNames + p3Name;
            }
        }
        p4Name = (UtilStaticVars.Player4 == "") ? "none" : UtilStaticVars.Player4;
        if (p4Name != "none")
        {
            if (fightersNames != "")
            {
                fightersNames = fightersNames + "_vs_" + p4Name;
            }
            else
            {
                fightersNames = fightersNames + p4Name;
            }
        }
        stage = UtilStaticVars.Stage;
        stocks = UtilStaticVars.Stocks;

        path = "Assets/Resources/Replays/" + fightersNames + ".txt";
        //if the replay name exists change name;
        while (File.Exists(path))
        {
            path = "Assets/Resources/Replays/" + fightersNames + replayInt.ToString() + ".txt";
            replayInt++;
        }
    }

    public bool addFrame(int frameNum, SmashCharacter smasher)
    {
        //if frames are too far ahead don't add other wise edit frame or add a new one.
        if(frameNum > frames.Count)
        {
            return false;
        }
        else if(frameNum < frames.Count)
        {
            smasher.addInputToFrameRecording(frames[frameNum]);
        }
        else if(frameNum == frames.Count)
        {
            Frame f = new Frame();
            f.p1.destroyedProj = new List<int>();
            f.p2.destroyedProj = new List<int>();
            f.p3.destroyedProj = new List<int>();
            f.p4.destroyedProj = new List<int>();
            f.p1.projPos = new List<Vector3>();
            f.p2.projPos = new List<Vector3>();
            f.p3.projPos = new List<Vector3>();
            f.p4.projPos = new List<Vector3>();
            smasher.addInputToFrameRecording(f);
            frames.Add(f);
        }
        return true;
    }

    public void Print()
    {
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine(stocks);
        writer.WriteLine(stage);
        writer.WriteLine(p1Name);
        writer.WriteLine(p2Name);
        writer.WriteLine(p3Name);
        writer.WriteLine(p4Name);
        writer.WriteLine("\n");
        writer.Close();
        for(int i = 0; i < frames.Count; i++)
        {
            frames[i].Print(path);
        }
        //import to editor
        AssetDatabase.ImportAsset(path);
    }

    public void Read(string fp)
    {
        // clear frames just in case
        frames.Clear();
        using (StreamReader reader = new StreamReader(fp))
        {
            char[] delimiter = { ' ' };
            string line;
            stocks = int.Parse(reader.ReadLine());
            stage = reader.ReadLine();
            p1Name = reader.ReadLine();
            p2Name = reader.ReadLine();
            p3Name = reader.ReadLine();
            p4Name = reader.ReadLine();
            //empty lines 
            reader.ReadLine();
            reader.ReadLine();

            //read frames
            while ((line = reader.ReadLine()) != null)
            {
                Frame f = new Frame();
                string[] values = line.Split(delimiter, System.StringSplitOptions.RemoveEmptyEntries);
                Vector3 pos1 = new Vector3();
                pos1.x = float.Parse(values[0]);
                pos1.y = float.Parse(values[1]);
                pos1.z = float.Parse(values[2]);
                f.p1.pos = pos1;
                f.p1.flipped = bool.Parse(values[3]);
                f.p1.inv = bool.Parse(values[4]);
                f.p1.percent = float.Parse(values[5]);
                f.p1.stocks = int.Parse(values[6]);
                f.p1.fsMeter = int.Parse(values[7]);
                f.p1.animation = (values[8] == "/") ? "" : values[8];
                //for animation names that have spaces in them
                int i = (f.p1.animation == "")? 8 : 9;
                while(values[i] != "/")
                {
                    f.p1.animation = f.p1.animation + " " + values[i];
                    i++;
                }
                i++;
                if (f.p1.animation.Contains("."))
                {
                    f.p1.animation = f.p1.animation.Replace(".", "_");
                }
                f.p1.spawnProjectile = int.Parse(values[i]);
                i++;
                f.p1.destroyedProj = new List<int>();
                //get list entries
                while (values[i] != "/")
                {
                    f.p1.destroyedProj.Add(int.Parse(values[i]));
                    i++;
                }
                i++;
                f.p1.projPos = new List<Vector3>();
                while (!values[i].Contains("END"))
                {
                    Vector3 projPos = new Vector3();
                    projPos.x = float.Parse(values[i]);
                    projPos.y = float.Parse(values[i + 1]);
                    projPos.z = float.Parse(values[i + 2]);
                    f.p1.projPos.Add(projPos);
                    i += 3;
                }

                //end of p1 data
                line = reader.ReadLine();
                values = line.Split(delimiter, System.StringSplitOptions.RemoveEmptyEntries);
                Vector3 pos2 = new Vector3();
                pos2.x = float.Parse(values[0]);
                pos2.y = float.Parse(values[1]);
                pos2.z = float.Parse(values[2]);
                f.p2.pos = pos2;
                f.p2.flipped = bool.Parse(values[3]);
                f.p2.inv = bool.Parse(values[4]);
                f.p2.percent = float.Parse(values[5]);
                f.p2.stocks = int.Parse(values[6]);
                f.p2.fsMeter = int.Parse(values[7]);
                f.p2.animation = (values[8] == "/") ? "" : values[8];
                //for animation names that have spaces in them
                i = (f.p2.animation == "") ? 8 : 9;
                while (values[i] != "/")
                {
                    f.p2.animation = f.p2.animation + " " + values[i];
                    i++;
                }
                i++;
                if(f.p2.animation.Contains("."))
                {
                    f.p2.animation = f.p2.animation.Replace(".", "_");
                }
                f.p2.spawnProjectile = int.Parse(values[i]);
                i++;
                f.p2.destroyedProj = new List<int>();
                //get list entries
                while (values[i] != "/")
                {
                    f.p2.destroyedProj.Add(int.Parse(values[i]));
                    i++;
                }
                i++;
                f.p2.projPos = new List<Vector3>();
                while (!values[i].Contains("END"))
                {
                    Vector3 projPos = new Vector3();
                    projPos.x = float.Parse(values[i]);
                    projPos.y = float.Parse(values[i + 1]);
                    projPos.z = float.Parse(values[i + 2]);
                    f.p2.projPos.Add(projPos);
                    i += 3;
                }

                //end of p2 data
                line = reader.ReadLine();
                values = line.Split(delimiter, System.StringSplitOptions.RemoveEmptyEntries);
                Vector3 pos3 = new Vector3();
                pos3.x = float.Parse(values[0]);
                pos3.y = float.Parse(values[1]);
                pos3.z = float.Parse(values[2]);
                f.p3.pos = pos3;
                f.p3.flipped = bool.Parse(values[3]);
                f.p3.inv = bool.Parse(values[4]);
                f.p3.percent = float.Parse(values[5]);
                f.p3.stocks = int.Parse(values[6]);
                f.p3.fsMeter = int.Parse(values[7]);
                f.p3.animation = (values[8] == "/") ? "" : values[8];
                //for animation names that have spaces in them
                i = (f.p3.animation == "") ? 8 : 9;
                while (values[i] != "/")
                {
                    f.p3.animation = f.p3.animation + " " + values[i];
                    i++;
                }
                i++;
                if (f.p3.animation.Contains("."))
                {
                    f.p3.animation = f.p3.animation.Replace(".", "_");
                }
                f.p3.spawnProjectile = int.Parse(values[i]);
                i++;
                f.p3.destroyedProj = new List<int>();
                //get list entries
                while (values[i] != "/")
                {
                    f.p3.destroyedProj.Add(int.Parse(values[i]));
                    i++;
                }
                i++;
                f.p3.projPos = new List<Vector3>();
                while (!values[i].Contains("END"))
                {
                    Vector3 projPos = new Vector3();
                    projPos.x = float.Parse(values[i]);
                    projPos.y = float.Parse(values[i + 1]);
                    projPos.z = float.Parse(values[i + 2]);
                    f.p3.projPos.Add(projPos);
                    i += 3;
                }

                //end of p3 data
                line = reader.ReadLine();
                values = line.Split(delimiter, System.StringSplitOptions.RemoveEmptyEntries);
                Vector3 pos4 = new Vector3();
                pos4.x = float.Parse(values[0]);
                pos4.y = float.Parse(values[1]);
                pos4.z = float.Parse(values[2]);
                f.p4.pos = pos4;
                f.p4.flipped = bool.Parse(values[3]);
                f.p4.inv = bool.Parse(values[4]);
                f.p4.percent = float.Parse(values[5]);
                f.p4.stocks = int.Parse(values[6]);
                f.p4.fsMeter = int.Parse(values[7]);
                f.p4.animation = (values[8] == "/") ? "" : values[8];
                //for animation names that have spaces in them
                i = (f.p4.animation == "") ? 8 : 9;
                while (values[i] != "/")
                {
                    f.p4.animation = f.p4.animation + " " + values[i];
                    i++;
                }
                i++;
                if (f.p4.animation.Contains("."))
                {
                    f.p4.animation = f.p4.animation.Replace(".", "_");
                }
                f.p4.spawnProjectile = int.Parse(values[i]);
                i++;
                f.p4.destroyedProj = new List<int>();
                //get list entries
                while (values[i] != "/")
                {
                    f.p4.destroyedProj.Add(int.Parse(values[i]));
                    i++;
                }
                i++;
                f.p4.projPos = new List<Vector3>();
                while (!values[i].Contains("END"))
                {
                    Vector3 projPos = new Vector3();
                    projPos.x = float.Parse(values[i]);
                    projPos.y = float.Parse(values[i + 1]);
                    projPos.z = float.Parse(values[i + 2]);
                    f.p4.projPos.Add(projPos);
                    i += 3;
                }

                //end of p4 data
                frames.Add(f);
            }
        }
    }

    public void addToUtilStaticVars()
    {
        UtilStaticVars.IsReplay = true;
        UtilStaticVars.Stage = stage;
        UtilStaticVars.Stocks = stocks;
        UtilStaticVars.Player1 = (p1Name == "none") ? "" : p1Name;
        UtilStaticVars.Player2 = (p2Name == "none") ? "" : p2Name;
        UtilStaticVars.Player3 = (p3Name == "none") ? "" : p3Name;
        UtilStaticVars.Player4 = (p4Name == "none") ? "" : p4Name;
        UtilStaticVars.Replay = this;
        if(stage == "Island")
        {
            UtilStaticVars.Theme = "IslandTheme";
        }
        else if (stage == "RocketZone")
        {
            UtilStaticVars.Theme = "RocketTheme";
        }
    }

    public Frame getReplayInput(int frame)
    {
        try
        {
            Frame ret = frames[frame];
            return ret;
        }
        catch 
        {

            return new Frame();
        }
    }
}
