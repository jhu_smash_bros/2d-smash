﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class KidGokuHitboxController : MonoBehaviour
{
    public DamageValues dv;
    public SmashCharacter goku;

    private Animator an;
    private Collider2D cl;
    private Rigidbody2D rb;
    private Vector2 storedVel = Vector2.zero;

    void Start()
    {
        if (gameObject.tag == "Kamehameha")
        {
            an = gameObject.GetComponent<Animator>();
            rb = gameObject.GetComponent<Rigidbody2D>();
        }
    }

    void OnDisable()
    {
        goku.hitting = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // damage character on hit
        if (other.transform.parent.name != goku.name
            && (other.tag == "Character" || other.tag == "Shield"))
        {
            cl = other;
            StartCoroutine(goku.Damage(cl.attachedRigidbody.name,
                                       dv.percentDamage, dv.knockback,
                                       dv.hitStun, dv.follow,
                                       dv.followDis, dv.spike));
        }
    }

    void FixedUpdate()
    {
        // don't fixed update on replay
        if (UtilStaticVars.IsReplay) return;

        // don't animate/move when paused
        if (UtilStaticVars.Pause)
        {
            if (gameObject.tag == "Kamehameha")
            {
                an.enabled = false;
                if (storedVel == Vector2.zero)
                {
                    storedVel = rb.velocity;
                    rb.velocity = Vector2.zero;
                }
            }
            return;
        }

        // account for projectile movement
        if (gameObject.tag == "Kamehameha")
        {
            if (!an.enabled) an.enabled = true;
            
            if (storedVel != Vector2.zero)
            {
                rb.velocity = storedVel;
                storedVel = Vector2.zero;
            }

            SetVelY(goku.rb.velocity.y);
        }

        // account for longer attack
        if (goku.hitting && cl != null)
        {
            if (gameObject.name == "Blast1Hitbox1"
                || gameObject.name == "Blast2Hitbox1")
            {
                StartCoroutine(goku.Damage(cl.attachedRigidbody.name,
                                           dv.percentDamage,
                                           dv.knockback,
                                           dv.hitStun)
                );
                cl.GetComponentInParent<Rigidbody2D>().velocity =
                    gameObject.GetComponentInParent<Rigidbody2D>().velocity;
            }
        }
    }

    /***********
    SET VELOCITY
    ************/
    public void SetVelY(float y)
    {
        rb.velocity = new Vector2(rb.velocity.x, y);
    }
    public void SetVelX(float x)
    {
        rb.velocity = new Vector2(goku.facing * x,
                                  rb.velocity.y);
    }

    /*********
    KAMEHAMEHA
    **********/
    public void ReleaseB()
    {
        an.SetTrigger("b released");
        transform.position =
            new Vector3(transform.position.x + goku.facing * 5f,
                        transform.position.y + 1f, 0f);
    }
    public void UpdateBlastStrength(float blastStrength)
    {
        an.SetFloat("blast strength", blastStrength);
    }
    public void ReverseImage()
	{
        rb = gameObject.GetComponent<Rigidbody2D>();
		rb.transform.localScale =
            new Vector2(-1 * rb.transform.localScale.x,
                        rb.transform.localScale.y);
	}
    public void TurnOff()
    {
        if (UtilStaticVars.IsReplay) return;

        goku.RemoveProjectile(this.gameObject);
        Destroy(this.gameObject);
    }
}
