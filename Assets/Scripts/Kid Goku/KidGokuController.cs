﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KidGokuController : SmashCharacter
{
    public GameObject nimbus;
    public GameObject kamehamehaPrefab;

    private bool startSideB = false;
    private float blastStrength = 0f;
    private KidGokuHitboxController kamehameha;

	public override void FixedUpdate()
    {
        // don't run fixed update stuff during replay
        if (UtilStaticVars.IsReplay) return;

        // reset, movement, jumping/falling, crouching
        if (active)
        {
            base.FixedUpdate();
            if (isPaused) return;
        }

        /***
        IDLE
        ****/
        if (an.GetCurrentAnimatorStateInfo(0).IsName("idle"))
        {
            startSideB = false;
        }
        /*****
        SIDE B
        ******/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("side b"))
        {
            if (!startSideB)
            {
                transform.position =
                    new Vector3(transform.position.x,
                                transform.position.y + 2f, 0f);
                startSideB = true;
            }
        }
        /********
        NEUTRAL B
        *********/
        else if (an.GetCurrentAnimatorStateInfo(0).IsTag("goku neutral b"))
        {
            rb.gravityScale = 0;
            if (falling) {
                rb.velocity = new Vector2(0f, -3f);
            }

            if (an.GetCurrentAnimatorStateInfo(0).IsName("neutral b charge 1"))
            {
                if (specialHold && blastStrength <= 1)
                {
                    blastStrength += Time.deltaTime/4;
                    kamehameha.UpdateBlastStrength(blastStrength);
                }
                else
                {
                    if (blastStrength == 0) blastStrength = 0.1f;
                    an.SetTrigger("b released");
                }
            }
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("neutral b shoot 1"))
            {
                if (blastStrength > 0)
                {
                    if (blastStrength <= 0.3f) Play("GokuBlast0");
                    else Play("GokuBlast1");
                    kamehameha.ReleaseB();
                    blastStrength = 0f;
                }
            }
        }
        /**********
        DASH ATTACK
        ***********/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("dash attack"))
        {
            // change the animation based on speed
            an.SetFloat("speed", Mathf.Abs(rb.velocity.x));
        }
        /**********
        FINAL SMASH
        ***********/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("fs"))
        {
            // nothing here yet
        }
	}

    /*****
    NIMBUS
    ******/
    public void ActivateNimbus(int setting)
    {
        if (setting > 0)
            nimbus.SetActive(true);
        else
            nimbus.SetActive(false);
    }

    /*********
    KAMEHAMEHA
    **********/
    public void ActivateBlast()
    {
        GameObject blast;
        blast = Instantiate(kamehamehaPrefab,
                            new Vector3(transform.position.x - facing * 1f,
                                        transform.position.y + 2f, 0f),
                                        Quaternion.identity);

        kamehameha = blast.GetComponent<KidGokuHitboxController>();
        kamehameha.goku = this;
        kamehameha.transform.Find("Blast0Hitbox").GetComponent<KidGokuHitboxController>().goku = this;
        kamehameha.transform.Find("Blast1Hitbox1").GetComponent<KidGokuHitboxController>().goku = this;
        kamehameha.transform.Find("Blast1Hitbox2").GetComponent<KidGokuHitboxController>().goku = this;
        kamehameha.transform.Find("Blast2Hitbox1").GetComponent<KidGokuHitboxController>().goku = this;
        kamehameha.transform.Find("Blast2Hitbox2").GetComponent<KidGokuHitboxController>().goku = this;
        if (facing == -1) kamehameha.ReverseImage();
    }

    /***********
    SET VELOCITY
    ************/
    public void SetVelY(float y)
    {
        rb.velocity = new Vector2(rb.velocity.x, y);
    }
    public void SetVelX(float x)
    {
        rb.velocity = new Vector2(facing * x, rb.velocity.y);
    }
}
