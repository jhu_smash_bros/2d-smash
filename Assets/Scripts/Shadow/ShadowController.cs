﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowController : SmashCharacter
{
    public GameObject aura;
    public GameObject sparkPrefab;
    public GameObject fballPrefab;

    public override void Update()
    {
        base.Update();
        if (UtilStaticVars.IsReplay)
        {
            if (an.GetCurrentAnimatorStateInfo(0).IsName("fs 2"))
            {
                aura.gameObject.SetActive(true);
                FballController fb = projList[0].GetComponent<FballController>();
                if(fb != null) {
                    fb.an.SetBool("hit", true);
                }
            }
            else
            {
                aura.gameObject.SetActive(false);
            }
        }
    }

    public override void FixedUpdate()
    {
        // don't run fixed update stuff during replay
        if (UtilStaticVars.IsReplay) return;
        
        // reset, movement, jumping/falling, crouching
        if (active)
        {
            base.FixedUpdate();
            if (isPaused) return;
        }

        /*****
        PUMMEL
        ******/
        if (an.GetCurrentAnimatorStateInfo(0).IsName("pummel 1")
            || an.GetCurrentAnimatorStateInfo(0).IsName("pummel 2")
            || an.GetCurrentAnimatorStateInfo(0).IsName("pummel 3"))
        {
            if (attackKey)
                an.SetTrigger("a");
        }
        /*****
        DOWN B
        ******/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("down b"))
        {
            // as long as b is held, you heal
            if (specialHold)
                an.SetBool("healing", true);
            else
                an.SetBool("healing", false);
            
            // stop healing once your percentage is 0%
            if (percentage > 0f)
            {
                percentage -= 0.1f;
                percentageGoal = percentage;
            }
            else
            {
                percentage = 0;
                an.SetBool("healing", false);
            }
        }
        /**********
        FINAL SMASH
        ***********/
        else if (an.GetCurrentAnimatorStateInfo(0).IsName("fs 2"))
        {
            rb.gravityScale = 0f;
            aura.gameObject.SetActive(true);
            rb.velocity = new Vector2(0f, 0f);
            transform.position = Vector3.MoveTowards(transform.position,
                                                     new Vector3(0f, 5f, 0f), 0.2f);
        }
	}

    /**********
    PROJECTILES
    ***********/
    public void ActivateSpark()
    {
        if (!UtilStaticVars.IsReplay)
        {
            GameObject spark =
                Instantiate(sparkPrefab,
                            new Vector3(transform.position.x + facing * 5f,
                                        transform.position.y + 3.5f, 0f),
                            Quaternion.identity);
            spark.GetComponent<ShadowHitboxController>().shadow = this;
            projList.Add(spark);
            spawnedProjectile = 1;
        }
    }
    public void ActivateFball()
    {
        if (!UtilStaticVars.IsReplay)
        {
            GameObject fball =
                Instantiate(fballPrefab,
                            new Vector3(transform.position.x + facing * 5.5f,
                                        transform.position.y + 2.7f, 0f),
                            Quaternion.identity);
            fball.GetComponent<FballController>().shadow = this;
            projList.Add(fball);
            spawnedProjectile = 2;
        }
    }
    public override void InstantiateProjectile(int type)
    {
        if (type == 1)
        {
            GameObject spark =
                Instantiate(sparkPrefab,
                            new Vector3(transform.position.x + facing * 5f,
                                        transform.position.y + 3.5f, 0f),
                            Quaternion.identity);
            spark.GetComponent<ShadowHitboxController>().shadow = this;
            projList.Add(spark);
        }
        if (type == 2)
        {
            GameObject fball =
                Instantiate(fballPrefab,
                            new Vector3(transform.position.x + facing * 5.5f,
                                        transform.position.y + 2.7f, 0f),
                            Quaternion.identity);
            fball.GetComponent<FballController>().shadow = this;
            projList.Add(fball);
        }
    }

    /***********
    SET VELOCITY
    ************/
    public void SetVelY(float y)
    {
        rb.velocity = new Vector2(rb.velocity.x, y);
    }
    public void SetVelX(float x)
    {
        rb.velocity = new Vector2(facing * x, rb.velocity.y);
    }
}
