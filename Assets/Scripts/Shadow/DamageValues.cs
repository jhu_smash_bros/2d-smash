﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DamageValues
{
    public float percentDamage;    // percent damage
    public Vector2 knockback;      // knockback
    public bool follow;            // does enemy follow you
    public float followDis;        // how far does enemy follow
    public bool spike;             // does attack spike
    public float shieldStun;       // shield stun
    public int shieldDamage;       // shield damage
    public int hitStun;            // hit stun in frames
}
