﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowAI : GenericAI
{
    private ShadowController shadow;
    private LinkedList<GameObject> platforms = new LinkedList<GameObject>();
    private Transform farLeftLedge;
    private Transform farRightLedge;
    private SmashCharacter targetEnemy;
    private int jump = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameObject[] plats = GameObject.FindGameObjectsWithTag("Stage");
        foreach (GameObject p in plats)
        {
            platforms.AddLast(p);
            if (farLeftLedge == null || p.transform.Find("left ledge").position.x <
                farLeftLedge.transform.position.x)
            {
                farLeftLedge = p.transform.Find("left ledge");
            }
            if (farRightLedge == null || p.transform.Find("right ledge").position.x >
                farRightLedge.transform.position.x)
            {
                farRightLedge = p.transform.Find("right ledge");
            }
        }
        shadow = GetComponent<ShadowController>();
    }

    public override void inputSim()
    {
        bool offStage = isOffStage();

        inputClear();
        //target set
        targetEnemy = enemy1;
        if (enemy2 != null && !enemy2.dead && enemy2.stocks > targetEnemy.stocks)
        {
            targetEnemy = enemy2;
        }
        if (enemy3 != null && !enemy3.dead && enemy3.stocks > targetEnemy.stocks)
        {
            targetEnemy = enemy3;
        }

        //check jumps
        if (shadow.rb.velocity.y == 0)
        {
            jump = 0;
        }
        else if (jump == 0 && shadow.rb.velocity.y != 0)
        {
            jump = 1;
        }

        if (targetEnemy != null && !targetEnemy.dead)
        {
            float xPosDiff = targetEnemy.transform.position.x - transform.position.x;
            float yPosDiff = targetEnemy.transform.position.y - transform.position.y;
            //x axis movement
            if (Mathf.Abs(xPosDiff) > 6)
            {
                hAlt = xPosDiff / Mathf.Abs(xPosDiff);
            }
            else
            {
                hAlt = 0;
            }
            //y axis movement
            if (hAlt == 0)
            {
                if (yPosDiff > .5 && shadow.rb.velocity.y <= 0 && targetEnemy.rb.velocity.y < 0)
                {
                    jumpKey = true;
                    if(jump < 2)
                    {
                        jump++;
                    }
                }
                else if (yPosDiff < -.5)
                {

                    vAlt = -1;

                }
            }

            //opponent offstage then heal
            if (targetEnemy.transform.position.x <
                farLeftLedge.position.x &&
                targetEnemy.rb.velocity.y != 0)
            {
                vAlt = -1;
                hAlt = 0;
                specialKey = true;
            }
            else if (targetEnemy.transform.position.x >
                farRightLedge.position.x &&
                targetEnemy.rb.velocity.y != 0)
            {
                vAlt = -1;
                hAlt = 0;
                specialKey = true;
            }

            //onstage attacks
            if(Mathf.Abs(xPosDiff+yPosDiff) < 6 && !((xPosDiff < 0 && shadow.facing == 1) || (xPosDiff > 0 && shadow.facing == -1)))
            {
                attackKey = true;
            }
            else if(Mathf.Abs(xPosDiff) > 10 && Mathf.Abs(xPosDiff) < 25 && Mathf.Abs(yPosDiff) < 1)
            {
                vAlt = 0;
                specialKey = true;
                if(xPosDiff > 0)
                {
                    hAlt = 1;
                }
                else
                {
                    hAlt = -1;
                }
            }
            else if(Mathf.Abs(xPosDiff + yPosDiff) < 6 && !((xPosDiff > 0 && shadow.facing == 1) || (xPosDiff < 0 && shadow.facing == -1)))
            {
                vAlt = 0;
                hAlt = 0;
                specialKey = true;
            }

            //recovery
            if (offStage)
            {
                if(shadow.rb.velocity.y <= 0)
                {
                    if(jump < 2)
                    {
                        jumpKey = true;
                        attackKey = false;
                        specialKey = false;
                        //jump++;
                    }
                    else if (!shadow.an.GetCurrentAnimatorStateInfo(0).IsName("up b") || !shadow.an.GetCurrentAnimatorStateInfo(0).IsName("jump 1"))
                    {
                        vAlt = 1;
                        hAlt = 0;
                        specialKey = true;
                        jumpKey = false;
                        attackKey = false;
                    }
                    else
                    {
                        if(transform.position.x < farLeftLedge.position.x)
                        {
                            hAlt = 1;
                        }
                        else
                        {
                            hAlt = -1;
                        }
                    }
                }
                else
                {
                    if (transform.position.x < farLeftLedge.position.x)
                    {
                        hAlt = 1;
                    }
                    else
                    {
                        hAlt = -1;
                    }
                }
            }
        }
    }

    private bool isOffStage()
    {
        //return true if further left or right than any other platform 
        if(transform.position.x < farLeftLedge.position.x 
            || transform.position.x > farRightLedge.position.x)
        {
            return true;
        }

        foreach (GameObject p in platforms)
        {

            //return false if platform is under character
            if(p.transform.position.y < transform.position.y)
            {
                if(p.transform.Find("left ledge").position.x < transform.position.x
                    && p.transform.Find("right ledge").position.x > transform.position.x){
                    return false;
                }
            }
        }
        //return true if that fails
        return true;
    }
}
