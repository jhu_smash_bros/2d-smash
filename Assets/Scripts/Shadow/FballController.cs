﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FballController : MonoBehaviour
{
    public Vector3[] locations;
    public SmashCharacter shadow;

    [HideInInspector]
	public Animator an;
    private Rigidbody2D rb;
    private SmashCharacter target;

    private int locIndex = 0;
    private float timeLimit = 0f;

    void Start()
    {
        locIndex = 0;
        timeLimit = 0f;
		an = GetComponent<Animator>();
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    private void TurnOff()
    {
        shadow.rb.gravityScale = 1f;
        shadow.an.SetBool("final smash", false);
        shadow.an.SetBool("final smash 2", false);
        shadow.GetComponent<ShadowController>().aura.SetActive(false);

        if (target != null)
            target.an.SetBool("attacked", false);

        shadow.RemoveProjectile(this.gameObject);
        Destroy(this.gameObject);
    }

    public void SetVelX(float x)
    {
        rb.velocity = new Vector2(shadow.facing * x, rb.velocity.y);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        //damage character on hit
        if ((other.tag == "Character" || other.tag == "Shield") && other.transform.parent.name != shadow.name &&
            !other.gameObject.GetComponentInParent<SmashCharacter>().dodging)
        {
            // start next stage if the first fball hits
            if (an.GetCurrentAnimatorStateInfo(0).IsName("stage 1"))
            {
                an.SetBool("hit", true);
                shadow.an.SetBool("final smash 2", true);

                rb.velocity = new Vector2(0f, 0f);
                target = other.attachedRigidbody.GetComponent<SmashCharacter>();
                StartCoroutine(shadow.Damage(target.name, 0f, new Vector2(0f, 0f), 0));
            }
            // do damage while the ball is moveing back and forth
            else if (an.GetCurrentAnimatorStateInfo(0).IsName("stage 2"))
            {
                if (locIndex > 0)
                {
                    if (locIndex != locations.Length)
                    {
                        shadow.Play("ShadowFballHit");
                        StartCoroutine(shadow.Damage(target.name, 14f, new Vector2(0f, 0f), 0));
                    }
                    else
                    {
                        shadow.Play("ShadowFballHitFinal");
                        StartCoroutine(shadow.Damage(target.name, 14f, new Vector2(shadow.facing * 40f, 100f), 5));
                    }
                }
            }
        }
    }

    void FixedUpdate()
    {
        if (!UtilStaticVars.Pause && !UtilStaticVars.IsReplay)
        {
            // if the first fball hit
            if (an.GetBool("hit"))
            {
                // moving to the center
                if (locIndex == 0)
                {
                    target.an.SetBool("attacked", true);

                    target.rb.gravityScale = 0f;
                    target.rb.velocity = new Vector2(0f, 0f);

                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(0f, 0f, 0f), 0.2f);
                    target.transform.position = new Vector2(transform.position.x, transform.position.y - 3.5f);

                    // once you reach the center
                    if (transform.position == new Vector3(0f, 0f, 0f))
                    {
                        locIndex += 1;
                        shadow.Play("ShadowFS2");
                    }
                }
                // spirit ball attack!
                if (locIndex > 0 && shadow.transform.position == new Vector3(0f, 5f, 0f))
                {
                    if (locIndex < locations.Length)
                    {
                        target.rb.velocity = new Vector2(0f, 0f);
                        target.transform.position = new Vector2(0f, -3.5f);
                    }

                    // move to designated locations
                    if (transform.position != locations[locIndex - 1])
                        transform.position = Vector3.MoveTowards(transform.position, locations[locIndex - 1], 2f);
                    else
                        locIndex += 1;
                }
                // end attack 
                if (locIndex == locations.Length + 1)
                {
                    shadow.Play("ShadowLaugh");
                    shadow.an.SetBool("jumping", true);
                    shadow.rb.velocity = new Vector2(0f, -7f);
                    TurnOff();
                }
            }
            // while the first fball is moving
            else
            {
                timeLimit += Time.deltaTime;
                if (timeLimit > 0.5f)
                    TurnOff();
            }
        }
    }
}
