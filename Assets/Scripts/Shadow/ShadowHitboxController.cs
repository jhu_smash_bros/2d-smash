﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShadowHitboxController : MonoBehaviour
{
    public DamageValues dv;
    public SmashCharacter shadow;

    private Collider2D cl;
    private Rigidbody2D rb;
    private Vector2 storedVel = Vector2.zero;

    void Start()
    {
        if (gameObject.tag == "Spark")
            rb = gameObject.GetComponent<Rigidbody2D>();
    }

    public void TurnOff()
    {
        if (!UtilStaticVars.IsReplay)
        {
            shadow.RemoveProjectile(this.gameObject);
            Destroy(this.gameObject);
        }
    }

    // only used for the spark
    public void SetVelX(float x)
    {
        rb.velocity = new Vector2(shadow.facing * x, rb.velocity.y);
        if (shadow.an.GetBool("jumping"))
            rb.velocity = new Vector2(shadow.facing * x, -1 * x);
    }

    // once the hitbox is removed
    void OnDisable()
    {
        shadow.hitting = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Stage" && gameObject.tag == "Spark")
            rb.velocity = new Vector2(0f, 0f);

        //damage character on hit
        if ((other.tag == "Character" || other.tag == "Shield") && other.transform.parent.name != shadow.name)
        {
            cl = other;
            StartCoroutine(shadow.Damage(other.attachedRigidbody.name,
                                         dv.percentDamage, dv.knockback,
                                         dv.hitStun, dv.follow,
                                         dv.followDis, dv.spike));
        }
    }

    void FixedUpdate()
    {
        // don't fixed update on replay
        if (UtilStaticVars.IsReplay) return;
        
        // don't animate/move when paused
        if (UtilStaticVars.Pause)
        {
            if (gameObject.tag == "Spark")
            {
                GetComponent<Animator>().enabled = false;
                if (storedVel == Vector2.zero)
                {
                    storedVel = rb.velocity;
                    rb.velocity = Vector2.zero;
                }
            }
            return;
        }

        if (gameObject.tag == "Spark")
        {
            GetComponent<Animator>().enabled = true;
            if (storedVel != Vector2.zero)
            {
                rb.velocity = storedVel;
                storedVel = Vector2.zero;
            }
        }

        // account for longer attack
        if (shadow.hitting && cl != null)
        {
            if (gameObject.name == "NeutralBHitbox1")
            {
                StartCoroutine(shadow.Damage(cl.attachedRigidbody.name,
                                             dv.percentDamage, dv.knockback,
                                             dv.hitStun, dv.follow,
                                             dv.followDis, dv.spike)
                );
                cl.GetComponentInParent<Rigidbody2D>().velocity = shadow.rb.velocity;
            }
        }
    }
}
